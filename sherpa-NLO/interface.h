/*
 * sherpa/interface.h
 *
 * This file is part of NJet library
 * Copyright (C) 2011, 2012, 2013 NJet Collaboration
 *
 * This software is distributed under the terms of the GNU General Public
 * License (GPL)
 */

#ifndef SHERPA_INTERFACE_H
#define SHERPA_INTERFACE_H

#include <array>
#include <string>
#include <vector>

#include "njet.h"
#include "ngluon2/Mom.h"

namespace NJetSherpaInterface {

    // initialise NJet
    void initNJet();

    // external particles defined by a string with PDG numbers
    // eg. "21 21 -> 22 22" means: gluon gluon -> photon photon
    // also provide alpha and alpha_s powers to specify process
    // return process ID for leading order channel (LO*LO)
    int getProcessLO(
            const std::string& particles,
            int alphaS,
            int alpha);

    // return process ID for colour correlated channel
    // for real counterterm
    // also provide alpha and alpha_s powers to specify process
    int getProcessCC(
            const std::string& particles,
            int alphaS,
            int alpha,
            int i,
            int k);

    // return process ID for spin correlated channel
    // for real counterterm
    // also provide alpha and alpha_s powers to specify process
    int getProcessSC(
            const std::string& particles,
            int alphaS,
            int alpha,
            int i,
            int k);

    int getProcessCS(
            const std::string& particles,
            int alphaS,
            int alpha,
            int i);

    // evaluate process and return result as complex number
    // specify process by ID from getProcess*
    // also provide values of external momenta, scale (mu_r^2), alpha, and alpha_s
    std::complex<double> evalProcess(
            int id,
            const std::vector<std::array<double, 4>>& mom,
            double MuR,
            double alphaS,
            double alpha);

    // convert interface momentum format to NJet momentum format
    MOM<double> convertMom(const std::array<double, 4>& mom);

}  // namespace NJetSherpaInterface

#endif
