#!/usr/bin/env bash

# Usage: ./analytic-expressions-template.sh <multiplicity> <splitting amplitude> <result type> <loop type>
# Example: ./analytic-expressions-template.sh 4 g2qqg LoopResult f
# Note <result type> does not include the template argument

# Generates a template for the function definitions of Split_$p_a
# where the splitting amplitude has $m legs 
# and has $f running in the loop, taking values 0 for tree, f for fermion, and g for gluon
# returning type $t<T>

m=$1
p=$2
t=$3
f=$4

u=$((2 ** $m))
for ((n = 0; n < $u; n++)); do

    z=$n
    arr=()
    for ((i = $m - 1; i >= 0; i--)); do
        y=$(($z - (2 ** $i)))
        if (($y >= 0)); then
            z=$y
            arr[$i]='+'
        else
            arr[$i]='-'
        fi
    done

    printf '// '
    for j in ${arr[*]}; do
        printf $j
    done
    printf '\ntemplate <typename T>\n'$t'<T> Split'$p'_a<T>::hSp'$f$n'(const int* p)\n{\n    return '$t'<T>();\n}\n\n'

done
