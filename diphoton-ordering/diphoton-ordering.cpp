#include <iostream>
#include <string>
#include <vector>

using std::cout;
using std::endl;
using std::string;
using std::vector;

int main(int argc, char **argv) {
    const string amp{argv[1]};
    auto mul{amp.size()};

    vector<int> gs;
    vector<int> As;
    const char gluon{'g'};
    const char photon{'A'};

    for (int i{0}; i < mul; ++i) {
        if (amp[i] == gluon) {
            gs.push_back(i);
        } else if (amp[i] == photon) {
            As.push_back(i);
        }

    };

    auto perm_len{gs.size()};
    auto num_As{As.size()};

    int i_max;
    int amp_perms[i_max][mul];

    if (num_As == 1) {
        i_max = perm_len;
        for (int i{0}; i < perm_len; ++i) {
            amp_perms[i][0] = As[0];
            for (int j{1}; j < perm_len + 1; ++j) {
                amp_perms[i][j] = gs[(j - 1 + i) % perm_len];
            }
        }

    } else if (num_As == 2) {
        int bases[perm_len][perm_len + 1];
        for (int i{0}; i < perm_len; ++i) {
            bases[i][0] = As[0];
            for (int j{1}; j < perm_len + 1; ++j) {
                bases[i][j] = gs[(j - 1 + i) % perm_len];
            }
        }

        i_max = perm_len * (mul - 1);
        for (int k{1}; k < mul; ++k) {
            for (int i{0}; i < perm_len; ++i) {
                for (int j{0}; j < mul; ++j) {
                    auto new_i{perm_len * (k - 1) + i};
                    if (j < k) {
                        amp_perms[new_i][j] = bases[i][j];
                    } else if (j == k) {
                        amp_perms[new_i][j] = As[1];
                    } else if (j > k) {
                        amp_perms[new_i][j] = bases[i][j - 1];
                    }
                }
            }
        }
    }

//    for (int i{0}; i < i_max; ++i) {
//        for (int j{0}; j < mul; ++j) {
//            cout << amp_perms[i][j];
//        }
//        cout << endl;
//    }

    cout << "{" << endl;
    for (int i{0}; i < i_max; ++i) {
        cout << "{";
        for (int j{0}; j < mul; ++j) {
            cout << amp_perms[i][j] << ",";
        }
        cout << "}," << endl;
    }
    cout << "}" << endl;

}