#include <algorithm>
#include <cassert>
#include <iostream>
#include <numeric>

template <typename T, int mulL_, int mulR_, int mulS_>
void maps::Map<T, mulL_, mulR_, mulS_>::print()
{
#ifdef DEBUG
    std::cout << "Reduced kinematics\n";

    for (int i { 0 }; i < mulR; ++i) {
        std::cout << "p`" << i << "=" << reducedMom[i] << '\n';
    }
    std::cout << '\n';

    for (int i { 0 }; i < mulR; ++i) {
        for (int j { i + 1 }; j < mulR; ++j) {
            const T s { 2 * dot(reducedMom[i], reducedMom[j]) };
            std::cout << "s" << i << j << "=" << ((s >= 0) ? " " : "") << s << "  ";
        }
        std::cout << '\n';
    }

    std::cout << "Splitting kinematics\n";

    for (int i { 0 }; i < mulS; ++i) {
        std::cout << "p``" << i << "=" << splitMom[i] << '\n';
    }
    std::cout << '\n';

    for (int i { 0 }; i < mulS; ++i) {
        for (int j { i + 1 }; j < mulS; ++j) {
            const T s { 2 * dot(splitMom[i], splitMom[j]) };
            std::cout << "s" << i << j << "=" << ((s >= 0) ? " " : "") << s << "  ";
        }
        std::cout << '\n';
    }
#endif
}

template <typename T, int mulL_, int mulR_, int mulS_>
void maps::Map<T, mulL_, mulR_, mulS_>::test()
{
    const T zero { 1e-10 };
    for (int i { 0 }; i < 2; ++i) {
        assert(reducedMom[o[i]].mass() <= zero);
    }
    MOM<T> sum { std::accumulate(reducedMom.cbegin(), reducedMom.cend(), MOM<T>()) };
    assert(sum.x0 <= zero);
    assert(sum.x1 <= zero);
    assert(sum.x2 <= zero);
    assert(sum.x3 <= zero);

#ifdef DEBUG
    MOM<T> sumS { std::accumulate(splitMom.cbegin(), splitMom.cend(), MOM<T>()) };
    std::cout << "Sum of splitting amplitude momenta:\n" << sumS << "\n\n";
#endif
}

template <typename T, int mulL_, int mulR_, int mulS_>
const std::array<MOM<T>, mulS_>& maps::Map<T, mulL_, mulR_, mulS_>::splitPS() const
{
    return splitMom;
}

template <typename T, int mulL_, int mulR_, int mulS_>
const std::array<MOM<T>, mulR_>& maps::Map<T, mulL_, mulR_, mulS_>::reducedPS() const
{
    return reducedMom;
}

template <typename T, int mulL_, int mulR_, int mulS_>
maps::Map<T, mulL_, mulR_, mulS_>::Map(int o1)
    : nc()
    , reducedMom()
    , splitMom()
{
    std::generate(o.begin(), o.end(), [o1]() mutable { return o1++ % mulF; });
}

template <typename T, int mulF, int mulR>
const MOM<T>& maps::Map2<T, mulF, mulR>::recoil() const
{
    return p3t;
}

template <typename T, int mulF, int mulR>
maps::Map2<T, mulF, mulR>::Map2(const int o1, const std::array<MOM<T>, mulF>& mom)
    : base(o1)
    , p1(mom[o[0]])
    , p2(mom[o[1]])
    , p3(mom[o[2]])
    , p12(p1 + p2)
    , x12_3(dot(p1, p2) / dot(p12, p3))
    , p12t(p12 - x12_3 * p3)
    , p3t((static_cast<T>(1.) + x12_3) * p3)
{
    splitMom = { -p12t, p1, p2 };
    std::copy_n(mom.cbegin(), o[0], reducedMom.begin());
    reducedMom[o[0]] = p12t;
    reducedMom[o[1]] = p3t;
    nc = 2;
    if (o[0] + nc + 1 < mulF) {
        std::copy(mom.cbegin() + o[0] + nc + 1, mom.cend(), reducedMom.begin() + o[0] + 2);
    }

    this->print();

    this->test();
};

template <typename T, int mulF, int mulR>
maps::Map3<T, mulF, mulR>::Map3(int o1, const std::array<MOM<T>, mulF>& mom)
    : base(o1)
    , p1(mom[o[0]])
    , p2(mom[o[1]])
    , p3(mom[o[2]])
    , p123(p1 + p2 + p3)
    , p4(mom[o[3]])
    , s123(S(p123))
    , x123_4(s123 / static_cast<T>(2.) / dot(p123, p4))
    , p123t(p123 - x123_4 * p4)
    , p4t((static_cast<T>(1.) + x123_4) * p4)
{
    splitMom = { -p123t, p1, p2, p3 };
    std::copy_n(mom.cbegin(), o[0], reducedMom.begin());
    reducedMom[o[0]] = p123t;
    reducedMom[o[1]] = p4t;
    nc = 3;
    if (o[0] + nc + 1 < mulF) {
        std::copy(mom.cbegin() + o[0] + nc + 1, mom.cend(), reducedMom.begin() + o[0] + 2);
    }

    this->print();

    this->test();
};

template <typename T, int mulF, int mulR>
const MOM<T>& maps::Map3<T, mulF, mulR>::recoil() const
{
    return p4t;
}
