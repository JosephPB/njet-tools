# eigen-test
## Install
```shell
cd ~/git
git clone https://gitlab.com/libeigen/eigen.git
mkdir -p ~/scratch/eigen
cd ~/scratch/eigen
echo "export EIGEN_LOCAL=$LOCAL/eigen" >> ~/.zshrc && source ~/.zshrc
cmake ~/git/eigen -DCMAKE_INSTALL_PREFIX=$EIGEN_LOCAL
make install
echo "export PKG_CONFIG_PATH=$EIGEN_LOCAL/share/pkgconfig:$PKG_CONFIG_PATH" >> ~/.zshrc
```
## Usage
See [`Makefile`](./Makefile) and [`test.cpp`](./test.cpp)
## SparseMatrix
* [Tutorial](https://eigen.tuxfamily.org/dox/group__TutorialSparse.html)
* [Reference](https://eigen.tuxfamily.org/dox/classEigen_1_1SparseMatrix.html#a54adf6aa526045f37e67e352da8fd105)
