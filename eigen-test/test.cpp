#include <Eigen/SparseCore>
#include <iostream>
#include <qd/qd_real.h>
#include <vector>

// template <typename T>
// void run1()
// {
//     const int rows { 20 };
//     const int cols { 10 };

//     std::vector<Eigen::Triplet<T>> tripletList;
//     tripletList.reserve(5);
//     for (int x { 0 }; x < 5; ++x) {
//         tripletList.push_back(Eigen::Triplet<T>(x, x, x));
//     }
//     Eigen::SparseMatrix<T, Eigen::RowMajor> mat(rows, cols);
//     mat.setFromTriplets(tripletList.begin(), tripletList.end());
//     std::cout
//         << mat.nonZeros() << '\n'
//         << '\n';
// }

template <typename T>
void reader(typename Eigen::SparseMatrix<T, Eigen::ColMajor> mat)
{
    for (int k { 0 }; k < mat.outerSize(); ++k) {
        for (typename Eigen::SparseMatrix<T, Eigen::ColMajor>::InnerIterator it(mat, k); it; ++it) {
            std::cout << it.value() << " @(i,j)=(" << it.row() << ',' << it.col() << ")\n";
        }
    }
}

template <typename T>
void run2()
{
    const int iLen { 20 }; // # rows = length of a column = i_max+1
    const int jLen { 10 }; // # cols = length of a row = j_max+1

    Eigen::SparseMatrix<T, Eigen::ColMajor> mat(iLen, jLen);

    // mat.reserve(std::vector<int>(cols, 1));

    // mat.insert(i, j) = v_ij;
    mat.insert(0, 0) = T(5.);
    mat.insert(1, 1) = 5.;
    mat.insert(9, 6) = 4. / 3.;
    mat.insert(9, 7) = T(4. / 3.);
    mat.insert(8, 7) = 4. / T(3.);
    mat.insert(9, 8) = T(4.) / 3.;
    mat.insert(9, 9) = T(4.) / T(3.);
    mat.insert(19, 9) = T(4.);

    mat.makeCompressed();

    std::cout
        << mat.rows() << '\n' // iLen
        << mat.cols() << '\n' // jLen
        << mat.outerSize() << '\n' // cols since ColMajor
        << mat.innerSize() << '\n' // rows since ColMajor
        << mat.nonZeros() << '\n'
        << mat.coeff(19, 1) << '\n'
        << '\n';

    reader(mat);
}

int main()
{
    std::cout.precision(32);
    std::cout.setf(std::ios_base::scientific);

    // run1<dd_real>();
    run2<dd_real>();
}
