---
title: NJet tools
author: Ryan Moodie
date: 4 Dec 2019
---

# Environment configuration

* On my laptop, using `zsh` configured at `~/.zshrc`
    * Local packages installed to `$LOCAL/PACKAGE` where `LOCAL=$HOME/local`
* On my IPPP system, using `bash` configured at `~/.profile` with `source $HOME/.profile`  in `~/.bashrc`
    * `~/.profile` contains:
    ```sh
    export LOCAL=$HOME/local
    export PATH=$LOCAL/bin:$PATH
    export LD_LIBRARY_PATH=$LOCAL/lib:$LD_LIBRARY_PATH
    ```
    * Local packages installed to `$LOCAL`
* On both, `GIT=$HOME/git`
* README pdfs generated from md with `pandoc` using `tex`
    * `sudo dnf install pandoc texlive-collection-basic texlive-fvextra texlive-collection-fontsrecommended texlive-ulem`

# Installing njet-develop

* On my laptop

```sh
sudo dnf install autoconf automake libtool gcc gcc-c++ gcc-gfortran make
cd ~/git
git clone --recursive git@bitbucket.org:njet/njet-develop.git
cd njet-develop
autoreconf -fi
mkdir -p ~/scratch/njet-build
cd ~/scratch/njet-build
echo "export NJET_LOCAL=$LOCAL/njet" >> ~/.zshrc
exec $SHELL
FFLAGS='-std=legacy' CXXFLAGS='-std=c++11 -O2 -Wall' ~/git/njet-develop/configure --prefix=$NJET_LOCAL --enable-oneloop1 --enable-quaddouble #--with-vc=$VC_LOCAL
make -j # fails
cd lib-oneloop1
make -j
cd ..
make check # optional
make -j install
echo "export PATH=$NJET_LOCAL/bin:$PATH" >> ~/.zshrc
echo "export LD_LIBRARY_PATH=$NJET_LOCAL/lib:$LD_LIBRARY_PATH" >> ~/.zshrc
exec $SHELL
```

* On my IPPP system (with `LOCAL=$HOME/local` and elsewise as above [so `NJET_LOCAL=LOCAL` and `VC_LOCAL=LOCAL`])

```sh
cd ~/git
git clone --recursive git@bitbucket.org:njet/njet-develop.git
cd njet-develop
autoreconf -fi
mkdir -p ~/build/njet
cd ~/build/njet  
FFLAGS='-std=legacy' CXXFLAGS='-std=c++11 -O2' ~/git/njet-develop/configure --prefix=$NJET_LOCAL --enable-oneloop1 --enable-quaddouble #--with-vc=$VC_LOCAL
make -j
make install
```

If there are issues with the `make`,
```sh
cd lib-oneloop1
make -j
cd ..
```
then try again.

* Makefiles then need

```sh
CPPFLAGS+= -I$(NJET_LOCAL)/include -I$(GIT)/njet-develop
LDFLAGS+= -L$(NJET_LOCAL)/lib
COMMON+= -DUSE_DD -DUSE_QD
LIBS+= -lnjet2 -lnjet2tools -lqd
```

# Installing [libVc](https://github.com/VcDevel/Vc)

* Install on my laptop (with `VC_LOCAL=$LOCAL/Vc`)

```sh
sudo dnf install cmake
cd ~/git
git clone --recursive git@github.com:VcDevel/Vc.git
mkdir -p ~/scratch/Vc
cd ~/scratch/Vc
cmake -DCMAKE_INSTALL_PREFIX=$VC_LOCAL -DBUILD_TESTING=OFF ~/git/Vc
make -j
make install
echo "export LD_LIBRARY_PATH=$VC_LOCAL/lib:$LD_LIBRARY_PATH" >> ~/.zshrc
exec $SHELL
```

then include in Makefile

```sh
CPPFLAGS+= -I$(VC_LOCAL)/include
LDFLAGS+= -L$(VC_LOCAL)/lib
COMMON+= -DUSE_VC
LIBS+= -lVc
```

* Warning this doesn't work!
    * Vc 1.4 breaks when used with GCC 9.2.1 
        * e.g. standard library class (complex) and functions (abs) involve comparisons, which Vc types break
    * NJet compile seems to need `CXXFLAGS+='-DUSE_VC -I$(VC_LOCAL)/include'`
    * `Vc_1.4` branch is for fixing this (not done)

* On the IPPP system, need newer cmake (with `VC_LOCAL=$LOCAL`)

```sh
mkdir ~/programs
cd ~/programs
wget https://github.com/Kitware/CMake/releases/download/v3.16.1/cmake-3.16.1-Linux-x86_64.tar.gz
tar xvzf cmake-3.16.1-Linux-x86_64.tar.gz
echo "alias cmake=~/programs/cmake-3.16.1-Linux-x86_64/bin/cmake" >> ~/.profile
```
then proceed as above.

# Updating BLHA interface

* On `zsh`

```sh
cd ~/git/njet-develop
./blha/njet.py -d >! chsums/NJetChannels.h
```

* On `bash`

```sh
cd ~/git/njet-develop
./blha/njet.py -d > chsums/NJetChannels.h
```
