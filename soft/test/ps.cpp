#include <algorithm>
#include <array>
#include <cassert>
#include <cmath>
#include <cstddef>
#include <iostream>
#include <numbers>
#include <numeric>
#include <tuple>

#include "mom4.hpp"

template <std::size_t mul>
void test_massless(const std::array<mom4, mul>& momenta)
{
    double zero { 1e-10 };
    std::for_each(momenta.cbegin(), momenta.cend(), [zero](const mom4& m) { assert(std::abs(m.mass2()) < zero); });
}

template <std::size_t mul>
void test_mom_cons(const std::array<mom4, mul>& momenta)
{
    double zero { 1e-10 };
    mom4 sum { std::accumulate(momenta.cbegin(), momenta.cend(), mom4(0.)) };
    std::for_each(sum.cbegin(), sum.cend(), [zero](double x) { assert(x < zero); });
}

std::array<mom4, 5> p5_c2(const double y1, const double y2, const double theta, const double alpha, const double sqrtS, int r)
{
    assert(0. <= y1);
    assert(0. <= y2);
    assert(y1 + y2 <= 1.);
    assert(r >= 0);

    r %= 5;

    const double x1 { 1. - y1 };
    const double x2 { 1. - y2 };

    // cos(beta)
    const double cb { 1. + 2. * (1. - x1 - x2) / x1 / x2 };
    assert((-1. <= cb) && (cb <= 1.));
    // sin(beta)
    const double sb { std::sqrt(1. - std::pow(cb, 2.)) };

    const double ct { std::cos(theta) };
    const double st { std::sin(theta) };

    const double ca { std::cos(alpha) };
    const double sa { std::sin(alpha) };

    std::array<mom4, 5> momenta { {
        sqrtS / 2. * mom4(-1., 0., 0., -1.),
        sqrtS / 2. * mom4(-1., 0., 0., 1.),
        x1 * sqrtS / 2. * mom4(1., st, 0., ct),
        x2 * sqrtS / 2. * mom4(1., ca * ct * sb + cb * st, sa * sb, cb * ct - ca * sb * st),
        mom4(),
    } };

    momenta[4] = -std::accumulate(momenta.cbegin(), momenta.cend() - 1, mom4(0.));

    std::rotate(momenta.begin(), momenta.begin() + r, momenta.end());

    test_massless(momenta);
    test_mom_cons(momenta);

    return momenta;
}

// int main(int argc, char* argv[])
int main()
{
    std::cout.precision(16);
    std::cout.setf(std::ios_base::scientific);
    std::cout << '\n';

    // take p3 soft (zero index)
    const std::array<mom4, 5> moms { p5_c2(1e-6, 1.-1e-5, std::numbers::pi / 3., std::numbers::pi / 5., 1., 0) };

    for (int i { 0 }; i < 5; ++i) {
        for (int j { i + 1 }; j < 5; ++j) {
            const double s { 2. * (moms[i] * moms[j]) };
            std::cout << "s" << i << j << "=" << ((s >= 0) ? " " : "") << s << '\n';
        }
    }
    std::cout << '\n';

    std::array<mom4, 4> red_moms;
    std::copy_n(moms.cbegin(), 3, red_moms.begin());
    red_moms[3] = moms[4];
    std::array<mom4, 3> soft_moms;
    std::copy(moms.cbegin() + 2, moms.cend(), soft_moms.begin());

    std::cout << "const std::array<MOM<double>, 5> full_mom { {\n";
    for (const mom4& m : moms) {
        std::cout << "    " << m << ",\n";
    }
    std::cout << "} };\n\n";

    std::cout << "const std::array<MOM<double>, 4> reduced_mom { {\n";
    for (const mom4& m : red_moms) {
        std::cout << "    " << m << ",\n";
    }
    std::cout << "} };\n\n";

    std::cout << "const std::array<MOM<double>, 3> soft_mom { {\n";
    for (const mom4& m : soft_moms) {
        std::cout << "    " << m << ",\n";
    }
    std::cout << "} };\n\n";

    std::cout << '\n';
}
