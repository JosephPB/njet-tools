# Exploration of overlapping IR limits

## Description

* Here, I investigate the behaviour of |A|^2 for gg->ggg on the s34,s45-plane.
* Phase space points are generated randomly from integer random number seeds.
* `overlap.cpp` leverages parallel execution to generate the squared amplitude values using `NJet`, recording the result and its (s34,s45) coordinates.
* `process.py` reads in the data as [`numpy.array`s](https://numpy.org/doc/stable/reference/generated/numpy.array.html) of [`float`s](https://docs.python.org/3.8/tutorial/floatingpoint.html#representation-error), discards divergent values, and stores the data as a [NumPy binary](https://numpy.org/doc/stable/reference/generated/numpy.lib.format.html#module-numpy.lib.format).
* `plot.py` plots

## Usage

* Compile: `make`
* Generate data: `./overlap <initial rseed> <number of threads> <total number of phase space points>`
* Process data: `./process.py`
* Generate plots: `./plot.py`

## TODO

* I should filter out PS points which are collinear in other limits.
* Check getPSpoint CoM. Set to fixed?
