#include <algorithm>
#include <array>
#include <cassert>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <mutex>
#include <thread>
#include <vector>

#include "ngluon2/Model.h"
#include "ngluon2/Mom.h"
#include "njet.h"
#include "tools/PhaseSpace.h"

class MatrixElement {
private:
    static constexpr int channel { 1 };
    static constexpr double mur { 91.188 };
    static constexpr double alphas { 0.118 };
    static constexpr double zero { 0. };
    static constexpr double half_cut { 50. };

    int rstatus;

public:
    static constexpr int legs { 5 };
    inline static const std::vector<Flavour<double>> flavours {
        StandardModel::G(),
        StandardModel::G(),
        StandardModel::G(),
        StandardModel::G(),
        StandardModel::G(),
    };

    double point(const std::vector<MOM<double>>& mom) const
    {
        std::array<double, legs * 5> LHMomenta;
        for (int p { 0 }; p < legs; ++p) {
            LHMomenta[0 + p * 5] = mom[p].x0;
            LHMomenta[1 + p * 5] = mom[p].x1;
            LHMomenta[2 + p * 5] = mom[p].x2;
            LHMomenta[3 + p * 5] = mom[p].x3;
            LHMomenta[4 + p * 5] = 0.;
        }

        double out;
        double acc { 0. };

        OLP_EvalSubProcess2(&channel, LHMomenta.data(), &mur, &out, &acc);

        return out;
    }

    bool good(const std::vector<MOM<double>>& mom, std::mutex& t_mtx)
    {
        for (int i { 0 }; i < legs; ++i) {
            for (int j { i + 1 }; j < legs; ++j) {
                if (!((i == 3 && j == 4) || (i == 2 && j == 3))) {
                    if (std::abs(dot(mom[i], mom[j])) < half_cut) {
                        {
                            std::scoped_lock<std::mutex> locked(t_mtx);
                            std::cout << "Discarding point: s" << i << j << "=" << 2. * dot(mom[i], mom[j]) << '\n';
                        }
                        return false;
                    }
                }
            }
        }
        return true;
    }

    MatrixElement()
    {
        OLP_Start("OLE_contract.lh", &rstatus);
        assert(rstatus);

        OLP_SetParameter("alphas", &alphas, &zero, &rstatus);
        assert(rstatus == 1);
    }
};

void calc(MatrixElement me, const int start_seed, const int number_points, std::mutex* f_mtx, std::mutex* t_mtx)
{
    std::vector<std::array<double, 3>> results;

    for (int rseed { start_seed }; rseed < (start_seed + number_points); ++rseed) {

        PhaseSpace<double> ps(me.flavours, rseed);
        std::vector<MOM<double>> mom { ps.getPSpoint() };

        if (me.good(mom, *t_mtx)) {
            const double s34 { 2. * dot(mom[2], mom[3]) };
            const double s45 { 2. * dot(mom[3], mom[4]) };

            results.push_back({ s34, s45, me.point(mom) });
        }
    }

    {
        std::scoped_lock<std::mutex> locked(*f_mtx);
        std::ofstream o("data.txt", std::ios::app);
        o.setf(std::ios_base::scientific);
        o.precision(16);
        for (const std::array<double, 3>& result : results) {
            // o << s34 << " " << s45 << " " << out << '\n';
            o << result[0] << " " << result[1] << " " << result[2] << '\n';
        }
    }
}

int main(int argc, char* argv[])
{
    std::cout.setf(std::ios_base::scientific);
    std::cout.precision(16);

    assert(argc == 4);
    const int init_seed { std::atoi(argv[1]) };
    assert(init_seed > 0);
    const int jobs { std::atoi(argv[2]) };
    assert(jobs > 0);
    const int pspoints { std::atoi(argv[3]) };
    assert(pspoints > 0);
    const int end_seed { init_seed + pspoints - 1 };
    const int job_points { pspoints / jobs };
    std::cout << "Total number of phase space points: " << (job_points * jobs) << ".\n"
              << "Random number seeds: "
              << init_seed << " to " << end_seed << " inclusive.\n"
              << "Running " << jobs << " thread" << (jobs == 1 ? "" : "s")
              << " with " << job_points << " points per thread.\n";

    MatrixElement me;
    std::mutex f_mtx;
    std::mutex t_mtx;

    std::vector<std::thread> threads(jobs);

    for (int i { 0 }; i < jobs; ++i) {
        threads[i] = std::thread(calc, me, (init_seed + i * job_points), job_points, &f_mtx, &t_mtx);
    }

    std::for_each(threads.begin(), threads.end(), [](std::thread& thrd) { thrd.join(); });
}
