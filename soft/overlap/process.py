#!/usr/bin/env python3
# coding=UTF-8

import pathlib

import numpy


class Process:
    def __init__(self, in_name, out_name):
        self.in_name = in_name
        self.out_name = out_name

    def read_in(self):
        if pathlib.Path(self.out_name).is_file():
            print(f"File {self.out_name} already exists.")
            if input("Use for reprocessing? [y for yes] ") == "y":
                with open(self.out_name, "rb") as in_file:
                    return numpy.load(in_file)

        print(f"Reading data from file {self.in_name}.")
        with open(self.in_name, "r") as in_file:
            data = numpy.loadtxt(in_file)
        return numpy.transpose(data)

    @staticmethod
    def process(data):
        print("Processing data.")
        data = numpy.transpose(data)
        div = []
        for i in range(len(data)):
            if any(numpy.isnan(data[i])):
                div.append(i)
        for i in div:
            print(f"Deleting row {i} since it has |A|^2=inf")
            data = numpy.delete(data, (i), axis=0)

        return numpy.transpose(data)

    def write_out(self, data):
        if pathlib.Path(self.out_name).is_file():
            print(f"File {self.out_name} already exists.")
            if input(f"Overwrite file {self.out_name}? [y for yes] ") != "y":
                print("*Not* overwriting file.")
                return None

        print(f"Writing data to file {self.out_name}.")
        with open(self.out_name, "wb") as out_file:
            numpy.save(out_file, data)


if __name__ == "__main__":
    p = Process("data.txt", "data.npy")
    data = p.read_in()
    data = p.process(data)
    p.write_out(data)
