#include <algorithm>
#include <array>
#include <cassert>
#include <cmath>
#include <cstddef>
#include <iostream>
#include <numbers>
#include <numeric>
#include <tuple>

#include "mom4.hpp"

template <std::size_t mul>
void test_massless(const std::array<mom4, mul>& momenta)
{
    double zero { 1e-10 };
    std::for_each(momenta.cbegin(), momenta.cend(), [zero](const mom4& m) { assert(std::abs(m.mass2()) < zero); });
}

template <std::size_t mul>
void test_mom_cons(const std::array<mom4, mul>& momenta)
{
    double zero { 1e-10 };
    mom4 sum { std::accumulate(momenta.cbegin(), momenta.cend(), mom4(0.)) };
    std::for_each(sum.cbegin(), sum.cend(), [zero](double x) { assert(x < zero); });
}

std::array<mom4, 6> p6_s2(const double x1, const double x2, const double alpha, const double beta, const double gamma, const double sqrtS, int r)
{
    assert(r >= 0);

    r %= 5;

    const double ca { cos(alpha) };
    const double cb { cos(beta) };
    const double cg { cos(gamma) };

    const double sa { sin(alpha) };
    const double sb { sin(beta) };
    const double sg { sin(gamma) };

    // x3 fixes masslessness of p5
    const double x3 {
        (4 - 4 * x1 + x1 * x1 - ca * ca * x1 * x1 - sa * sa * x1 * x1 - 4 * x2 + 2 * x1 * x2 - 2 * ca * ca * cb * x1 * x2 - 2 * sa * sa * x1 * x2 + x2 * x2 - ca * ca * cb * cb * x2 * x2 - sa * sa * x2 * x2 - ca * ca * sb * sb * x2 * x2) / (2 * (2 - x1 + ca * ca * cb * x1 + cg * sa * sa * x1 - ca * sa * sb * sg * x1 - x2 + ca * ca * cb * cb * x2 + cg * sa * sa * x2 + ca * ca * cg * sb * sb * x2))
    };

    // generate momenta recursively
    // incoming momenta are back-to-back
    std::array<mom4, 6> momenta { {
        -sqrtS / 2. * mom4(1., 0., 0., 1.),
        -sqrtS / 2. * mom4(1., 0., 0., -1.),
        // p2 = rotate p1 by alpha about x
        x1 * sqrtS / 2. * mom4(1., 0., -sa, ca),
        // p3 = rotate p2 by beta about y
        x2 * sqrtS / 2. * mom4(1., ca * sb, -sa, ca * cb),
        // p4 = rotate p3 by gamma about z
        x3 * sqrtS / 2. * mom4(1., ca * cg * sb + sa * sg, -(cg * sa) + ca * sb * sg, ca * cb),
        mom4(),
    } };
    // p5 given by momentum conservation
    momenta[5] = -std::accumulate(momenta.begin(), momenta.end() - 1, mom4());

    std::rotate(momenta.begin(), momenta.begin() + r, momenta.end());

    test_massless(momenta);
    test_mom_cons(momenta);

    return momenta;
}

// int main(int argc, char* argv[])
int main()
{
    std::cout.precision(16);
    std::cout.setf(std::ios_base::scientific);
    std::cout << '\n';

    const int mul { 6 };

    // take p2 & p3 soft (zero index)
    const double small { 1e-6 };
    const std::array<mom4, mul> moms { p6_s2(small, small, std::numbers::pi / 3., std::numbers::pi / 5., std::numbers::pi / 7., 1., 0) };
    // const std::array<mom4, mul> moms { p6_s2(small, std::pow(small, 2), std::numbers::pi / 3., std::numbers::pi / 5., std::numbers::pi / 7., 1., 0) };

    for (int i { 0 }; i < mul; ++i) {
        for (int j { i + 1 }; j < mul; ++j) {
            const double s { 2. * (moms[i] * moms[j]) };
            std::cout << "s" << i << j << "=" << ((s >= 0) ? " " : "") << s << '\n';
        }
    }
    std::cout << '\n';

    std::cout << "const std::array<MOM<double>, 6> full_mom { {\n";
    for (const mom4& m : moms) {
        std::cout << "    " << m << ",\n";
    }
    std::cout << "} };\n\n";
}
