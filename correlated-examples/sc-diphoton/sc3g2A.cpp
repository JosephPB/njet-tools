#include <iostream>

#include "njet.h"

int main()
{
    std::cout.sync_with_stdio(false);
    std::cout.setf(std::ios_base::scientific);
    std::cout.precision(16);

    std::cout << "\n  NJet: simple example of the scloopsq BLHA interface\n";

    const int legs { 5 };
    const int colLegs { 3 };
    const int pspoints { 1 };
    double Momenta[pspoints][legs][4] = {
        { { 2.5000000000000000e+02, 0.0000000000000000e+00, 0.0000000000000000e+00, 2.5000000000000000e+02 },
            { 2.5000000000000000e+02, 0.0000000000000000e+00, 0.0000000000000000e+00, -2.5000000000000000e+02 },
            { 1.5943717455296405e+02, -9.6097656622523473e+01, 1.0897987122994738e+02, -6.5641760242973277e+01 },
            { 2.4437697027369072e+02, 1.2354774195573731e+02, -2.0113465489985822e+02, 6.3252744257477040e+01 },
            { 9.6185855173345232e+01, -2.7450085333213842e+01, 9.2154783669910842e+01, 2.3890159854962425e+00 } }
    };

    int rstatus;
    OLP_Start("contract_sc3g2A.lh", &rstatus);
    if (rstatus) {
        std::cout << "OLP read in correctly\n";
    } else {
        std::cout << "seems to be a problem with the contract file...\n";
        exit(1);
    }

    char olpname[15];
    char olpversion[15];
    char olpmessage[255];
    OLP_Info(olpname, olpversion, olpmessage);
    std::cout << "Running " << olpname
              << " version " << olpversion
              << " note " << olpmessage << "\n";

    for (int pts { 0 }; pts < pspoints; ++pts) {
        std::cout << "==================== Test point " << pts + 1 << " ====================\n";
        double LHMomenta[legs * 5];
        for (int p { 0 }; p < legs; ++p) {
            for (int mu { 0 }; mu < 4; ++mu) {
                LHMomenta[mu + p * 5] = Momenta[pts][p][mu];
                std::cout << Momenta[pts][p][mu] << " ";
            }
            LHMomenta[4 + p * 5] = 0.;
            std::cout << "\n";
        }
        std::cout << "\n";

        const int channels { 1 };
        for (int p { 1 }; p <= channels; ++p) {
            const int epslen { 5 };
            // 2 (complex) * EpsN * square matrix
            const int reslen { 2 * epslen * colLegs * colLegs };
            // reslen*2 for error & result
            double out[2 * reslen] {};
            double acc { 0. };

            const double alphas { 0.118 };
            const double alpha { 1. / 137.035999084 };
            const double zero { 0. };
            const double mur { 91.188 };

            OLP_SetParameter("alphas", &alphas, &zero, &rstatus);
            if (rstatus == 1) {
                std::cout << "Setting AlphaS = " << alphas << ": OK\n";
            } else if (rstatus == 0) {
                std::cout << "Setting AlphaS: FAIL\n";
            } else {
                std::cout << "Setting AlphaS: UNKNOWN\n";
                exit(2);
            }

            OLP_SetParameter("alpha", &alpha, &zero, &rstatus);
            if (rstatus == 1) {
                std::cout << "Setting Alpha = " << alpha << ": OK\n";
            } else if (rstatus == 0) {
                std::cout << "Setting Alpha: FAIL\n";
            } else {
                std::cout << "Setting Alpha: UNKNOWN\n";
                exit(2);
            }

            OLP_EvalSubProcess2(&p, LHMomenta, &mur, out, &acc);

            std::cout << "muR = " << mur << "\n"
                      << "---- process number " << p << " ----\n"
                      << "OLP accuracy check: " << acc << "\n";

            for (int i { 0 }; i < colLegs; ++i) {
                for (int j { 0 }; j < colLegs; ++j) {
                    for (int a { 0 }; a < epslen; ++a) {
                        const int ra { epslen - 1 - a };
                        const double re { out[2 * epslen * (i + colLegs * j) + 2 * a] };
                        const double im { out[2 * epslen * (i + colLegs * j) + 2 * a + 1] };
                        std::cout << "SC_" << i << "," << j << "@O(e^-" << ra << ") = "
                                  << (re < 0 ? "" : " ") << re
                                  << " " << (im < 0 ? "-" : "+") << " "
                                  << std::abs(im) << "i +- "
                                  << out[reslen + 2 * epslen * (i + colLegs * j) + 2 * a]
                                  << " f\n";
                    }
                }
            }

            // for (double o : out) {
            //     std::cout << o << '\n';
            // }
        }
        std::cout << "\n";
    }

    return 0;
}
