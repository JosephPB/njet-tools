#-

S x1,x2,x3,x4,x5;


L cAN1425= (-64*(x2*x3 - (1 + x3)*(x4 - x5))*(-(x2*x3*(1 + x3)*x4) + x2^2*x3*(1 + x3)*x4 + (1 + x3)*x4*(-1 + x3*(x4 - x5)) + x2^3*x3*(-1 - x4 + x3*(-1 + x5) + x5)));

L cAD1425=(x1*x2^2*x3^2*(x2 - x4)*(1 + x2*x3 + x3*(-x4 + x5))*((1 + x3)*x4 + x2*(-1 - x4 + x3*(-1 + x5) + x5)));

L cAN1534= (-64*(x2 - x4 + x5)*(-(x2*(1 + x3)*x4*(-1 + x3^2*(-1 + 3*x4 - 3*x5) + x3*(-4 + x4 - x5))) - x2^2*x3*(1 + 2*x3)*x4*(-3 + x3*(-2 + x4 - x5)) - (1 + x3)^2*x4*(-1 + x3*(x4 - x5)) +
            x2^4*x3^3*(-1 + x5) + x2^3*x3^2*(3*x4 + x3*(-1 + 3*x4 + x5))));

L cAD1534= (x1*x2^2*x3*(x2^2*x3 - (1 + x3)*x4 + x2*(x3 - x3*x4))*((1 + x3)*x4 + x2*x3*(-1 + x5))*(1 + x2*x3 + x3*(-x4 + x5)));

L cAN1435= (64*(x2*x3 - (1 + x3)*(x4 - x5))*(2*x2^5*x3^3 - x2*(1 + x3)^2*x4*(-1 + x3*(-1 + x4 - x5)) - x2^2*(1 + 3*x3 + 2*x3^2)*(-1 + x3*(x4 - x5)) - (1 + x3)^2*x4*(-1 + x3*(x4 - x5)) +x2^4*x3^2*(5 - x4 + x3*(5 - 2*x4 + x5)) + x2^3*x3*(4 - x4 + x3*(8 - 5*x4 + 2*x5) + x3^2*(3 - 5*x4 + 3*x5))));

L cAD1435= (x1*x2^2*x3^2*(1 + x3)*(x2^2*x3 - x2*(1 + x3)*(-1 + x4) - (1 + x3)*x4)*(x4 + x2*(-1 + x5))*(1 + x2*x3 + x3*(-x4 + x5)));

L cAN1524= (-64*(x2 - x4 + x5)*(-(x2*(1 + x3)*x4*(-2 + x3^2*(-1 + 3*x4 - 3*x5) + 2*x3*(-2 + x4 - x5))) - (1 + x3)^2*x4*(-1 + x3*(x4 - x5)) + x2^4*x3^2*(x4 + x3*(-1 + x5)) + x2^3*x3*(2*x4 + 2*x3*(-1 + 2*x4 + x5) + x3^2*(-3 + x4 + 3*x5)) + x2^2*(x4 - 2*x3^3*(x4*(-3 + x5) - (-1 + x5)*x5) + x3*(-1 - x4^2 + x5 + x4*(5 + x5)) + x3^2*(-2 - 2*x4^2 + x5 + x5^2 + x4*(8 + x5)))));

L cAD1524= (x1*x2^2*(1 + x2)*x3*(x2*x3 - (1 + x3)*x4)*((1 + x3)*x4 + x2*(x4 + x3*(-1 + x5)))*(1 + x2*x3 + x3*(-x4 + x5)));

L cAN2435= (64*(1 + x2)*(x2*x3 - (1 + x3)*x4)*(-1 - x4 + x5 + 2*x3^3*(x2^3 - 3*x2*(x4 - x5) + (x4 - x5)^2 + x2^2*(2 - x4 + x5)) + x3^2*(-6*x2*(x4 - x5) + 3*(x4 - x5)^2 + x2^2*(2 - x4 + x5)) +x3*(-2 + x4^2 + 2*x5 + x5^2 - 2*x4*(1 + x5) + x2*(-1 - 2*x4 + 2*x5))));

L cAD2435= (x1*x2^2*x3^2*(1 + x3)*(x4 + x2*(-1 + x5))*(-1 + x2*x3 - (1 + x3)*x4 + x5 + x3*x5)*(1 + x2*x3 + x3*(-x4 + x5)));


L FBOX1M24135= (-16*x1*(1 + x2)*(1 + 2*(1 + x2)*x3 + 2*(1 + x2)^2*x3^2)*(x2*x3 - (1 + x3)*x4)*(x2*x3 - (1 + x3)*(x4 - x5)))/(x2^3*x3^2);

L FBOX1M24315= (16*x1*(1 + x2)*(2*(1 + x3)^2 + x2^2*(1 + 2*x3 + 2*x3^2) + x2*(2 + 6*x3 + 4*x3^2))*(x2*x3 - (1 + x3)*x4)*((1 + x3)*x4 + x2*x3*(-1 + x5)))/(x2^6*x3^2);

L FBOX1M25134=(-16*x1*(1 + x3 + x2*x3)*(1 + 2*(1 + x2)*x3 + 2*(1 + x2)^2*x3^2)*(x2 - x4)*(x2 - x4 + x5))/(x2^3*x3);

L FBOX1M25314= (16*x1*(1 + x3)*(1 + x3 + x2*x3)*(2*(1 + x3)^2 + x2^2*(1 + 2*x3 + 2*x3^2) + x2*(2 + 6*x3 + 4*x3^2))*(x2 - x4)*(x4 + x2*(-1 + x5)))/(x2^6*x3^2);

L FBOX1M34125= (-16*x1*(1 + 2*x3 + 2*x3^2)*(x2*x3 - (1 + x3)*(x4 - x5))*((1 + x3)*x4 + x2*x3*(-1 + x5)))/(x2^3*x3^2);

L FBOX1M35124= (-16*x1*(1 + x3)*(1 + 2*x3 + 2*x3^2)*(x4 + x2*(-1 + x5))*(x2 - x4 + x5))/(x2^3*x3);


L rat=-64/(x1*x2^2*x3^2);


Format O4;
.sort

Format C;

ExtraSymbols,array,Z;

#optimize cAN1425
#write <optimize_pppmm.c> "%O"
#write <optimize_pppmm.c> "const TreeValue cAN1425{%E};\n", cAN1425

#optimize cAD1425
#write <optimize_pppmm.c> "%O"
#write <optimize_pppmm.c> "const TreeValue cAD1425{%E};\n", cAD1425

#optimize cAN1534
#write <optimize_pppmm.c> "%O"
#write <optimize_pppmm.c> "const TreeValue cAN1534{%E};\n", cAN1534

#optimize cAD1534
#write <optimize_pppmm.c> "%O"
#write <optimize_pppmm.c> "const TreeValue cAD1534{%E};\n", cAD1534

#optimize cAN1435
#write <optimize_pppmm.c> "%O"
#write <optimize_pppmm.c> "const TreeValue cAN1435{%E};\n", cAN1435

#optimize cAD1435
#write <optimize_pppmm.c> "%O"
#write <optimize_pppmm.c> "const TreeValue cAD1435{%E};\n", cAD1435

#optimize cAN1524
#write <optimize_pppmm.c> "%O"
#write <optimize_pppmm.c> "const TreeValue cAN1524{%E};\n", cAN1524

#optimize cAD1524
#write <optimize_pppmm.c> "%O"
#write <optimize_pppmm.c> "const TreeValue cAD1524{%E};\n", cAD1524

#optimize cAN2435
#write <optimize_pppmm.c> "%O"
#write <optimize_pppmm.c> "const TreeValue cAN2435{%E};\n", cAN2435

#optimize cAD2435
#write <optimize_pppmm.c> "%O"
#write <optimize_pppmm.c> "const TreeValue cAD2435{%E};\n", cAD2435

#optimize FBOX1M24135
#write <optimize_pppmm.c> "%O"
#write <optimize_pppmm.c> "const TreeValue FBOX1M24135{%E};\n", FBOX1M24135

#optimize FBOX1M24315
#write <optimize_pppmm.c> "%O"
#write <optimize_pppmm.c> "const TreeValue FBOX1M24315{%E};\n", FBOX1M24315

#optimize FBOX1M25134
#write <optimize_pppmm.c> "%O"
#write <optimize_pppmm.c> "const TreeValue FBOX1M25134{%E};\n", FBOX1M25134

#optimize FBOX1M25314
#write <optimize_pppmm.c> "%O"
#write <optimize_pppmm.c> "const TreeValue FBOX1M25314{%E};\n", FBOX1M25314

#optimize FBOX1M34125
#write <optimize_pppmm.c> "%O"
#write <optimize_pppmm.c> "const TreeValue FBOX1M34125{%E};\n", FBOX1M34125

#optimize FBOX1M35124
#write <optimize_pppmm.c> "%O"
#write <optimize_pppmm.c> "const TreeValue FBOX1M35124{%E};\n", FBOX1M35124

#optimize rat
#write <optimize_pppmm.c> "%O"
#write <optimize_pppmm.c> "const TreeValue rat{%E};\n", rat

#write <optimize_pppmm.c> "const TreeValue cA1425{cAN1425/cAD1425};"
#write <optimize_pppmm.c> "const TreeValue cA1534{cAN1534/cAD1534};"
#write <optimize_pppmm.c> "const TreeValue cA1435{cAN1435/cAD1435};"
#write <optimize_pppmm.c> "const TreeValue cA1524{cAN1524/cAD1524};"
#write <optimize_pppmm.c> "const TreeValue cA2435{cAN2435/cAD2435};"

.end
