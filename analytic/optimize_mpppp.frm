#-

S x1,x2,x3,x4,x5;

L ratN=i_*(64*(x2 - x4 + x5)*(-((1 + x3)^2*x4^3) - x2*(1 + 3*x3 + 2*x3^2)*x4^3 + 
   x2^3*x3*(1 + 2*x3)*(x4^2 - (-1 + x5)^2) + x2^4*x3^2*(1 + x4 - x5)*(-1 + x5) + 
   x2^2*((1 + 3*x3 + 3*x3^2)*x4^2 - x3*(1 + x3)*x4^3 - 
     x3*(1 + x3)*(-1 + x5)*x5 - x4*(-1 + x5 + x3*(-3 + 2*x5) + 
       x3^2*(-3 + 2*x5)))));

L ratD=((1 + x2)*(1 + x3)*(1 + x3 + x2*x3)*(x2 - x4)^2*
  (1 + x4 - x5));

Format O4;
.sort

Format C;

ExtraSymbols,array,Z;

#optimize ratN
#write <optimize_mpppp.c> "%O"
#write <optimize_mpppp.c> "const TreeValue ratN{%E};\n", ratN

#optimize ratD
#write <optimize_mpppp.c> "%O"
#write <optimize_mpppp.c> "const TreeValue ratD{%E};\n", ratD

#write <optimize_mpppp.c> "const TreeValue rat{ratN/ratD};\n"

.end
