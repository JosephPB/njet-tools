#LOCAL?=$(HOME)/local
#VC_LOCAL?=$(LOCAL)/Vc

CXX=g++
CXXFLAGS=-g -O2 -pedantic -Wall -fPIC -DPIC -std=c++17
CXXFLAGS+=-I$(shell Sherpa-config --incdir) $(shell pkg-config njet2 --cflags) #-I$(VC_LOCAL)/include
CXXFLAGS+=-DUSE_DD -DUSE_QD -DUSE_VC
LDFLAGS=$(Sherpa-config --ldflags) $(shell pkg-config njet2 --libs) #-L$(VC_LOCAL)/lib
LDFLAGS+=-lqd #-lVc

SOURCES=NJET0q${N}gAA_Interface.cpp
OBJECTS=$(SOURCES:.cpp=.o)
DEPS=$(SOURCES:.cpp=.hpp)
LIB=libSherpaNJET0q${N}gAA.so

.PHONY: clean wipe all

all: $(LIB) OLE_contract_$(N)g2A.lh

.cpp.o: $(DEPS)
	$(CXX) $(CXXFLAGS) -c $< -o $@

# Generate the library file for the Sherpa/NJET 0qNgAA interface
$(LIB): $(OBJECTS)
	$(CXX) -shared -o $@ $^ $(LDFLAGS)

# Generate BLHA contract file from order file
OLE_contract_$(N)g2A.lh: OLE_order_$(N)g2A.lh
	njet.py -o $@ $<

# Remove compiled files
clean:
	rm -rf $(OBJECTS) $(LIB) OLE_contract_$(N)g2A.lh

# Remove everything
wipe: clean
	rm *.pdf
