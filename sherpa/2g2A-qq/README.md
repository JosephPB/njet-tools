---
title: gg $\rightarrow \gamma \gamma$ with custom analysis using optimised integrator --- integrator library
author: Ryan Moodie
date: 21 Feb 2020
---

## Usage

### Running

* Run on IPPP system
* Generates channels (integrator library) using quark initiated tree
    * `make`
    * ie. detect resonances
* Then can continue with [3g2A-opt](https://gitlab.com/eidoom/njet-tools/-/tree/master/sherpa/3g2A-opt)

### Cleaning

* Delete temporary files and get rid of output with `make clean`
* Also get rid of pdfs with `make wipe`
