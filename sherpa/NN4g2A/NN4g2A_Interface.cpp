#include <algorithm>
#include <cassert>
#include <chrono>
#include <cmath>
#include <complex>
#include <ctime>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

#if defined(NJET) || defined(BOTH)
#include "njet.h"
#endif

#include "NN4g2A_Interface.hpp"
#include "model_fns.h"

#define NN_MODEL "100k_new_sherpa"

// Use (void) to silent unused warnings.
#define assertm(exp, msg) assert(((void)msg, exp))

// cmath
using std::pow;
using std::sqrt;
// complex
using std::complex;
// iostream
using std::cout;
using std::endl;
using std::ostream;
// string
using std::string;
using std::to_string;

// SquaredMatrixElement class constructor and member implementations

NN4g2A::SquaredMatrixElement::SquaredMatrixElement()
    : zero(0.)
    , m_alpha(1. / 128.3479449790089)
    , m_alphacubed(pow(m_alpha, 3))
    , m_ge(sqrt(4. * M_PI * m_alpha))
    , m_prec(1e-5)
    , delta(2e-2)
    , s_com(5e5)
    , cut_dirs("cut_0.02/")
    , model_base("./models/parallel_fixed/" + std::string(NN_MODEL) + "/")
    , model_dirs()
    , pair_dirs({
          "pair_0.02_0/",
          "pair_0.02_1/",
          "pair_0.02_2/",
          "pair_0.02_3/",
          "pair_0.02_4/",
          "pair_0.02_5/",
          "pair_0.02_6/",
          "pair_0.02_7/",
          "pair_0.02_8/",
	  "pair_0.02_9/",
	  "pair_0.02_10/",
	  "pair_0.02_11/",
	  "pair_0.02_12/",
	  "pair_0.02_13/",
      })
    , metadatas(training_reruns, std::vector<std::vector<double>>(pairs + 1, std::vector<double>(10)))
    , kerasModels(training_reruns, std::vector<nn::KerasModel>(pairs + 1))
    , x(1e-2)
    , momfile("moms")
    , resfile("res")
{
    std::generate(model_dirs.begin(), model_dirs.end(), [n = 0]() mutable { return std::to_string(n++) + "/"; });
#ifndef NJET
    for (int i { 0 }; i < training_reruns; ++i) {
        // Near networks
        for (int j { 0 }; j < pairs; ++j) {
            std::string metadata_file { model_base + model_dirs[i] + pair_dirs[j] + "dataset_metadata.dat" };
            std::vector<double> metadata { nn::read_metadata_from_file(metadata_file) };
            for (int k { 0 }; k < 10; ++k) {
                metadatas[i][j][k] = metadata[k];
            };
            model_dir_models[i][j] = model_base + model_dirs[i] + pair_dirs[j] + "model.nnet";
	    kerasModels[i][j].load_weights(model_dir_models[i][j]);
        };
	// Cut networks
        std::string metadata_file { model_base + model_dirs[i] + cut_dirs + "dataset_metadata.dat" };
        std::vector<double> metadata { nn::read_metadata_from_file(metadata_file) };
        for (int k { 0 }; k < 10; ++k) {
            metadatas[i][pairs][k] = metadata[k];
        };
        model_dir_models[i][pairs] = model_base + model_dirs[i] + cut_dirs + "model.nnet";
	kerasModels[i][pairs].load_weights(model_dir_models[i][pairs]);
    }
#endif
#if defined(NJET) || defined(BOTH)
    const std::string f { "OLE_contract_" + std::to_string(NN4g2A::legs - 2) + "g2A.lh" };
    const char* contract { f.c_str() };
    int status;
    OLP_Start(contract, &status);
    assertm(status, "There seems to be a problem with the contract file.");
#endif
}

void NN4g2A::SquaredMatrixElement::SetParameter(const string& str, const double& val)
{
    if (str == "alpha") {
        m_alpha = val;
        m_alphacubed = pow(val, 3);
    } else if (str == "Accuracy") {
        // pass val to NJET
        m_prec = val;
    } else
        THROW(fatal_error, "Unknown parameter.");
}

void NN4g2A::SquaredMatrixElement::RecalcDependentParameters() {}

void NN4g2A::SquaredMatrixElement::PrintSummary() const
{
    msg_Info() << "Using NN4g2A Interface with the following parameters:"
               << endl
               << "  1/alpha = " << 1. / m_alpha << endl
               << "  ----------------------------------------"
               << endl;
}

// code to compute amplitude here
//double NN4g2A::SquaredMatrixElement::Calculate(const double point[NN4g2A::legs][NN4g2A::d]) const
double NN4g2A::SquaredMatrixElement::Calculate(const ATOOLS::Vec4D_Vector& point)
{
#ifdef TEST
    return 1.;
#endif

#ifdef DEBUG
    cout << "###### Calculating matrix element... ######" << endl;
#endif
#ifdef UNIT
    return 1.;
#endif

    long double s_23 = point[2][0]*point[3][0]-(point[2][1]*point[3][1]+point[2][2]*point[3][2]+point[2][3]*point[3][3]);

#ifndef NJET
    std::array<double, training_reruns> results;

    // moms is an vector of training_reruns results, each of which is an vector of FKS pairs results, each of which is an vector of flattened momenta
    std::vector<std::vector<std::vector<double>>> moms(training_reruns, std::vector<std::vector<double>>(pairs + 1, std::vector<double>(NN4g2A::legs * NN4g2A::d)));

    // flatten momenta
    for (int p { 0 }; p < NN4g2A::legs; ++p) {
        for (int mu { 0 }; mu < NN4g2A::d; ++mu) {
            // standardise input
            for (int k { 0 }; k < training_reruns; ++k) {
                for (int j { 0 }; j <= pairs; ++j) {
                    moms[k][j][p * NN4g2A::d + mu] = nn::standardise(point[p][mu], metadatas[k][j][mu], metadatas[k][j][NN4g2A::d + mu]);
                }
                moms[k][pairs][p * NN4g2A::d + mu] = nn::standardise(point[p][mu], metadatas[k][pairs][mu], metadatas[k][pairs][NN4g2A::d + mu]);
            }
        }
    }

    // cut/near check
    int cut_near { 0 };
    for (int j { 0 }; j < legs - 1; ++j) {
        for (int k { j + 1 }; k < legs; ++k) {
            const double prod { point[j][0] * point[k][0] - (point[j][1] * point[k][1] + point[j][2] * point[k][2] + point[j][3] * point[k][3]) };
            const double dist { prod / s_com };
            if (dist < delta) {
                cut_near += 1;
            }
        }
    }

    // inference
    //bool cut_network = true;
    //int pair_chosen = 8;

    for (int j { 0 }; j < training_reruns; ++j) {
        if (cut_near >= 1) {
            // the point is near an IR singularity
            // infer over all FKS pairs
            results[j] = 0;
            for (int k { 0 }; k < pairs; ++k) {
	      //if (cut_network == false && pair_chosen == k) {
       		    const double result { kerasModels[j][k].compute_output(moms[j][k])[0] };
                    const double result_pair { nn::destandardise(result, metadatas[j][k][8], metadatas[j][k][9]) };
		    results[j] += result_pair;
		    //} else {
		    //const double result_pair = 0.;
		    //results[j] += result_pair;
		    //}
            }
        } else {
            // the point is in a non-divergent region
            // use the 'cut' network which is the final entry in the pair network
	  //if (cut_network == true){
	          const double result { kerasModels[j][pairs].compute_output(moms[j][pairs])[0] };
                  results[j] = nn::destandardise(result, metadatas[j][pairs][8], metadatas[j][pairs][9]);
		  //} else {
		  //results[j] = 0.;
		  //}
	      
            
        }
    }

    const double mean { std::accumulate(results.cbegin(), results.cend(), 0.) / training_reruns };

#endif

#if defined(NJET) || defined(BOTH)
    const size_t n { 5 };
    double LHMomenta[NN4g2A::legs * n];
    for (size_t p { 0 }; p < NN4g2A::legs; ++p) {
        for (size_t mu { 0 }; mu < NN4g2A::d; ++mu) {
            LHMomenta[mu + p * n] = point[p][mu];
        }
        // Set masses
        LHMomenta[d + p * n] = 0.;
    }

    const double alphas { 0.118 };
    int alphasReturnStatus;
    OLP_SetParameter("alphas", &alphas, &zero, &alphasReturnStatus);
    assert(alphasReturnStatus == 1);

    // set alpha QED (answer changes if this changed, so does something)
    const double alpha { 1. / 137.035999084 };
    int alphaReturnStatus;
    OLP_SetParameter("alpha", &alpha, &zero, &alphaReturnStatus);
    assert(alphaReturnStatus == 1);

    const double mur { 91.188 };
    double out[11];
    double acc { 0. };
    const int channel { 1 };
    OLP_EvalSubProcess2(&channel, LHMomenta, &mur, out, &acc);

#ifdef DEBUG
    cout << "Notation: Tree     = A0.cA0" << endl;
    cout << "          Loop(-2) = 2*Re(A1.cA1)/eps^2" << endl;
    cout << "          Loop(-1) = 2*Re(A1.cA1)/eps^1" << endl;
    cout << "          Loop( 0) = 2*Re(A1.cA1)/eps^0" << endl;
    cout << endl;
    cout << "Tree            = " << out[3] << endl;
    cout << "Loop(-2)        = " << out[0] << endl;
    cout << "Loop(-1)        = " << out[1] << endl;
    cout << "Loop( 0)        = " << out[2] << endl;
    cout << "  only if NJetReturnAccuracy is used: (fractional error)" << endl;
    cout << "Loop(-2) error  = " << out[4] << endl;
    cout << "Loop(-1) error  = " << out[5] << endl;
    cout << "Loop( 0) error  = " << out[6] << endl;
    cout << endl;
#endif

    double njet_ans { out[4] };
    
#endif
    
#ifdef REC
    {
        std::ofstream o(momfile, std::ios::app);
        o.setf(std::ios_base::scientific);
        o.precision(16);
        for (int l { 0 }; l < NN4g2A::legs; ++l) {
            for (int mu { 0 }; mu < NN4g2A::d; ++mu) {
                o << point[l][mu] << " ";
            }
        }
#ifndef NJET
        o << mean << " ";
#endif
#if defined (NJET) || defined (BOTH)
        o << njet_ans << " ";
#endif
        o << '\n';
    }
#endif

#ifdef NJET
    return njet_ans;
#else
    return mean;
#endif


}

// Interface class constructor and member implementations

NN4g2A::Interface::Interface()
    : ME_Generator_Base("NN4g2A")
{
}

bool NN4g2A::Interface::Initialize(
    const string& path, const string& file,
    MODEL::Model_Base* const model,
    BEAM::Beam_Spectra_Handler* const beam,
    PDF::ISR_Handler* const isr)
{
    return true;
}

PHASIC::Process_Base*
NN4g2A::Interface::InitializeProcess(const PHASIC::Process_Info& pi, bool add)
{
    return NULL;
}

int NN4g2A::Interface::PerformTests()
{
    return 1;
}

bool NN4g2A::Interface::NewLibraries()
{
    return false;
}

void NN4g2A::Interface::SetClusterDefinitions(PDF::Cluster_Definitions_Base* const defs) {}

ATOOLS::Cluster_Amplitude*
NN4g2A::Interface::ClusterConfiguration(PHASIC::Process_Base* const proc, const size_t& mode)
{
    return NULL;
}

// Process class constructor and member implementations

NN4g2A::Process::Process(
    const PHASIC::Process_Info& pi,
    const ATOOLS::Flavour_Vector& flavs,
    const bool swap, const bool anti)
    : Tree_ME2_Base(pi, flavs)
    , m_me()
{
  //m_me.SetParameter("alpha", AlphaQED());
  //m_me.RecalcDependentParameters();

  //Data_Reader reader(" ", ";", "#", "=");

    m_me.PrintSummary();

    //rpa->gen.AddCitation(1, string("<Description of calculation> from \\cite{xxx:2019yy}"));
}

double NN4g2A::Process::Calc(const ATOOLS::Vec4D_Vector& p)
{
    if (p.size() != NN4g2A::legs)
        THROW(fatal_error, "Wrong process.");
    //double moms[NN4g2A::legs][NN4g2A::d];
    //for (size_t i { 0 }; i < NN4g2A::legs; ++i)
    //    for (size_t j { 0 }; j < NN4g2A::d; ++j)
    //        moms[i][j] = p[i][j];
    return m_me.Calculate(p);
}

int NN4g2A::Process::OrderQCD(const int& id)
{
    return NN4g2A::legs - 2;
}

int NN4g2A::Process::OrderEW(const int& id)
{
    return 2;
}

// End class member implementations

DECLARE_GETTER(NN4g2A::Interface, "NN4g2A", PHASIC::ME_Generator_Base, PHASIC::ME_Generator_Key);

PHASIC::ME_Generator_Base*
ATOOLS::Getter<PHASIC::ME_Generator_Base, PHASIC::ME_Generator_Key, NN4g2A::Interface>::
operator()(const PHASIC::ME_Generator_Key& key) const
{
    return new NN4g2A::Interface();
}

void ATOOLS::Getter<PHASIC::ME_Generator_Base, PHASIC::ME_Generator_Key, NN4g2A::Interface>::
    PrintInfo(ostream& str, const size_t width) const
{
    str << "Interface to the NN4g2A calculation";
}

using namespace PHASIC;

DECLARE_TREEME2_GETTER(NN4g2A::Process, "NN4g2A::Process")

PHASIC::Tree_ME2_Base*
ATOOLS::Getter<PHASIC::Tree_ME2_Base, PHASIC::Process_Info, NN4g2A::Process>::
operator()(const PHASIC::Process_Info& pi) const
{
    assert(pi.m_loopgenerator == "NN4g2A");
    assert(MODEL::s_model->Name() == string("SM"));
    assert(pi.m_fi.m_nloewtype == nlo_type::lo);
    assert(pi.m_fi.m_nloqcdtype == nlo_type::lo);
    Flavour_Vector fl(pi.ExtractFlavours());
    // check for g g  -> g g a a
    assert(fl[0].Kfcode() == kf_gluon && fl[1].Kfcode() == kf_gluon && fl[4].Kfcode() == kf_gluon && fl[5].Kfcode() == kf_gluon
        && fl[2].Kfcode() == kf_photon && fl[3].Kfcode() == kf_photon);
    return new NN4g2A::Process(pi, fl, 0, 0);
}
