#ifndef NN4G2A_INTERFACE_HPP
#define NN4G2A_INTERFACE_HPP

#include "ATOOLS/Org/Run_Parameter.H"
#include "MODEL/Main/Model_Base.H"
#include "MODEL/Main/Running_AlphaS.H"
#include "PHASIC++/Process/ME_Generator_Base.H"
#include "PHASIC++/Process/Tree_ME2_Base.H"

#include "njet.h"

#include <cmath>
#include <complex>
#include <iostream>
#include <string>
#include <cassert>

#include "model_fns.h"

namespace NN4g2A {

constexpr unsigned int legs { 6 };
constexpr unsigned int d { 4 };

class SquaredMatrixElement {
public:
    SquaredMatrixElement();
    void SetParameter(const std::string& str, const double& val);
    void RecalcDependentParameters();
    void PrintSummary() const;
    double Calculate(const ATOOLS::Vec4D_Vector& point);

private:
    const double zero; // for BLHA interface complex parameter entry
    static constexpr int n { 5 };        // momenta fifth entry is mass
  
    // complex mass scheme input parameters
    double m_alpha;
    // compute other SM parameters from these:
    double m_alphacubed, m_ge;
    double m_prec;
    
    const double delta;
  const double s_com;

    // n.b. there is an additional FKS pair for the cut network (for non-divergent regions)
    static constexpr int pairs { 14 };
    static constexpr int training_reruns { 20 };

    const std::string cut_dirs;
    const std::string model_base;

    std::array<std::string, training_reruns> model_dirs;
    const std::array<std::string, pairs> pair_dirs;

    std::vector<std::vector<std::vector<double>>> metadatas;
    std::array<std::array<std::string, pairs + 1>, training_reruns> model_dir_models;
    std::vector<std::vector<nn::KerasModel>> kerasModels;

    const double x;

    const std::string momfile;
    const std::string resfile;
    const std::string kinfile;
    const std::string timefile;

};

class Interface : public PHASIC::ME_Generator_Base {
public:
    // constructor
    Interface();

    // member functions
    bool Initialize(
        const std::string& path,
        const std::string& file,
        MODEL::Model_Base* const model,
        BEAM::Beam_Spectra_Handler* const beam,
        PDF::ISR_Handler* const isr);

    PHASIC::Process_Base*
    InitializeProcess(const PHASIC::Process_Info& pi, bool add);

    int PerformTests();

    bool NewLibraries();

    void SetClusterDefinitions(PDF::Cluster_Definitions_Base* const defs);

    ATOOLS::Cluster_Amplitude*
    ClusterConfiguration(PHASIC::Process_Base* const proc, const size_t& mode);
};

class Process : public PHASIC::Tree_ME2_Base {
protected:
    SquaredMatrixElement m_me;

public:
    Process(
        const PHASIC::Process_Info& pi,
        const ATOOLS::Flavour_Vector& flavs,
        const bool swap,
        const bool anti);

    double Calc(const ATOOLS::Vec4D_Vector& p);

    int OrderQCD(const int& id);

    int OrderEW(const int& id);
};

} // End namespace NN4g2A

#endif // NN4G2A_INTERFACE_HPP
