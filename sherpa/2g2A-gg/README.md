---
title: gg $\rightarrow$ g$\gamma \gamma$ with custom analysis using four-gluon tree for integration
author: Ryan Moodie
date: 14 Feb 2020
---

## Results

* Failure
* <https://www.ippp.dur.ac.uk/~rmoodie/files/research/2g2A-gg/>

## Usage

### Running

* Run on IPPP system
* Uses library from [previous 2g2A](https://gitlab.com/eidoom/njet-tools/tree/master/sherpa/NJET0q2gAA)
    * Run `make libSherpaNJET0q2gAA.so` there first
    * Files are symlinked here
* Does grid optimisation using quark initiated tree
    * `make Results.db`
* Uses [custom Rivet analysis](https://gitlab.com/eidoom/njet-tools/tree/master/sherpa/analysis-diphoton)
    * Run `make` there first
* Then do analysis here. 
    * On IPPP system,
        * `make analysis` or `./local-analyses <init seed> <num runs> <total num events>`
        * `make copy`

### Cleaning

* Delete temporary files with `make flush`.
* Get rid also of output with `make clean`
* Get rid also of analysis with `make reset`
* Also get rid of pdfs with `make wipe`
