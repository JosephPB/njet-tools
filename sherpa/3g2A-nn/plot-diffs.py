#!/usr/bin/env python3
# coding=UTF-8

import pickle

import matplotlib.pyplot


class Plots:
    def __init__(self):
        with open("res.pickle", "rb") as f:
            (
                self.nn,
                self.njet,
                self.logratio,
                self.err_abs,
                self.err_rel,
                self.cut,
            ) = pickle.load(f)

        # print(sum([1 for a in self.ratio if a<0]))
        # print(sum(self.cut)/len(self.cut))
        # print(len(self.cut))
        # print(sum(self.cut)/len(self.cut))
        # print(max(self.nn))
        # print(min(self.nn))
        # print(sum(self.nn) / len(self.nn))

    def all_diff(self):
        fig, ax = matplotlib.pyplot.subplots()

        ax.hist(self.logratio, density=True, bins=150, histtype="step")
        ax.set_xlabel("ln(|NN/NJet|)")
        ax.set_ylabel("Fraction of sample")
        ax.set_title(f"Diff without JADE cuts. Sample size: {len(self.logratio)}.")

        # fig.savefig("diff.pdf", bbox_inches="tight", format="pdf")
        fig.savefig("all_diff.png", bbox_inches="tight", format="png", dpi=300)

    def diff(self):
        fig, ax = matplotlib.pyplot.subplots()

        accepted = [a for a, b in zip(self.logratio, self.cut) if not b]
        perc = 100 * len(accepted) / len(self.cut)

        ax.hist(accepted, bins=100)
        ax.set_xlabel("ln(|NN/NJet|)")
        ax.set_ylabel("Frequency")
        ax.set_title(
            f"Diff with JADE cuts. Sample size: {len(accepted)} ({int(perc)}% of all)."
        )

        # fig.savefig("diff.pdf", bbox_inches="tight", format="pdf")
        fig.savefig("diff.png", bbox_inches="tight", format="png", dpi=300)


if __name__ == "__main__":
    p = Plots()
    p.diff()
    # p.all_diff()
