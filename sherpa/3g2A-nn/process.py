#!/usr/bin/env python3
# coding=UTF-8

import pickle
import pathlib


class Getter:
    def __init__(self):
        self.res = "100k_1e-16_sherpa+JADE/res"
        respic = "res.pickle"
        resdata = self.get_res()

        self.sijs = "100k_1e-16_sherpa+JADE/sijs"
        sijspic = "sijs.pickle"
        sijsdata = self.get_sijs()

        while True:
            try:
                i = resdata[1].index(float("inf"))
                print(f"Chucking {i}th point - divergent.")
                for lst in resdata:
                    lst.pop(i)
            except ValueError:
                break

        self.write(respic, resdata)
        self.write(sijspic, sijsdata)

    def write(self, filename, data):
        if pathlib.Path(filename).is_file():
            print(f"File {filename} already exists.")
            if input("Overwrite? [y for yes] ") != "y":
                return None
        with open(filename, "wb") as f:
            pickle.dump(data, f)

    def get_sijs(self):
        with open(self.sijs, "r") as datafile:
            sijs = [
                float(a)
                for line in [
                    line.rstrip("\n").split(" ")[:-1] for line in datafile.readlines()
                ]
                for a in line
            ]
        return sijs

    def get_res(self):
        with open(self.res, "r") as datafile:
            data = [line.rstrip("\n").split(" ") for line in datafile.readlines()]

        nn, njet, diff, err_abs, err_rel, cut = zip(*data)

        nn = [float(a) for a in nn]
        njet = [float(a) for a in njet]
        diff = [float(a) for a in diff]
        err_abs = [float(a) for a in err_abs]
        err_rel = [float(a) for a in err_rel]
        cut = [(a == "1") for a in cut]

        return nn, njet, diff, err_abs, err_rel, cut


if __name__ == "__main__":
    g = Getter()
