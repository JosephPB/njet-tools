#!/usr/bin/env python2.7
# coding=UTF-8

import matplotlib
matplotlib.use('Agg')

import yoda

class GetHistos:
    def __init__(self, filename):
        aos = yoda.read("Analysis.yoda", "/diphoton/.*")

        with open(filename, 'w') as f:
            for t, h in aos.items():
                if (("diphoton" in t) and not ("RAW" in t)):
                    f.write(t + "\n")
                    for b in h.bins:
                        f.write("{} {}\n".format(b.xMid, b.height))
                        f.write("\n")

if __name__ == "__main__":
    gh = GetHistos("histos")
