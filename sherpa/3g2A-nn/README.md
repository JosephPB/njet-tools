---
title: gg $\rightarrow$ g$\gamma \gamma$ from a neural net
author: Ryan Moodie
date: 11 Jun 2020
---

* CoM=1 TeV
* Uses JADE cuts

## Results

* <https://www.ippp.dur.ac.uk/~rmoodie/files/research/3g2A-nn/>

## Usage

### Running

* The following files are symlinked here
    * Uses interface from [NN2g3A](https://gitlab.com/eidoom/njet-tools/tree/master/sherpa/NN3g2A)
        * Run `make -j` there first
    * Uses [custom Rivet analysis](https://gitlab.com/eidoom/njet-tools/tree/master/sherpa/analysis-diphoton)
        * Run `make` there first
* On IPPP system,
    * Generate integrator:
        * `make Results.db`
    * Then do analysis:
        * Either:
            * SLURM: `make analysis-batch`
            * Local: `make analysis-local`
        * `make Analysis.yoda`
        * `make rivet-plots/`
        * `make copy`
* On laptop,
    * Run with `make test`, or just `Sherpa`
    * `make show`

### Cleaning

* Delete temporary files with `make clean`
* Get rid also of analysis with `make reset`
* Also get rid of pdfs with `make wipe`
