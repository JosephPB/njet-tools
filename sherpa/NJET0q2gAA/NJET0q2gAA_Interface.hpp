#ifndef NJET0q2GAA_INTERFACE_HPP
#define NJET0q2GAA_INTERFACE_HPP

#include "ATOOLS/Org/Run_Parameter.H"
#include "MODEL/Main/Model_Base.H"
#include "MODEL/Main/Running_AlphaS.H"
#include "PHASIC++/Process/ME_Generator_Base.H"
#include "PHASIC++/Process/Tree_ME2_Base.H"

#include "njet.h"

#include <cmath>
#include <complex>
#include <iostream>
#include <string>
#include <cassert>

namespace NJET0q2gAA {

constexpr unsigned int legs { 4 };
constexpr unsigned int d { 4 };

class SquaredMatrixElement {
public:
    SquaredMatrixElement();
    void SetParameter(const std::string& str, const double& val);
    void RecalcDependentParameters();
    void PrintSummary() const;
    double Calculate(const double point[d][legs]) const;

private:
    // complex mass scheme input parameters
    double m_alpha;
    // compute other SM parameters from these:
    double m_alphacubed, m_ge;
    double m_prec;
};

class Interface : public PHASIC::ME_Generator_Base {
public:
    // constructor
    Interface();

    // member functions
    bool Initialize(const std::string& path, const std::string& file,
        MODEL::Model_Base* const model,
        BEAM::Beam_Spectra_Handler* const beam,
        PDF::ISR_Handler* const isr);

    PHASIC::Process_Base*
    InitializeProcess(const PHASIC::Process_Info& pi, bool add);

    int PerformTests();

    bool NewLibraries();

    void SetClusterDefinitions(PDF::Cluster_Definitions_Base* const defs);

    ATOOLS::Cluster_Amplitude*
    ClusterConfiguration(PHASIC::Process_Base* const proc, const size_t& mode);
};

class Process : public PHASIC::Tree_ME2_Base {
protected:
    SquaredMatrixElement m_me;

public:
    Process(
        const PHASIC::Process_Info& pi,
        const ATOOLS::Flavour_Vector& flavs,
        const bool swap, const bool anti);

    double Calc(const ATOOLS::Vec4D_Vector& p);

    int OrderQCD(const int& id);

    int OrderEW(const int& id);
};

} // End namespace NJET0q2gAA

#endif // NJET0q2GAA_INTERFACE_HPP
