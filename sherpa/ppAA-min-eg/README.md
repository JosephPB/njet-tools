# ppAA minimal example

Uses Sherpa matrix element generator.
Only does hard scattering matrix element, no PDFs, showering, etc.
Runs.
Returns inclusive cross-section.
Also does analysis and makes rivet plots.
Run card based on [this one](https://sherpa.hepforge.org/doc/SHERPA-MC-2.2.8.html#LHC_005fZJets).
Used recommended settings by Alan.

Run calculation with `make`.
Show plots with `make show`.

[plots](https://www.ippp.dur.ac.uk/~rmoodie/files/research/2p2A/ATLAS_2017_I1591327/index.html)
