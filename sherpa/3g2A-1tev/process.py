#!/usr/bin/env python3
# coding=UTF-8

import pickle
import pathlib


class Getter:
    def __init__(self):
        self.res = "res"
        respic = self.res + ".pickle"
        resdata = self.get_res()

        self.mom = "moms"
        mompic = self.mom + ".pickle"
        momdata = self.get_mom()

        momdata = momdata[len(momdata) - len(resdata) :]

        while True:
            try:
                i = resdata.index(float("inf"))
                print(f"Chucking {i}th point - divergent.")
                resdata.pop(i)
                momdata.pop(i)
            except ValueError:
                break

        self.write(respic, resdata)
        self.write(mompic, momdata)

    def write(self, filename, data):
        if pathlib.Path(filename).is_file():
            print("File already exists.")
            if input("Overwrite? [y for yes] ") != "y":
                return None
        with open(filename, "wb") as f:
            pickle.dump(data, f)

    def get_res(self):
        with open(self.res, "r") as datafile:
            data = [line.strip(" ").rstrip("\n") for line in datafile.readlines()]

        njet = [float(a) for a in data]

        return njet

    def get_mom(self):
        with open(self.mom, "r") as datafile:
            data = [
                line.strip().rstrip("\n").split(" ") for line in datafile.readlines()
            ]

        out = []
        tmp = []
        for line in data:
            if line == [""]:
                out.append(tmp)
                tmp = []
            else:
                tmp.append([float(a) for a in line])

        return out


if __name__ == "__main__":
    g = Getter()
