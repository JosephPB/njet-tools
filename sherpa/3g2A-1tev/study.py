#!/usr/bin/env python3
# coding=UTF-8

import pickle


class Plots:
    def __init__(self):
        with open("res.pickle", "rb") as f:
            self.njet = pickle.load(f)

        i = self.njet.index(max(self.njet))
        print(self.njet[i])
        print()

        with open("moms.pickle", "rb") as f:
            self.moms = pickle.load(f)

        # print(max(self.njet[500000:1500000]))
        # print(len([a for a in self.njet if a > 1e4]))
        # print(sum([a for a in self.njet if a < 1e2]))
        # print(max([a for a in self.njet if a != float('inf')]))

        # print(self.mean(1e-1))
        # print(self.mean(1e0))
        # print(self.mean(1e4))
        # print(self.mean(1e5))
        # print(self.mean(1e10))

        mom = self.moms[i]

        for i in range(len(mom)):
            print(f"p{i}={mom[i]}")
        print()

        for i in range(len(mom)):
            for j in range(i + 1, len(mom)):
                sij = 2 * self.dot(mom[i], mom[j])
                print(f"s{i+1}{j+1}={sij}")
        print()

    def mean(self, cutoff):
        l1e2 = [a for a in self.njet if a < cutoff]
        return sum(l1e2) / len(l1e2)

    def dot(self, mom1, mom2):
        return mom1[0] * mom2[0] - (
            mom1[1] * mom2[1] + mom1[2] * mom2[2] + mom1[3] * mom2[3]
        )


if __name__ == "__main__":
    p = Plots()
