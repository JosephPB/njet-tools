#!/usr/bin/env python3
# coding=UTF-8

import pickle
import math

import matplotlib.pyplot


class Plots:
    def __init__(self):
        with open("res.pickle", "rb") as f:
            njet = pickle.load(f)

        # cut = float('inf')
        cut = 1e-2
        discarded = [a for a in njet if a >= cut]
        njet = [a for a in njet if a < cut]
        le = len(njet)
        mean = sum(njet) / le
        sds = [(a - mean) ** 2 for a in njet]

        print(max(sds))
        exit()

        sd = math.sqrt(sum(sds) / (le * (le - 1)))
        res = [(a - mean) / sd for a in njet]

        fig, ax = matplotlib.pyplot.subplots()

        ax.hist(res, bins=100, histtype="step", log=True, label="(value - mean)/standard_deviation")
        ax.legend(loc="best")
        ax.set_xlabel("std($|A|^2$)")
        ax.set_ylabel("Number")
        ax.set_title(f"Standardised NJet results. Cutoff: {cut}. Number discarded {len(discarded)}.")

        fig.savefig(f"standardised_{cut}.png", bbox_inches="tight", format="png", dpi=300)


if __name__ == "__main__":
    p = Plots()
