---
title: gg $\rightarrow$ g$\gamma \gamma$ at 1 TeV
author: Ryan Moodie
date: 25 Jun 2020
---

* Includes JADE cuts for comparison to [NN](https://gitlab.com/eidoom/njet-tools/tree/master/sherpa/3g2A-nn).

## Results

* <https://www.ippp.dur.ac.uk/~rmoodie/files/research/3g2A-1tev/>

## Usage

### Running

* Uses interface from [NN3g2A](https://gitlab.com/eidoom/njet-tools/tree/master/sherpa/NN3g2A)
    * Run `make libSherpaNJet3g2A.so` there first
    * Note that it's symlinked as `libSherpaNN3g2A.so` to match the interface name `NN3g2A`, but it used NJet, not NN, by #macros (just to keep everything in the same file so I'm sure there's no differences)
* Uses contract file from [NJET0q3gAA](https://gitlab.com/eidoom/njet-tools/tree/master/sherpa/NJET0q3gAA)
    * Run `make OLE_contract_3g2A.lh` there first
* Above files are symlinked here
* Uses [custom Rivet analysis](https://gitlab.com/eidoom/njet-tools/tree/master/sherpa/analysis-diphoton)
    * Run `make Rivetdiphoton.so` there first
* Generate integration grid:
    * `make Results.db`
* Then do analysis here. 
    * On IPPP system,
        * Either:
            * SLURM: `make analysis-batch`
            * Local: `make analysis-local`
        * `make Analysis.yoda`
        * `make rivet-plots/`
        * `make copy`
    * On laptop,
        * Test with `Sherpa`
        * `make show`

### Cleaning

* Get rid of output with `make clean`
* Also get rid of pdfs with `make wipe`
