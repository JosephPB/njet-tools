N=4
RUNCARD=Run.dat
LIB=libSherpaNJET0q${N}gAA.so
START?=1
JOBS?=200
EVENTS?=1000000
CORES?=$(shell nproc)

.PHONY: clean reset wipe all show test flush analysis

all: analysis

include ../../Makefile.inc

# Generate matrix element mapping/process information
Process/: $(RUNCARD) $(LIB) OLE_contract_$(N)g2A.lh
	Sherpa -f $< INIT_ONLY=1 -l process.log || exit 0

# Do integration (grid optimisation)
Results.db: $(RUNCARD) Process/
	mpirun -n $(CORES) Sherpa -f $< -e 0 -a 0 -l integration.log

# Event generation and analysis on IPPP system - on laptop, just do `Sherpa -f $(RUNCARD)`
analysis: $(RUNCARD) Results.db
	./analyses.sh $(START) $(JOBS) $(EVENTS)

# After above, on IPPP system, you must merge yoda results from each job with:
Analysis.yoda: part.*.yoda
	yodamerge -o $@ $^

# Generate the plots with Rivet, using custom analysis
rivet-plots/: Analysis.yoda 
	RIVET_ANALYSIS_PATH=../analysis-diphoton rivet-mkhtml -n $(CORES) $<

# Show the plots
show: rivet-plots/
	chromium-browser $<index.html

# Remove temporary files (mainly from batch usage)
flush:
	rm -rf part.*.yoda *.out *.err *.log Status__* Sherpa_References.tex

# Remove compiled files
clean: flush
	rm -rf rivet-plots 

# Remove results of calculation
reset: clean
	rm -rf Process Results.db Results.db.bak Analysis.yoda Events.hepmc2g

# Remove everything
wipe: reset
	rm *.pdf
