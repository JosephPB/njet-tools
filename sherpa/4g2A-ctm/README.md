---
title: gg $\rightarrow$ gg$\gamma \gamma$
author: Ryan Moodie
date: 3 Feb 2020
---

## Results

* <https://www.ippp.dur.ac.uk/~rmoodie/files/research/4g2A/diphoton/index.html>

## Usage

### Running

* See `Makefile`

### Cleaning

* Delete temporary files created by batch execution with `make flush`.
* Get rid of compilations with `make clean`
* Get rid also of matrix element generation and integration (grid optimisation) with `make reset`
* Also get rid of pdfs with `make wipe`

## Details

* Uses PDFs
* Uses jets (FastJet, antikt)
* Does nothing else fancy with final states (no showering, hadronisation)
