#!/usr/bin/env bash

if [ ! -f "hepmc/Events.hepmc2g" ]; then
    cd hepmc || exit 1
    make
    cd .. || exit 1
fi

l=(0 50 100 200)

for a in ${l[@]}; do
    for b in ${l[@]}; do
        for c in ${l[@]}; do
            dir="myy${a}-ptyy${b}-ptj${c}"
            pln="myy${a}_ptyy${b}_ptj${c}"

            if [ ! -d "${dir}" ]; then
                mkdir ${dir}

                cd ${dir} || exit 1
                ln -sf ../template/Makefile .

                ln -sf ../../NJET0q3gAA/OLE_contract_3g2A.lh .
                ln -sf ../../NN3g2A/libSherpaNJet3g2A.so libSherpaNN3g2A.so

                cp ../template/analysis.sh .

                perl -pi -e "s|(-a diphoton)|\1_${pln}|g" "analysis.sh"
                perl -pi -e "s|(--analysis-path ../../analysis-cuts)|\1/${dir}|g" "analysis.sh"

                make analysis

                cd .. || exit 1
            fi
        done
    done
done

wait
