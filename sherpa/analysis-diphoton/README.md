# rivet analysis: diphoton

Docs: <https://gitlab.com/hepcedar/rivet/blob/master/doc/tutorials/simple-analysis.md>

Analysis was initialised with
```shell
rivet-mkanalysis diphoton
```

Find name with
```shell
rivet --list-analyses --analysis-path=$PWD | tail -1
```
