---
title: gg $\rightarrow$ g$\gamma \gamma$ at 1 TeV
author: Joseph Bullock
date: 23 Feb 2020
---

## Usage

### Running

* Uses interface from [NN4g2A](https://gitlab.com/eidoom/njet-tools/tree/master/sherpa/NN4g2A)
    * Run `make libSherpaNJet4g2A.so` there first
    * Note that it's symlinked as `libSherpaNN4g2A.so` to match the interface name `NN4g2A`, but it used NJet, not NN, by #macros (just to keep everything in the same file so I'm sure there's no differences)
* Uses contract file from [NJET0q4gAA](https://gitlab.com/eidoom/njet-tools/tree/master/sherpa/NJET0q4gAA)
    * Run `make OLE_contract_4g2A.lh` there first
* Above files are symlinked here
* Uses [custom Rivet analysis](https://gitlab.com/eidoom/njet-tools/tree/master/sherpa/analysis-diphoton)
    * Run `make Rivetdiphoton.so` there first
* Generate integration grid:
    * `make Results.db`
* Then do analysis here. 
    * On IPPP system,
        * Either:
            * SLURM: `make analysis-batch`
            * Local: `make analysis-local`
        * `make Analysis.yoda`
        * `make rivet-plots/`
        * `make copy`
    * On laptop,
        * Test with `Sherpa`
        * `make show`

### Cleaning

* Get rid of output with `make clean`
* Also get rid of pdfs with `make wipe`
