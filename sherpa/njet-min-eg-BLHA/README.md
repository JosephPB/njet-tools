---
title: Minimal example of using NJet to provide matrix elements for Sherpa
author: Ryan Moodie
date: 4 Dec 2019
---

# Current state

* Only born and virtual have been set up.

# Running

* Run with 

```sh
make <NLOpart>
```

where  

`<NLOpart>`|process
-----------|-------
`B`        |born
`V0`,`V1`  |virtual
`I`        |integrated
`RS`       |real subtracted

* For virt, do `<NLOpart>`=`V0` then `V1`. i.e.

```sh
make V0
make V1
```

Cancel `V0` when it hits infinite loop (which is fine).
`V0` is simply to generate the order file.
`V1` then runs the calculation.

* mpiexec doesn't work, use /usr/lib64/openmpi/bin/mpiexec instead

# Further references

* [Sherpa external OLP](https://sherpa.hepforge.org/doc/SHERPA-MC-2.2.8.html#External-one_002dloop-ME)

* [Fixed order NLO event generation with NJet+Sherpa](https://bitbucket.org/njet/njet/wiki/NJetSherpa/NJetSherpa)

* [Rivet diphoton example](https://rivet.hepforge.org/analyses/ATLAS_2017_I1591327.html)
