.PHONY: all flush clean

all:

flush:
	rm -rf Result

clean:
	rm -rf Status__*
	rm -rf Process/
	rm -rf Result/
	rm -rf Event/
	rm -rf Analysis/
	rm -rf Plots/
	rm -rf plots/
	rm -f Results.db*
	rm -f result.db*
	rm -f Sherpa_References.tex
	rm -f makelibs
	rm -f SConstruct
	rm -f .sconsign.dblite
	rm -f *.lh
	rm -f event.root
	rm -f analysis.yoda

