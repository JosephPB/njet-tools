#include <bitset>
#include <cassert>
#include <climits>
#include <complex>
#include <iostream>
#include <string>

#include <qd/qd_real.h>

std::bitset<sizeof(float) * CHAR_BIT> float2binary(float input)
{
    // https://stackoverflow.com/a/474058/5922871
    // std::cout << sizeof(float) << '\n';
    // std::cout << sizeof(int) << '\n';
    // union {
    //     float inp;
    //     int out;
    // } data;
    // data.inp = input;
    // std::bitset<sizeof(float) * CHAR_BIT> bits(data.out);
    // return bits;

    // https://stackoverflow.com/a/7533881/5922871
    assert(sizeof(float) <= sizeof(int));
    return std::bitset<sizeof(float) * CHAR_BIT>(*reinterpret_cast<unsigned int*>(&input));
}

std::bitset<sizeof(double) * CHAR_BIT> double2binary(double input)
{
    assert(sizeof(double) <= sizeof(unsigned long int));
    return std::bitset<sizeof(double) * CHAR_BIT>(*reinterpret_cast<unsigned long int*>(&input));
}

// // https://stackoverflow.com/a/52630845/5922871
// float cast_down(double N)
// {
//     const int el { 11 };
//     const int fl { 52 };
//     const int Ebias { 1023 };
//     const int Emax { 2047 };
//     int Fscale{0x1p-52};
//     // get bit representation of float
//     std::bitset<sizeof(double) * CHAR_BIT> n { double2binary(N) };
//     int s { n[0] };
//     std::bitset<el> eb;
//     for (int i { 0 }; i < el; ++i) {
//         eb[i] = n[1 + i];
//     }
//     unsigned long int e { eb.to_ulong() };
//     std::bitset<fl> fb;
//     for (int i { 0 }; i < fl; ++i) {
//         fb[i] = n[1 + el + i];
//     }
//     unsigned long int f { fb.to_ulong() };

//     // Interpret the sign.
//     double S = s ? -1 : +1;

//     // Classify the exponent.
//     if (e == 0)
//         // The value is zero or subnormal.
//         return S * std::ldexp(f * Fscale, 1 - Ebias);

//     else if (e < eMax)
//         // The value is normal.
//         return S * std::ldexp(1 + f * Fscale, e - Ebias);

//     // else
//     //     // The value is NaN or infinite.
//     //     if (f == 0)
//     //     // The value is infinite.
//     //     return S * INFINITY;
//     // else
//     //     // The value is a NaN.
//     return NAN;
// }

// int main(int argc, char* argv[])
int main()
{
    const int len { 64 };
    std::cout.precision(len);
    std::cout.setf(std::ios_base::scientific);

    qd_real a = "1.234567890123456789012345678901234567890123456789012345678901234567890";
    std::cout << " 0 " << a << '\n';

    qd_real b = 1.234567890123456789012345678901234567890123456789012345678901234567890;
    std::cout << " 1 " << b << '\n';

    qd_real c { "1.234567890123456789012345678901234567890123456789012345678901234567890" };
    std::cout << " 2 " << c << '\n';

    qd_real d { 1.234567890123456789012345678901234567890123456789012345678901234567890 };
    std::cout << " 3 " << d << '\n';

    qd_real e;
    e = "1.234567890123456789012345678901234567890123456789012345678901234567890";
    std::cout << " 4 " << e << '\n';

    e = 1.234567890123456789012345678901234567890123456789012345678901234567890;
    std::cout << " 5 " << e << '\n';

    std::cout << " 6 " << 1.234567890123456789012345678901234567890123456789012345678901234567890 << '\n';

    std::cout << " 7 " << qd_real(1.234567890123456789012345678901234567890123456789012345678901234567890) << '\n';

    std::cout << " 8 " << qd_real("1.234567890123456789012345678901234567890123456789012345678901234567890") << '\n';

    std::cout << " 9 " << static_cast<qd_real>(1.234567890123456789012345678901234567890123456789012345678901234567890) << '\n';

    std::cout << "10 " << static_cast<qd_real>("1.234567890123456789012345678901234567890123456789012345678901234567890") << '\n';

    qd_real f { 1. + 1. };
    std::cout << "13 " << f << '\n';

    qd_real g { 1. * 3. };
    std::cout << "14 " << g << '\n';

    qd_real h { 1. / 3. };
    std::cout << "14 " << h << '\n';

    qd_real i { 1. / qd_real(3.) };
    std::cout << "15 " << i << '\n';

    qd_real j { qd_real(1.) / qd_real(3.) };
    std::cout << "17 " << j << '\n';

    std::complex<qd_real> k(0, 1.234567890123456789012345678901234567890123456789012345678901234567890);
    std::cout << "17 " << k << '\n';

    std::complex<qd_real> l(0, "1.234567890123456789012345678901234567890123456789012345678901234567890");
    std::cout << "18 " << l << '\n';

    std::complex<qd_real> m { std::complex<qd_real>(0, "1.234567890123456789012345678901234567890123456789012345678901234567890") };
    std::cout << "19 " << m << '\n';

    // std::complex<qd_real> o { std::complex<qd_real>(0, '1.234567890123456789012345678901234567890123456789012345678901234567890') };
    // std::cout << "19 " << o << '\n';

    // how to keep this general in templates? double won't accept the ""
    std::cout << "20 " << double(1.111111111111111111111111111111111111111111111111111111111111111111111) << '\n';

    // "ly (& with =)
    double n { 1.111111111111111111111111111111111111111111111111111111111111111111111 };
    std::cout << "21 " << n << '\n';

    const std::string blk(2 * len + 21, '=');
    std::cout << blk << '\n';

    qd_real Q { "1.234567890123456789012345678901234567890123456789012345678901234567890" };
    std::cout << "  Q " << Q << '\n';
    dd_real D { "1.234567890123456789012345678901234567890123456789012345678901234567890" };
    std::cout << "  D " << D << '\n';
    double S { 1.234567890123456789012345678901234567890123456789012345678901234567890 };
    std::cout << "  S " << S << '\n';
    // all these fail (reducion in precision cast)
    // // static cast
    // std::cout << "DsS " << static_cast<double>(D) << '\n';
    // // functional cast
    // std::cout << "DfS " << double(D) << '\n';
    // // C-style cast
    // std::cout << "DcS " << (double)D << '\n';
    // // initialisation with conversion
    // double DiS = D;
    // std::cout << cast_down(1.) << '\n';
    std::cout << sizeof(S) << '\n';
    std::cout << sizeof(D) << '\n';
    std::cout << sizeof(S*D) << '\n';
}
