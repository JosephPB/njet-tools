LOCAL?=$(HOME)/local
GIT?=$(HOME)/git
NJET_LOCAL?=$(LOCAL)/njet
# VC_LOCAL?=$(LOCAL)/Vc

CXX=LD_LIBRARY_PATH=$(NJET_LOCAL)/lib g++
# CXX+= -pg
CXXFLAGS=-g -O2 -Wall -Wextra -std=c++17
CPPFLAGS=-I$(NJET_LOCAL)/include -I$(GIT)/njet-develop 
# CPPFLAGS+=-I$(VC_LOCAL)/include
LDFLAGS=-L$(NJET_LOCAL)/lib 
# LDFLAGS+=-L$(VC_LOCAL)/lib
# DEFS=-DUSE_DD -DUSE_QD 
DEFS+=-DDEBUG
# DEFS+=-DUSE_VC
LIBS=-lnjet2 -lnjet2tools #-lqd 
# LIBS+=-lVc
CPPFLAGS+=`pkg-config PentagonFunctions --cflags`
LDFLAGS+=`pkg-config PentagonFunctions --libs`
LIBS+=-lLi2++

SRCS=$(wildcard *.cpp)
PRGS=$(basename $(SRCS)) 
DEPS=$(wildcard *.hpp) $(wildcard *.ipp) $(wildcard ../common/*.hpp) $(wildcard ../common/*.ipp)

.PHONY: clean all wipe flush reset

all: OLE_contract_diphoton.lh calcPoints parPoints generateMomenta convMom

convMom: %: %.o
	$(CXX) -o $@ $^

calcPoints: %: %.o
	$(CXX) -o $@ $^ $(LDFLAGS) $(LIBS)

parPoints: %: %.o
	$(CXX) -o $@ $^ $(LDFLAGS) $(LIBS) 

generateMomenta: %: %.o
	$(CXX) -o $@ $^ $(LDFLAGS) $(LIBS)

%.o: %.cpp $(DEPS)
	$(CXX) $(CXXFLAGS) -o $@ -c $< $(CPPFLAGS) $(DEFS)

flush:
	rm -f *.out *.err *.part

clean: flush
	rm -f *.o __pycache__

wipe: clean
	rm -f $(PRGS) OLE_contract* 

reset: wipe
	rm -f *.dat *.mom *.res

OLE_contract_%.lh: OLE_order_%.lh
	njet.py -o $@ $<
