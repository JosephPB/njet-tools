template <int mul>
calcPoints<mul>::calcPoints(const std::string contract_, const std::string momFile_, const std::string resultsFile_)
    : contract(contract_)
    , momFile(momFile_)
    , resultsFile(resultsFile_)
    , momenta(convMom::getMom<double, mul>(momFile))
{
    std::cout << "\n"
              << "  NJet: simple use of the BLHA interface for " << std::to_string(mul - 2) << "g2A\n"
              << "  nb. loop induced, so A0 vanishes\n"
              << "  Using BLHA contract file " << this->contract << "\n"
              << "  Phase space points will be read from " << this->momFile << "\n"
              << "  Results will be written to " << this->resultsFile << "\n"
              << "\n";

    int contractRstatus;
    OLP_Start(this->contract.c_str(), &contractRstatus);
    assert(contractRstatus);

    char olpname[15];
    char olpversion[15];
    char olpmessage[255];
    OLP_Info(olpname, olpversion, olpmessage);
    std::cout << "Running " << olpname
              << " version " << olpversion
              << " note " << olpmessage
              << "\n";

    std::cout << "\nNotation:\n"
#ifdef DEBUG
              << "          O(e^-4) = A1.cA1 @ O(1/eps^2)\n"
              << "          O(e^-3) = A1.cA1 @ O(1/eps^1)\n"
              << "          O(e^-2) = A1.cA1 @ O(1/eps^2)\n"
              << "          O(e^-1) = A1.cA1 @ O(1/eps^1)\n"
#endif
              << "          O(1)    = A1.cA1 @ O(eps^0)\n"
              << "          Errors are fractional\n"
              << "          zero indexing used throughout\n\n";
}

template <int mul>
void calcPoints<mul>::write()
{
    std::ofstream o(this->resultsFile, std::ios::app);
    o.setf(std::ios_base::scientific);
    o.precision(16);
    for (auto& result : this->results) {
        // result, relative (fractional/decimal) error, absolute error
        o << result[0] << "  " << result[1] << "  " << result[2] << '\n';
    }
}

template <int mul>
void calcPoints<mul>::calc(const int pt, const bool verbose)
{
    if (verbose) {
        std::cout << "==================== Test point " << pt << " ====================\n";
    }

    std::array<std::array<double, this->d>, mul> point { this->momenta[pt] };

    double LHMomenta[mul * this->n];
    for (int p { 0 }; p < mul; ++p) {
        for (int mu { 0 }; mu < this->d; ++mu) {
            LHMomenta[mu + p * this->n] = point[p][mu];
        }
        // Set masses
        LHMomenta[this->d + p * this->n] = 0.;
    }

#ifdef DEBUG
    ps::printPhaseSpacePoint<double, mul>(point, verbose);
    if (verbose) {
        std::cout << "\n";
    }

    ps::checkPhaseSpacePoint<double, mul>(point, verbose);
    if (verbose) {
        std::cout << "\n";
    }
#endif // DEBUG

    const int channel { 1 };
    /* const int channels { 1 }; */
    /* for (int channel { 1 }; channel <= channels; ++channel) { */
    double out[11];
    double acc { 0. };

    const double zero { 0. };
    const double mur { 91.188 };

    int alphaSrStatus;
    const double alphas { 0.118 };
    OLP_SetParameter("alphas", &alphas, &zero, &alphaSrStatus);
    assert(alphaSrStatus == 1);

    int alphaRstatus;
    const double alpha { 1. / 137.035999084 };
    OLP_SetParameter("alpha", &alpha, &zero, &alphaRstatus);
    assert(alphaRstatus == 1);

    auto t1 { std::chrono::high_resolution_clock::now() };
    OLP_EvalSubProcess2(&channel, LHMomenta, &mur, out, &acc);
    auto t2 { std::chrono::high_resolution_clock::now() };

    /* std::cout << "---- channel number " << channel << " ----\n"; */

    if (verbose) {
#ifdef DEBUG
        for (int i { 0 }; i < 4; ++i) {
            std::cout << "O(e^-" << (4 - i) << ") = " << (out[i] < 0 ? "" : " ") << out[i] << " +- " << out[6 + i] << " f\n";
        }
#endif
        std::cout << "O(1)    = " << (out[4] < 0 ? "" : " ") << out[4] << " +- " << out[10] << " f\n\n"
                  << "Time: " << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count() << "ms\n";
    }

    // result finite part, fractional error, absolute error
    this->results.push_back({ out[4], out[10], out[10] * out[4] });
    /* } */
    if (verbose) {
        std::cout << "\n";
    }
}

template <int mul>
void calcPoints<mul>::calcLoop(const bool verbose)
{
    const int num { static_cast<int>(this->momenta.size()) };
    auto t0 { std::chrono::high_resolution_clock::now() };
    for (int pt { 0 }; pt < num; ++pt) {
        this->calc(pt, verbose);
    }
    auto t3 { std::chrono::high_resolution_clock::now() };
    if (verbose) {
        std::cout << "Total time for " << num << " points: " << std::chrono::duration_cast<std::chrono::milliseconds>(t3 - t0).count() << "ms\n\n";
    }
    this->write();
}

template <int mul>
void calcPoints<mul>::calcSingle(const int pt)
{
    this->calc(pt);
    this->write();
}
