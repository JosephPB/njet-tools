#ifndef NJET_EXAMPLES_PHASESPACE_H
#define NJET_EXAMPLES_PHASESPACE_H

#include <cassert>
#include <cmath>
#include <iostream>
#include <vector>

#include "ngluon2/Model.h"
#include "tools/PhaseSpace.h"

#include "common.hpp"

namespace ps {

    template <typename T>
        const std::vector<MOM<T>>
        getMasslessPhaseSpacePoint(const int mul, const bool verbose = false, const int rseed = 1, const double sqrtS = 1.);

    // BROKEN - needs complex momenta
    // hard coded single collinear phase-space point for 2->2
    // collinear in 0||1 (s12->0) as theta -> 0
    template <typename T>
        std::vector<MOM<T>> getCollinearPhaseSpacePoint4(const T theta, const bool verbose = false);

    // hard coded single collinear phase-space point for 2->3
    // collinear in 3||4 (s34->0)
    template <typename T>
        std::vector<MOM<T>> getCollinearPhaseSpacePoint5(const T lambda, const bool verbose = false);

    // To check that sij scales correctly with lambda (and other sij do not)
    template <typename T>
        void testCollinearPhaseSpacePoint(const std::vector<MOM<T>>& mom, const int mul);

    // To check that collinear sij scales correctly with lambda (and other sij do not)
    template <typename T>
        void testPhaseSpacePoint5(const T lambda);

    // hard coded single collinear phase-space point for 2->4, 0||1
    template <typename T>
        std::vector<MOM<T>> getCollinear01PhaseSpacePoint6(const T lambda, const bool verbose = false, const T sqrtS = 1e3);

    // template for hard coded single collinear phase-space point for 2->4
    template <typename T>
        std::vector<MOM<T>>
        getCollinearPhaseSpacePoint6(const T alpha, const T beta, const T gamma, const bool verbose = false,
                const T sqrtS = 1);

    // hard coded single collinear phase-space point for 2->4, 2||3
    template <typename T>
        std::vector<MOM<T>> getCollinear23PhaseSpacePoint6(const T lambda, const bool verbose = false, const T sqrtS = 1);

    // hard coded single collinear phase-space point for 2->4, 3||4
    template <typename T>
        std::vector<MOM<T>> getCollinear34PhaseSpacePoint6(const T lambda, const bool verbose = false, const T sqrtS = 1);

    // To check that s01 scales correctly with lambda (and other sij do not)
    template <typename T>
        void testCollinear01PhaseSpacePoint6(const T lambda, const bool verbose = false);

    // To check that s34 scales correctly with lambda (and other sij do not)
    template <typename T>
        void testCollinear34PhaseSpacePoint6(const T lambda, const bool verbose = false);

    // To check that s23 scales correctly with lambda (and other sij do not)
    template <typename T>
        void testCollinear23PhaseSpacePoint6(const T lambda, const bool verbose = false);

    // To check that s12 scales correctly with lambda (and other sij do not)
    template <typename T>
        void testCollinearPhaseSpacePoint4(const T lambda, const bool verbose = false);

    // test that phase space point has massless momenta and that momentum conservation is obeyed
    template <typename T, int mul>
        void checkPhaseSpacePoint(const std::array<std::array<T, 4>, mul>& momenta, const bool verbose = false);

    template <typename T, int mul>
        void printPhaseSpacePoint(const std::array<std::array<T, 4>, mul>& momenta, const bool verbose = false);
} // namespace ps

#include "phaseSpace.ipp"

#endif //NJET_EXAMPLES_PHASESPACE_H
