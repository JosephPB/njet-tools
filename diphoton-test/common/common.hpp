#ifndef NJET_EXAMPLES_COMMON_H
#define NJET_EXAMPLES_COMMON_H

#include "ngluon2/Model.h"
#include "tools/PhaseSpace.h"

#include <cassert>
#include <complex>
#include <iostream>
#include <string>
#include <vector>

namespace com {

    const Flavour<double> A { StandardModel::Ax(StandardModel::IL(), StandardModel::IL().C()) };
    const Flavour<double> g { StandardModel::G() };

    // light quark charge sum
    // 2 * up-type-quark-charge + 3 * down-type-quark-charge
    // 2 * std::pow(2 / 3, 2) + 3 * std::pow(-1 / 3, 2)
    template <typename T>
        const T Nf { 11. / 9. };

    template <typename T>
        const T Nf2 { Nf<T> * Nf<T> };

    const int Nc { 3 };
    const int Nc2 { Nc * Nc };
    const int Nc3 { Nc2 * Nc };
    const int Nc4 { Nc3 * Nc };
    const int Nc6 { Nc4 * Nc2 };

    enum Basis {
        reflectedFundamental,
        fundamental
    };

    template <typename T>
        T mod_sq(std::complex<T> n);

    template <typename T>
        void print(const std::string& name, const T& datum, const bool verbose = true);

    template <typename AT, int len>
        std::string genArrStr(const AT (&array)[len]);

    template <int mul>
        std::string genHelStr(const int (&helInt)[mul]);

    template <int mul>
        void printHelicity(const std::string& name, const int (&helInt)[mul], const bool verbose = true);

    template <typename T>
        void printMomenta(const std::vector<MOM<T>> momenta, const bool verbose = false);

    // test that phase space point has massless momenta and that momentum conservation is obeyed
    template <typename T>
        void checkPhaseSpacePoint(const std::vector<MOM<T>>& momenta, const bool verbose = false);

} // namespace com

#include "common.ipp"

#endif //NJET_EXAMPLES_COMMON_H
