#include <chrono>
#include <iostream>
#include <vector>

#include "analytic/0q5g-2l-analytic.h"
#include "ngluon2/Model.h"
#include "ngluon2/Mom.h"
#include "tools/PhaseSpace.h"
#include "chsums/NJetAccuracy.h"

template <typename T>
void run()
{
    const int pspoints { 10 };
    const int legs { 5 };
    // const T scale_test_factor { 1. };
    const double sqrtS { 1. };
    const double mur { sqrtS/2. };

    std::vector<Flavour<double>> flavours(legs, StandardModel::G());

    NJetAccuracy<T>* amp { NJetAccuracy<T>::template create<Amp0q5g_a2l<T>>() };
    // Amp0q5g_a<T> amp(scale_test_factor);
    amp->setMuR2(mur * mur);

    amp->setNf(0);

    for (int p { 1 }; p < pspoints + 1; ++p) {
        std::cout << "==================== Test point " << p
                  << " ====================" << '\n';

        // rseed = p
        PhaseSpace<T> ps(legs, flavours.data(), p, sqrtS);
        const std::vector<MOM<T>> momenta { ps.getPSpoint() };
        ps.showPSpoint();

        // std::cout << '\n';
        // for (int i { 0 }; i < legs; ++i) {
        //     for (int j { i + 1 }; j < legs; ++j) {
        //         std::cout << "s" << i + 1 << j + 1 << "/s12=" <<
        //         dot(Momenta[p][i], Momenta[p][j]) / dot(Momenta[p][0],
        //         Momenta[p][1]) * 2 << '\n';
        //     }
        // }

        amp->setMomenta(momenta);

	// ensure virt_part does not conflict with setpenta on xis
        amp->born();
        amp->virt();
        amp->virtsq();

        auto t1 { std::chrono::high_resolution_clock::now() };
        amp->setpenta2l();
        auto t2 { std::chrono::high_resolution_clock::now() };
        auto d1 {
            std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count()
        };

        std::cout << "Pentagon functions evaluation time: " << d1 << "ms\n";

        auto t3 { std::chrono::high_resolution_clock::now() };
        amp->dblvirt();
        auto t4 { std::chrono::high_resolution_clock::now() };
        auto d2 {
            std::chrono::duration_cast<std::chrono::milliseconds>(t4 - t3).count()
        };

        auto t5 { std::chrono::high_resolution_clock::now() };
        amp->virtsqe2();
        auto t6 { std::chrono::high_resolution_clock::now() };
        auto d3 {
            std::chrono::duration_cast<std::chrono::milliseconds>(t6 - t5).count()
        };

        std::cout << "2L evaluation time:    " << d2 << "ms\n"
                  << "L^2 evaluation time:   " << d3 << "ms\n\n"
                  << "VV evaluation time:    " << d1 + d2 + d3 << "ms\n\n"
                  << "Born:                  " << amp->born_value() << "\n"
                  << "Loop:                  " << amp->virt_value() << "\n"
                  << "LoopSq:                " << amp->virtsq_value() << "\n"
                  << "LoopSqe2:              " << amp->virtsqe2_value() << "\n"
                  << "Absolute error:        " << amp->virtsqe2_error() << '\n';
	for (int i{4};i>=0;--i){
                  std::cout << "Fractional error[e^" << i << "]: " << amp->virtsqe2_error().get(i).real()/amp->virtsqe2_value().get(i).real() << '\n';
	}
	std::cout
                  << "LoopSqe2[/e4]/B/5^3:   " << amp->virtsqe2_value().get4().real()/amp->born_value()/225. << "\n"
                  << "DblLoop:               " << amp->dblvirt_value() << "\n"
                  << "Absolute error:        " << amp->dblvirt_error() << '\n';
	for (int i{4};i>=0;--i){
                  std::cout<< "Fractional error[e^" << i << "]: " << amp->dblvirt_error().get(i).real()/amp->dblvirt_value().get(i).real() << '\n';
	}
	std::cout
                  << "DblLoop[/e4]/B/Nc/5^2: " << amp->dblvirt_value().get4().real()/amp->born_value()/75. << "\n";
    }
}

int main()
{
    std::cout.setf(std::ios_base::scientific, std::ios_base::floatfield);
    std::cout.precision(16);

    std::cout << '\n'
              << "  NJet: simple example of PentagonFunctions-cpp based 2-loop "
                 "evaluation"
              << '\n'
              << '\n'
              << "  Notation: DblLoop  = 2*Re(A2.cA0)" << '\n'
              << "            LoopSqe2 = A1.cA1" << '\n'
              << '\n';

    run<qd_real>();
}
