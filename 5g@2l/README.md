# 5g@2l

* 5 gluon 2 loops

## Installing GCC on IPPP system

```shell
cd $SCRATCH
git clone git://gcc.gnu.org/git/gcc.git gcc-src  # slow
# git checkout releases/gcc-10  # this failed, so I used the `master` branch instead (experimental)
mkdir gcc-bld
mkdir gcc-lcl
cd gcc-src
./contrib/download_prerequisites
cd ../gcc-bld
$SCRATCH/gcc-src/configure --prefix=$SCRATCH/gcc-lcl --disable-multilib
make -j
make install
```

Use with `CXX='${SCRATCH}/gcc-lcl/bin/g++'`

## Compiling NJet with new analytic library setup
* Laptop, SD, 1L
    ```shell
    CPPFLAGS=`pkg-config PentagonFunctions --cflags` LDFLAGS=`pkg-config PentagonFunctions --libs` CXXFLAGS='-std=c++17 -Wall -Wextra -O0' ~/git/njet-develop/configure --prefix=$NJET_LOCAL --enable-oneloop1 --with-qd=no --enable-an0q5g
    ```
* IPPP, scratch, SD, 2L
    ```shell
    CPPFLAGS=`pkg-config PentagonFunctions --cflags` LDFLAGS=`pkg-config PentagonFunctions --libs` CXXFLAGS='-std=c++17 -Wall -Wextra -O0' $SCRATCH/njet-develop/configure --prefix=$NJET_LOCAL --enable-oneloop1 --with-qd=no --enable-an0q5g2l
    ```
* IPPP, scratch, DD, 2L
    ```shell
    CPPFLAGS=`pkg-config PentagonFunctions --cflags` LDFLAGS=`pkg-config PentagonFunctions --libs` CXXFLAGS='-std=c++17 -Wall -Wextra -O0' $SCRATCH/njet-develop/configure --prefix=$NJET_LOCAL --enable-oneloop1 --enable-an0q5g2l
    ```
* IPPP, home, SD, 2L
    ```shell
    CPPFLAGS=`pkg-config PentagonFunctions --cflags` LDFLAGS=`pkg-config PentagonFunctions --libs` CXXFLAGS='-std=c++17 -Wall -Wextra -O0' $GIT/njet-develop/configure --prefix=$NJET_LOCAL --enable-oneloop1 --with-qd=no --enable-an0q5g2l
    ```
* IPPP, home, DD, 2L
    ```shell
    CPPFLAGS=`pkg-config PentagonFunctions --cflags` LDFLAGS=`pkg-config PentagonFunctions --libs` CXXFLAGS='-std=c++17 -Wall -Wextra -O0' $GIT/njet-develop/configure --prefix=$NJET_LOCAL --enable-oneloop1 --enable-an0q5g2l
    ```
