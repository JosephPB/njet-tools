#include <array>
#include <cmath>
#include <iostream>
#include <numeric>

template <int mul>
struct Helicity : std::array<int, mul> {
    int order() const
    {
        return std::abs(std::accumulate(this->cbegin(), this->cend(), 0));
    }
};

template <int mul>
std::ostream& operator<<(std::ostream& out, const Helicity<mul>& hel)
{
    for (int h : hel) {
        out << (h == 1 ? '+' : '-');
    }
    return out;
}
