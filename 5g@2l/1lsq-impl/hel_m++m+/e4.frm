#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci43;
S tci42;
S tci41;
S tcr41;
S tcr42;
S tcr43;
S tci21;
S tcr44;
S tcr31;
S tcr45;
S tcr33;
S tcr32;
S f64;
S f187;
S f61;
S f125;
S f77;
S f120;
S f70;
S f122;
S f218;
S f73;
S f280;
S f128;
S f284;
S f229;
S f232;
S f235;
S f242;
S f3;
S f80;
S f2;
S f144;
S f84;
S f87;
S f5;
S f86;
S f265;
S f94;
S f264;
S f91;
S f260;
S f22;
S f21;
S f98;
S f275;
S f277;
S f270;
S f118;
S f271;
S f44;
S f116;
S f111;
S f113;
S f109;
S f103;
S f208;
S f101;
S f107;
S f105;
S fvu1u2u8;
S fvu3u78;
S fvu4u25;
S fvu4u221;
S fvu4u28;
S fvu3u71;
S fvu1u2u3;
S fvu3u70;
S fvu1u2u7;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u237;
S fvu4u289;
S fvu1u1u1;
S fvu1u1u6;
S fvu3u19;
S fvu1u1u7;
S fvu1u1u4;
S fvu1u1u5;
S fvu4u282;
S fvu3u14;
S fvu1u1u8;
S fvu3u63;
S fvu1u1u9;
S fvu4u148;
S fvu3u12;
S fvu2u1u10;
S fvu4u202;
S fvu4u201;
S fvu4u39;
S fvu4u290;
S fvu4u139;
S fvu4u291;
S fvu4u293;
S fvu4u295;
S fvu3u82;
S fvu2u1u1;
S fvu3u37;
S fvu3u36;
S fvu3u80;
S fvu3u35;
S fvu3u81;
S fvu4u213;
S fvu2u1u2;
S fvu2u1u5;
S fvu3u33;
S fvu4u215;
S fvu3u32;
S fvu2u1u7;
S fvu3u31;
S fvu2u1u6;
S fvu2u1u9;
S fvu3u6;
S fvu2u1u8;
S fvu3u4;
S fvu3u2;
S fvu3u38;
S fvu3u1;
S fvu4u83;
S fvu3u25;
S fvu4u273;
S fvu4u271;
S fvu4u80;
S fvu4u69;
S fvu4u277;
S fvu3u23;
S fvu4u67;
S fvu4u12;
S fvu4u279;
S fvu4u66;
S fvu4u313;
S fvu4u91;
S fvu4u309;
S fvu4u93;
S fvu1u1u10;
S fvu4u192;
S fvu4u190;
S fvu4u41;
S fvu4u100;
S fvu4u246;
S fvu4u174;
S fvu4u199;
S fvu4u102;
S fvu4u244;
S fvu4u171;
S fvu4u49;
S fvu4u182;
S fvu4u184;
S fvu4u51;
S fvu4u255;
S fvu3u45;
S fvu4u250;
S fvu3u42;
S fvu3u43;
S fvu4u252;
S fvu3u40;
ExtraSymbols,array,Z;

L ieu2ueu4 = (-2556*f2 + 2556*f3 - 2556*f5 - 36*f70 + 
      11*f73)/81 + ((-f125 + 12*f208)*fvu3u1)/3 + 
    ((f125 - 12*f208)*fvu3u2)/6 + ((-f125 + 12*f208)*fvu3u4)/18 + 
    ((-f125 + 12*f208)*fvu3u6)/9 + 
    ((3*f113 + f125 - 12*f208 - 36*f271 + 36*f277)*fvu3u12)/18 + 
    ((f125 - 12*f208)*fvu3u14)/9 + ((-f125 + 12*f208)*fvu3u19)/9 + 
    ((f125 - 12*f208)*fvu3u31)/9 + 
    ((3*f109 + f125 - 12*f208 + 36*f235)*fvu3u32)/18 + 
    ((f116 + 12*f264 + 8*f271 - 4*f275 - 12*f280 + 12*f284)*
      fvu3u33)/6 + ((-f120 - 12*f242)*fvu3u35)/6 - 
    (2*(f125 - 12*f208)*fvu3u36)/9 + ((-f125 + 12*f208)*fvu3u37)/
     9 + ((-f125 + 12*f208)*fvu3u38)/6 + ((f125 - 12*f208)*fvu3u40)/
     6 + ((f125 - 12*f208)*fvu3u42)/3 + ((-f2 + f3 - f5)*fvu4u12)/
     4 + ((-f2 + f3 - f5)*fvu4u66)/4 + 
    ((f2 - f3 + f5 - 2*f44 - 2*f86 + 2*f144 - 4*f187 + 
       4*f260 + 4*f264)*fvu4u67)/4 + 
    ((-f2 + f3 - f5 - 2*f77 - 4*f218)*fvu4u69)/4 + 
    ((-f2 + f3 - f5)*fvu4u237)/4 + 
    ((-f2 + f3 - f5)*fvu1u1u1^4)/24 + 
    ((f125 - 12*f208)*fvu1u1u3^3)/18 + 
    ((-f2 + f3 - f5)*fvu1u1u4^4)/24 + 
    ((-f125 + 12*f208)*fvu1u1u5^3)/3 + 
    ((-f2 - 11*f3 + 11*f5 + f91 - f94 - 2*f125 + 24*f208 - 
       12*f229 + 12*f265)*fvu1u1u6^3)/18 + 
    ((-f2 + f3 - f5)*fvu1u1u6^4)/24 + 
    ((f94 + 12*f229)*fvu1u1u7^3)/18 + 
    ((-f2 + f3 - f5)*fvu1u1u7^4)/24 + 
    ((-f2 + f3 - f5)*fvu1u1u8^4)/24 + 
    ((f111 + f128 - 72*f208 + 72*f235)*fvu2u1u10)/9 + 
    ((-f125 + 12*f208)*fvu1u2u7*tci11^2)/3 + 
    ((521*f2 - 521*f3 + 521*f5 - 366*f44 + 874*f77 - 366*f86 + 
       366*f144 - 732*f187 + 1748*f218 + 732*f260 + 732*f264)*
      tci11^4)/4320 + fvu1u1u8^2*(-f103/6 - 
      (7*(f2 - f3 + f5)*tci11^2)/24) + 
    fvu2u1u2*((-6*f21 + f22 + 6*f113 + f122 + 72*f242 - 
        72*f271 + 72*f277)/9 + ((f77 + 2*f218)*tci11^2)/12) + 
    fvu2u1u8*((-f111 - f118 - 72*f235 - 72*f264 - 48*f271 + 
        24*f275 + 72*f280 - 72*f284)/9 + 
      ((f44 + f86 - f144 + 2*f187 - 2*f260 - 2*f264)*tci11^2)/
       12) + fvu2u1u6*((f118 - f122 - 72*f242 + 72*f264 + 
        48*f271 - 24*f275 - 72*f280 + 72*f284)/9 + 
      ((-f44 - f77 - f86 + f144 - 2*f187 - 2*f218 + 2*f260 + 
         2*f264)*tci11^2)/12) + 
    ((-432*f2 - f64 + f73 + f101 + 432*f265)*tci12)/27 + 
    ((f2 - f3 + f5)*fvu1u1u8^3*tci12)/6 + 
    ((f111 + f128 - 72*f208 + 72*f235)*fvu1u2u8*tci12)/9 + 
    (4*(f125 - 12*f208)*fvu2u1u1*tci12)/9 + 
    ((f125 - 12*f208)*fvu2u1u5*tci12)/3 + 
    fvu1u1u1^3*((-f80 - f125 + 12*f208 - 12*f270)/18 + 
      ((f2 - f3 + f5)*tci12)/6) + 
    fvu1u1u4^3*((12*f2 + f80 - f91 - 12*f265 + 12*f270)/18 + 
      ((f2 - f3 + f5)*tci12)/6) + 
    fvu2u1u9*((-6*f21 + f22 + 6*f113 + f128 - 72*f208 - 
        72*f271 + 72*f277)/9 - (4*(f125 - 12*f208)*tci12)/9) + 
    fvu1u1u7^2*((-f98 + f111 + f128 - 72*f208 - 72*f229 + 
        72*f235)/18 + ((-f125 + 12*f208)*fvu1u1u8)/6 + 
      ((-f2 + f3 - f5)*tci11^2)/24 + 
      ((f125 - 12*f208)*tci12)/6) + 
    fvu1u1u6^2*((72*f3 - 72*f5 + f61 - f111 - f118 + 72*f229 - 
        72*f235 - 72*f264 - 72*f265 - 48*f271 + 24*f275 + 
        72*f280 - 72*f284)/18 + ((-f125 + 12*f208)*fvu1u1u7)/9 + 
      ((f125 - 12*f208)*fvu1u1u8)/18 + 
      ((-f2 + f3 - f5 + f44 + f86 - f144 + 2*f187 - 2*f260 - 
         2*f264)*tci11^2)/24 + ((-f125 + 12*f208)*tci12)/18) + 
    fvu1u1u5^2*(((f125 - 12*f208)*fvu1u1u6)/6 + 
      ((-f125 + 12*f208)*fvu1u1u7)/18 + 
      ((f125 - 12*f208)*fvu1u1u8)/9 + ((-f125 + 12*f208)*tci12)/
       9) + fvu1u1u3^2*(((-f125 + 12*f208)*fvu1u1u4)/18 + 
      ((f125 - 12*f208)*fvu1u1u7)/6 + ((-f125 + 12*f208)*tci12)/
       2) + fvu1u1u4^2*((-72*f2 - f61 + f70 + f84 + f98 + 
        3*f103 + f118 - f122 - 72*f242 + 72*f264 + 72*f265 - 
        72*f270 + 48*f271 - 24*f275 - 72*f280 + 72*f284)/18 + 
      ((f125 - 12*f208)*fvu1u1u8)/18 + 
      ((-7*f2 + 7*f3 - 7*f5 - f44 - f77 - f86 + f144 - 
         2*f187 - 2*f218 + 2*f260 + 2*f264)*tci11^2)/24 + 
      ((-12*f2 - f80 + f91 + 12*f265 - 12*f270)*tci12)/6) + 
    fvu1u1u1^2*((-12*f21 + 2*f22 - f84 + 12*f113 + f122 + 
        f128 - 72*f208 + 72*f242 + 72*f270 - 144*f271 + 144*f277)/
       18 + ((-f125 + 12*f208)*fvu1u1u3)/18 + 
      ((-f125 + 12*f208)*fvu1u1u7)/6 + 
      ((f125 - 12*f208)*fvu1u1u8)/18 + 
      ((-7*f2 + 7*f3 - 7*f5 + f77 + 2*f218)*tci11^2)/24 + 
      ((f80 + f125 - 12*f208 + 12*f270)*tci12)/6) + 
    tci11^2*((-504*f2 + 72*f3 - 72*f5 - 12*f21 + 2*f22 - 
        6*f61 + 7*f70 + 6*f98 + 12*f113 + 2*f118 + 2*f128 - 
        144*f208 + 144*f264 + 432*f265 - 48*f271 - 48*f275 + 
        144*f277 - 144*f280 + 144*f284)/108 + 
      ((-36*f2 + 3*f91 - f109 + 2*f113 + f116 + 3*f120 + 
         4*f125 - 48*f208 - 12*f235 + 36*f242 + 12*f264 + 
         36*f265 - 16*f271 - 4*f275 + 24*f277 - 12*f280 + 
         12*f284)*tci12)/36) + fvu1u1u3*
     (((-f125 + 12*f208)*fvu1u1u4^2)/18 + 
      ((f125 - 12*f208)*fvu1u1u5^2)/6 - 
      (2*(f125 - 12*f208)*fvu2u1u1)/9 + 
      ((-f125 + 12*f208)*fvu2u1u5)/3 + 
      (35*(f125 - 12*f208)*tci11^2)/54 + 
      ((f125 - 12*f208)*fvu1u1u4*tci12)/9 + 
      ((-f125 + 12*f208)*fvu1u1u5*tci12)/3 + 
      ((-f125 + 12*f208)*fvu1u1u7*tci12)/3 + 
      ((f125 - 12*f208)*fvu1u2u7*tci12)/3) + 
    fvu1u2u3*(((-f118 + f122 + 72*f242 - 72*f264 - 48*f271 + 
         24*f275 + 72*f280 - 72*f284)*tci12)/9 + 
      ((f44 + f77 + f86 - f144 + 2*f187 + 2*f218 - 2*f260 - 
         2*f264)*tci11^2*tci12)/12) + 
    fvu1u1u5*(((f125 - 12*f208)*fvu1u1u6^2)/6 + 
      ((f125 - 12*f208)*fvu1u1u7^2)/6 + 
      ((f125 - 12*f208)*fvu1u1u8^2)/9 + 
      ((-f125 + 12*f208)*fvu2u1u5)/3 - 
      (4*(f125 - 12*f208)*fvu2u1u7)/9 + 
      (13*(f125 - 12*f208)*tci11^2)/108 - 
      (2*(f125 - 12*f208)*fvu1u1u8*tci12)/9 + 
      ((f125 - 12*f208)*fvu1u2u7*tci12)/3 + 
      fvu1u1u6*(((-f125 + 12*f208)*fvu1u1u8)/3 + 
        ((f125 - 12*f208)*tci12)/3) + 
      fvu1u1u7*(((f125 - 12*f208)*fvu1u1u8)/9 + 
        ((-f125 + 12*f208)*tci12)/9)) + 
    ((-f2 + f3 - f5 + 4*f77 + 8*f218)*tcr11^4)/24 + 
    tci11^2*((6*f109 - 2*f113 - 6*f116 + 7*f120 + 19*f125 - 
        228*f208 + 72*f235 + 84*f242 - 72*f264 - 24*f271 + 
        24*f275 - 24*f277 + 72*f280 - 72*f284)/36 + 
      ((6*f2 - 6*f3 + 6*f5 + 5*f44 + 9*f77 + 5*f86 - 5*f144 + 
         10*f187 + 18*f218 - 10*f260 - 10*f264)*tci12)/12)*
     tcr12 + (5*(f2 - f3 + f5 + 2*f77 + 4*f218)*tci11^2*
      tcr12^2)/18 + ((2*f113 - f120 - f125 + 12*f208 - 
        12*f242 - 24*f271 + 24*f277)/9 - 
      (2*(3*f2 - 3*f3 + 3*f5 + 2*f77 + 4*f218)*tci12)/3)*
     tcr12^3 + (19*(f2 - f3 + f5 + 2*f77 + 4*f218)*tcr12^4)/
     36 + tcr11^3*((-4*f113 - f120 - 12*f242 + 48*f271 - 
        48*f277)/18 + ((-f2 + f3 - f5 - 3*f77 - 6*f218)*
        tci12)/3 + ((-f2 + f3 - f5 - f77 - 2*f218)*tcr12)/
       3) + (((12*f2 - 12*f3 + 12*f5 + 25*f77 + 50*f218)*
        tci11^2)/12 + (2*(f113 + f120 + f125 - 12*f208 + 
         12*f242 - 12*f271 + 12*f277)*tci12)/3)*tcr21 + 
    tcr11^2*(((30*f2 - 30*f3 + 30*f5 + 43*f77 + 86*f218)*
        tci11^2)/24 + ((2*f113 + 3*f120 + 2*f125 - 24*f208 + 
         36*f242 - 24*f271 + 24*f277)*tci12)/6 + 
      (f2 - f3 + f5 + f77 + 2*f218)*tci12*tcr12 + 
      ((f2 - f3 + f5 + 2*f77 + 4*f218)*tcr12^2)/2 + 
      (-f2 + f3 - f5)*tcr21) + 
    ((-2*f113 + f120 + f125 - 12*f208 + 12*f242 + 24*f271 - 
        24*f277)/3 + 2*(3*f2 - 3*f3 + 3*f5 + 2*f77 + 4*f218)*
       tci12)*tcr31 + 
    ((-2*f113 + f120 + f125 - 12*f208 + 12*f242 + 24*f271 - 
        24*f277)/12 + ((3*f2 - 3*f3 + 3*f5 + 2*f77 + 4*f218)*
        tci12)/2)*tcr32 + 
    ((616*f2 - 616*f3 + 616*f5 + 18*f109 + 74*f113 - 18*f116 - 
        11*f120 - 11*f125 + 132*f208 + 216*f235 - 132*f242 - 
        216*f264 - 1032*f271 + 72*f275 + 888*f277 + 216*f280 - 
        216*f284)/72 + ((-27*f2 + 27*f3 - 27*f5 + 18*f44 - 
         56*f77 + 18*f86 - 18*f144 + 36*f187 - 112*f218 - 
         36*f260 - 36*f264)*tci12)/12)*tcr33 + 
    fvu1u1u8*(f105/9 + (2*(f125 - 12*f208)*fvu2u1u9)/9 + 
      (f103*tci12)/3 + tci11^2*((f125 - 12*f208)/54 + 
        ((f2 - f3 + f5)*tci12)/4) - 
      (7*(f2 - f3 + f5)*tcr33)/3) + 
    fvu1u1u6*((-432*f3 + 432*f5 - f64 - 432*f229 + 432*f265)/27 + 
      ((f125 - 12*f208)*fvu1u1u8^2)/18 - 
      (2*(f125 - 12*f208)*fvu2u1u8)/9 + 
      ((-f111 - 72*f235)*tci12)/9 + tci11^2*
       ((-3*f2 - 33*f3 + 33*f5 + 3*f91 - 3*f94 + 8*f125 - 
          96*f208 - 36*f229 + 36*f265)/108 + 
        ((f2 - f3 + f5)*tci12)/24) + 
      fvu1u1u7*((f118 + 72*f264 + 48*f271 - 24*f275 - 72*f280 + 
          72*f284)/9 + (2*(f125 - 12*f208)*fvu1u1u8)/9 + 
        ((f2 - f3 + f5 - 2*f44 - 2*f86 + 2*f144 - 4*f187 + 
           4*f260 + 4*f264)*tci11^2)/24 - 
        (2*(f125 - 12*f208)*tci12)/9) + 
      fvu1u1u8*((f111 + 72*f235)/9 + ((-f2 + f3 - f5)*
          tci11^2)/24 + ((-f125 + 12*f208)*tci12)/9) - 
      (7*(f2 - f3 + f5)*tcr33)/3) + 
    fvu1u1u4*((432*f2 + f64 - f73 - f87 - f101 - 3*f105 - 
        432*f265 + 432*f270)/27 + ((f125 - 12*f208)*fvu1u1u8^2)/
       18 + fvu1u1u6*((f122 + 72*f242)/9 + 
        ((f2 - f3 + f5 + 2*f77 + 4*f218)*tci11^2)/24) + 
      fvu1u1u7*((-f118 - 72*f264 - 48*f271 + 24*f275 + 72*f280 - 
          72*f284)/9 + ((-f2 + f3 - f5 + 2*f44 + 2*f86 - 
           2*f144 + 4*f187 - 4*f260 - 4*f264)*tci11^2)/24) + 
      ((72*f2 + f61 - f70 - f84 - f98 - 3*f103 - 72*f265 + 
         72*f270)*tci12)/9 + tci11^2*
       ((7*(12*f2 + f80 - f91 - 12*f265 + 12*f270))/36 + 
        ((f2 - f3 + f5)*tci12)/4) + 
      fvu1u1u8*((-6*f21 + f22 + 6*f113 - 72*f271 + 72*f277)/9 + 
        ((-f2 + f3 - f5)*tci11^2)/24 + 
        ((-f125 + 12*f208)*tci12)/9) - 
      (7*(f2 - f3 + f5)*tcr33)/3) + 
    fvu1u1u1*((f87 - 432*f270)/27 + ((f125 - 12*f208)*fvu1u1u3^2)/
       18 + ((-f125 + 12*f208)*fvu1u1u5^2)/6 + 
      ((-f125 + 12*f208)*fvu1u1u8^2)/18 - 
      (2*(f125 - 12*f208)*fvu2u1u1)/9 + 
      (2*(f125 - 12*f208)*fvu2u1u9)/9 + 
      fvu1u1u4*((6*f21 - f22 - 6*f113 + 72*f271 - 72*f277)/9 + 
        ((-f125 + 12*f208)*fvu1u1u8)/9 + 
        ((f2 - f3 + f5)*tci11^2)/24) + 
      fvu1u1u6*((-f122 - 72*f242)/9 + 
        ((-f2 + f3 - f5 - 2*f77 - 4*f218)*tci11^2)/24) + 
      ((f84 - f122 - f128 + 72*f208 - 72*f242 - 72*f270)*
        tci12)/9 + fvu1u1u8*((6*f21 - f22 - 6*f113 + 72*f271 - 
          72*f277)/9 + ((f2 - f3 + f5)*tci11^2)/24 + 
        ((f125 - 12*f208)*tci12)/9) + 
      fvu1u1u7*((-f128 + 72*f208)/9 + ((-f2 + f3 - f5)*
          tci11^2)/24 + ((f125 - 12*f208)*tci12)/3) + 
      fvu1u1u3*(((f125 - 12*f208)*fvu1u1u4)/9 + 
        ((-f125 + 12*f208)*tci12)/9) + 
      tci11^2*((-7*f80 - 10*f125 + 120*f208 - 84*f270)/36 + 
        ((2*f2 - 2*f3 + 2*f5 - f77 - 2*f218)*tci12)/12) - 
      (7*(f2 - f3 + f5)*tcr33)/3) + 
    fvu1u1u7*((f101 + 432*f229)/27 + 
      ((-f125 + 12*f208)*fvu1u1u8^2)/6 - 
      (4*(f125 - 12*f208)*fvu2u1u7)/9 - 
      (2*(f125 - 12*f208)*fvu2u1u8)/9 + 
      ((f118 + 72*f264 + 48*f271 - 24*f275 - 72*f280 + 72*f284)*
        tci12)/9 + fvu1u1u8*((-f111 - 72*f235)/9 + 
        ((f2 - f3 + f5)*tci11^2)/24 + 
        ((f125 - 12*f208)*tci12)/3) + 
      tci11^2*((f94 - 4*f125 + 48*f208 + 12*f229)/36 + 
        ((f2 - f3 + f5 - 2*f44 - 2*f86 + 2*f144 - 4*f187 + 
           4*f260 + 4*f264)*tci12)/24) - 
      (7*(f2 - f3 + f5)*tcr33)/3) + 
    tcr11*(tci11^2*((-7*f120 + 3*f125 - 36*f208 - 84*f242)/
         36 - (11*(f2 - f3 + f5 + f77 + 2*f218)*tci12)/12) + 
      (((-9*f2 + 9*f3 - 9*f5 - 10*f77 - 20*f218)*tci11^2)/6 + 
        ((-f120 - f125 + 12*f208 - 12*f242)*tci12)/3)*tcr12 - 
      (4*(f77 + 2*f218)*tcr12^3)/3 + 
      ((-2*(f113 - 12*f271 + 12*f277))/3 + 2*(f2 - f3 + f5)*
         tci12)*tcr21 - 2*(f2 - f3 + f5)*tcr31 + 
      ((-f2 + f3 - f5)*tcr32)/2 + 
      ((37*f2 - 37*f3 + 37*f5 + 18*f77 + 36*f218)*tcr33)/12) + 
    (f2 - f3 + f5)*tcr41 + 
    (2*(f2 - f3 + f5 + 2*f77 + 4*f218)*tcr42)/3 + 
    ((f2 - f3 + f5 + 2*f77 + 4*f218)*tcr43)/4 + 
    2*(f2 - f3 + f5 + 2*f77 + 4*f218)*tcr44 + 
    ((f2 - f3 + f5 + 2*f77 + 4*f218)*tcr45)/2;
L ieu1ueu4 = (-432*f2 + 432*f3 - 432*f5 + f73)/27 + 
    (-f2 + f3 - f5)*fvu3u1 + ((f2 - f3 + f5)*fvu3u2)/2 + 
    ((-f2 + f3 - f5)*fvu3u4)/6 + ((-f2 + f3 - f5)*fvu3u6)/3 + 
    ((-f2 + f3 - f5)*fvu3u12)/3 + ((f2 - f3 + f5)*fvu3u14)/3 + 
    ((-f2 + f3 - f5)*fvu3u19)/3 + ((f2 - f3 + f5)*fvu3u31)/3 + 
    ((-f2 + f3 - f5)*fvu3u32)/3 + 
    ((f2 - f3 + f5 - 2*f44 - 2*f86 + 2*f144 - 4*f187 + 
       4*f260 + 4*f264)*fvu3u33)/2 + 
    ((-f2 + f3 - f5 - 2*f77 - 4*f218)*fvu3u35)/2 - 
    (2*(f2 - f3 + f5)*fvu3u36)/3 + ((-f2 + f3 - f5)*fvu3u37)/
     3 + ((-f2 + f3 - f5)*fvu3u38)/2 + ((f2 - f3 + f5)*fvu3u40)/
     2 + (f2 - f3 + f5)*fvu3u42 + ((f2 - f3 + f5)*fvu1u1u3^3)/
     6 + ((f2 - f3 + f5)*fvu1u1u4^3)/6 + (-f2 + f3 - f5)*
     fvu1u1u5^3 + ((-f2 + f3 - f5)*fvu1u1u6^3)/6 + 
    ((f2 - f3 + f5)*fvu1u1u7^3)/6 + 
    ((f2 - f3 + f5)*fvu1u1u8^3)/6 + 
    fvu1u1u1^2*((f80 + 2*f113 + f120 + f125 - 12*f208 + 
        12*f242 + 12*f270 - 24*f271 + 24*f277)/6 + 
      ((-f2 + f3 - f5)*fvu1u1u3)/6 + 
      ((-f2 + f3 - f5)*fvu1u1u7)/2 + 
      ((f2 - f3 + f5)*fvu1u1u8)/6) + 
    ((f113 + f120 + 12*f242 - 12*f271 + 12*f277)*fvu2u1u2)/3 + 
    ((f116 - f120 - 12*f242 + 12*f264 + 8*f271 - 4*f275 - 
       12*f280 + 12*f284)*fvu2u1u6)/3 + 
    ((-f109 - f116 - 12*f235 - 12*f264 - 8*f271 + 4*f275 + 
       12*f280 - 12*f284)*fvu2u1u8)/3 + 
    ((f109 + f125 - 12*f208 + 12*f235)*fvu2u1u10)/3 + 
    (-f2 + f3 - f5)*fvu1u2u7*tci11^2 + 
    fvu1u1u8*(f103/3 + (2*(f2 - f3 + f5)*fvu2u1u9)/3 + 
      (23*(f2 - f3 + f5)*tci11^2)/36) + 
    ((-72*f2 - f61 + f70 + f98 + 72*f265)*tci12)/9 + 
    ((-f2 + f3 - f5)*fvu1u1u8^2*tci12)/2 + 
    ((-f116 + f120 + 12*f242 - 12*f264 - 8*f271 + 4*f275 + 
       12*f280 - 12*f284)*fvu1u2u3*tci12)/3 + 
    ((f109 + f125 - 12*f208 + 12*f235)*fvu1u2u8*tci12)/3 + 
    (4*(f2 - f3 + f5)*fvu2u1u1*tci12)/3 + 
    (f2 - f3 + f5)*fvu2u1u5*tci12 + 
    fvu1u1u6^2*((f2 + 11*f3 - 11*f5 - f91 + f94 - f109 - 
        f116 + 12*f229 - 12*f235 - 12*f264 - 12*f265 - 8*f271 + 
        4*f275 + 12*f280 - 12*f284)/6 + 
      ((-f2 + f3 - f5)*fvu1u1u7)/3 + 
      ((f2 - f3 + f5)*fvu1u1u8)/6 + ((-f2 + f3 - f5)*tci12)/
       6) + fvu1u1u5^2*(((f2 - f3 + f5)*fvu1u1u6)/2 + 
      ((-f2 + f3 - f5)*fvu1u1u7)/6 + 
      ((f2 - f3 + f5)*fvu1u1u8)/3 + ((-f2 + f3 - f5)*tci12)/
       3) + fvu1u1u4^2*((-12*f2 - f80 + f91 + f116 - f120 - 
        12*f242 + 12*f264 + 12*f265 - 12*f270 + 8*f271 - 4*f275 - 
        12*f280 + 12*f284)/6 + ((f2 - f3 + f5)*fvu1u1u8)/6 + 
      ((-f2 + f3 - f5)*tci12)/2) + 
    fvu1u1u3^2*(((-f2 + f3 - f5)*fvu1u1u4)/6 + 
      ((f2 - f3 + f5)*fvu1u1u7)/2 - 
      (3*(f2 - f3 + f5)*tci12)/2) + 
    fvu2u1u9*((f113 + f125 - 12*f208 - 12*f271 + 12*f277)/3 - 
      (4*(f2 - f3 + f5)*tci12)/3) + 
    fvu1u1u7^2*((-f94 + f109 + f125 - 12*f208 - 12*f229 + 
        12*f235)/6 + ((-f2 + f3 - f5)*fvu1u1u8)/2 + 
      ((f2 - f3 + f5)*tci12)/2) + 
    tci11^2*((-83*f2 + 11*f3 - 11*f5 + 6*f91 + 2*f113 + 
        2*f116 + 2*f125 - 24*f208 + 24*f264 + 72*f265 - 8*f271 - 
        8*f275 + 24*f277 - 24*f280 + 24*f284)/36 + 
      ((-f2 + f3 - f5 - f44 + 3*f77 - f86 + f144 - 2*f187 + 
         6*f218 + 2*f260 + 2*f264)*tci12)/6) + 
    fvu1u1u3*(((-f2 + f3 - f5)*fvu1u1u4^2)/6 + 
      ((f2 - f3 + f5)*fvu1u1u5^2)/2 - 
      (2*(f2 - f3 + f5)*fvu2u1u1)/3 + (-f2 + f3 - f5)*
       fvu2u1u5 + (35*(f2 - f3 + f5)*tci11^2)/18 + 
      ((f2 - f3 + f5)*fvu1u1u4*tci12)/3 + 
      (-f2 + f3 - f5)*fvu1u1u5*tci12 + (-f2 + f3 - f5)*
       fvu1u1u7*tci12 + (f2 - f3 + f5)*fvu1u2u7*tci12) + 
    fvu1u1u4*((72*f2 + f61 - f70 - f84 - f98 - 3*f103 - 
        72*f265 + 72*f270)/9 + ((f120 + 12*f242)*fvu1u1u6)/3 + 
      ((-f116 - 12*f264 - 8*f271 + 4*f275 + 12*f280 - 12*f284)*
        fvu1u1u7)/3 + ((f2 - f3 + f5)*fvu1u1u8^2)/6 + 
      (7*(f2 - f3 + f5)*tci11^2)/12 + 
      ((12*f2 + f80 - f91 - 12*f265 + 12*f270)*tci12)/3 + 
      fvu1u1u8*((f113 - 12*f271 + 12*f277)/3 + 
        ((-f2 + f3 - f5)*tci12)/3)) + 
    fvu1u1u6*((-72*f3 + 72*f5 - f61 - 72*f229 + 72*f265)/9 + 
      ((f2 - f3 + f5)*fvu1u1u8^2)/6 - 
      (2*(f2 - f3 + f5)*fvu2u1u8)/3 + 
      (11*(f2 - f3 + f5)*tci11^2)/36 + 
      ((-f109 - 12*f235)*tci12)/3 + fvu1u1u8*
       ((f109 + 12*f235)/3 + ((-f2 + f3 - f5)*tci12)/3) + 
      fvu1u1u7*((f116 + 12*f264 + 8*f271 - 4*f275 - 12*f280 + 
          12*f284)/3 + (2*(f2 - f3 + f5)*fvu1u1u8)/3 - 
        (2*(f2 - f3 + f5)*tci12)/3)) + 
    fvu1u1u1*((f84 - 72*f270)/9 + ((f2 - f3 + f5)*fvu1u1u3^2)/
       6 + ((-f2 + f3 - f5)*fvu1u1u5^2)/2 + 
      ((-f120 - 12*f242)*fvu1u1u6)/3 + 
      ((-f2 + f3 - f5)*fvu1u1u8^2)/6 + 
      fvu1u1u4*((-f113 + 12*f271 - 12*f277)/3 + 
        ((-f2 + f3 - f5)*fvu1u1u8)/3) - 
      (2*(f2 - f3 + f5)*fvu2u1u1)/3 + 
      (2*(f2 - f3 + f5)*fvu2u1u9)/3 + 
      ((-f2 + f3 - f5)*tci11^2)/4 + 
      ((-f80 - f120 - f125 + 12*f208 - 12*f242 - 12*f270)*
        tci12)/3 + fvu1u1u3*(((f2 - f3 + f5)*fvu1u1u4)/3 + 
        ((-f2 + f3 - f5)*tci12)/3) + 
      fvu1u1u8*((-f113 + 12*f271 - 12*f277)/3 + 
        ((f2 - f3 + f5)*tci12)/3) + 
      fvu1u1u7*((-f125 + 12*f208)/3 + (f2 - f3 + f5)*
         tci12)) + fvu1u1u7*((f98 + 72*f229)/9 + 
      ((-f2 + f3 - f5)*fvu1u1u8^2)/2 - 
      (4*(f2 - f3 + f5)*fvu2u1u7)/3 - 
      (2*(f2 - f3 + f5)*fvu2u1u8)/3 + 
      ((-f2 + f3 - f5)*tci11^2)/4 + 
      ((f116 + 12*f264 + 8*f271 - 4*f275 - 12*f280 + 12*f284)*
        tci12)/3 + fvu1u1u8*((-f109 - 12*f235)/3 + 
        (f2 - f3 + f5)*tci12)) + 
    fvu1u1u5*(((f2 - f3 + f5)*fvu1u1u6^2)/2 + 
      ((f2 - f3 + f5)*fvu1u1u7^2)/2 + 
      ((f2 - f3 + f5)*fvu1u1u8^2)/3 + (-f2 + f3 - f5)*
       fvu2u1u5 - (4*(f2 - f3 + f5)*fvu2u1u7)/3 + 
      (13*(f2 - f3 + f5)*tci11^2)/36 - 
      (2*(f2 - f3 + f5)*fvu1u1u8*tci12)/3 + 
      (f2 - f3 + f5)*fvu1u2u7*tci12 + 
      fvu1u1u7*(((f2 - f3 + f5)*fvu1u1u8)/3 + 
        ((-f2 + f3 - f5)*tci12)/3) + 
      fvu1u1u6*((-f2 + f3 - f5)*fvu1u1u8 + (f2 - f3 + f5)*
         tci12)) + (3*(f2 - f3 + f5 + 2*f77 + 4*f218)*tci12*
      tcr11^2)/2 + ((3*f2 - 3*f3 + 3*f5 - 2*f77 - 4*f218)*
      tcr11^3)/6 + ((8*f2 - 8*f3 + 8*f5 + 6*f44 + 7*f77 + 
       6*f86 - 6*f144 + 12*f187 + 14*f218 - 12*f260 - 12*f264)*
      tci11^2*tcr12)/6 - 
    (2*(2*f2 - 2*f3 + 2*f5 + f77 + 2*f218)*tcr12^3)/3 + 
    2*(f2 - f3 + f5 + 2*f77 + 4*f218)*tci12*tcr21 + 
    tcr11*(((-2*f2 + 2*f3 - 2*f5 - 7*f77 - 14*f218)*
        tci11^2)/6 - 2*(f2 - f3 + f5 + f77 + 2*f218)*tci12*
       tcr12 + 2*(f2 - f3 + f5)*tcr21) + 
    2*(2*f2 - 2*f3 + 2*f5 + f77 + 2*f218)*tcr31 + 
    ((2*f2 - 2*f3 + 2*f5 + f77 + 2*f218)*tcr32)/2 + 
    ((74*f2 - 74*f3 + 74*f5 + 18*f44 - 11*f77 + 18*f86 - 
       18*f144 + 36*f187 - 22*f218 - 36*f260 - 36*f264)*tcr33)/
     12;
L ieu0ueu4 = (-72*f2 + 72*f3 - 72*f5 + f70)/9 + 
    ((-f2 + f3 - f5 + 2*f77 + 4*f218)*fvu1u1u1^2)/2 + 
    ((-f2 + f3 - f5 - 2*f44 - 2*f77 - 2*f86 + 2*f144 - 
       4*f187 - 4*f218 + 4*f260 + 4*f264)*fvu1u1u4^2)/2 + 
    ((-f2 + f3 - f5 + 2*f44 + 2*f86 - 2*f144 + 4*f187 - 
       4*f260 - 4*f264)*fvu1u1u6^2)/2 + 
    ((-f2 + f3 - f5)*fvu1u1u7^2)/2 + 
    ((-f2 + f3 - f5)*fvu1u1u8^2)/2 + 2*(f77 + 2*f218)*
     fvu2u1u2 - 2*(f44 + f77 + f86 - f144 + 2*f187 + 2*f218 - 
      2*f260 - 2*f264)*fvu2u1u6 + 
    2*(f44 + f86 - f144 + 2*f187 - 2*f260 - 2*f264)*fvu2u1u8 + 
    ((-21*f2 + 21*f3 - 21*f5 - 4*f44 - 4*f86 + 4*f144 - 
       8*f187 + 8*f260 + 8*f264)*tci11^2)/12 + 
    ((-12*f2 + f91 + 12*f265)*tci12)/3 + 
    (f2 - f3 + f5)*fvu1u1u8*tci12 + 
    2*(f44 + f77 + f86 - f144 + 2*f187 + 2*f218 - 2*f260 - 
      2*f264)*fvu1u2u3*tci12 + 
    fvu1u1u4*((12*f2 + f80 - f91 - 12*f265 + 12*f270)/3 + 
      (f2 - f3 + f5 + 2*f77 + 4*f218)*fvu1u1u6 + 
      (-f2 + f3 - f5 + 2*f44 + 2*f86 - 2*f144 + 4*f187 - 
        4*f260 - 4*f264)*fvu1u1u7 + (-f2 + f3 - f5)*fvu1u1u8 + 
      (f2 - f3 + f5)*tci12) + 
    fvu1u1u6*((-f2 - 11*f3 + 11*f5 + f91 - f94 - 12*f229 + 
        12*f265)/3 + (f2 - f3 + f5 - 2*f44 - 2*f86 + 2*f144 - 
        4*f187 + 4*f260 + 4*f264)*fvu1u1u7 + 
      (-f2 + f3 - f5)*fvu1u1u8 + (f2 - f3 + f5)*tci12) + 
    fvu1u1u1*((-f80 - 12*f270)/3 + (f2 - f3 + f5)*fvu1u1u4 + 
      (-f2 + f3 - f5 - 2*f77 - 4*f218)*fvu1u1u6 + 
      (-f2 + f3 - f5)*fvu1u1u7 + (f2 - f3 + f5)*fvu1u1u8 + 
      (-f2 + f3 - f5 - 2*f77 - 4*f218)*tci12) + 
    fvu1u1u7*((f94 + 12*f229)/3 + (f2 - f3 + f5)*fvu1u1u8 + 
      (f2 - f3 + f5 - 2*f44 - 2*f86 + 2*f144 - 4*f187 + 
        4*f260 + 4*f264)*tci12);
L ieum1ueu4 = 
   (-11*(f2 - f3 + f5))/3 + (f2 - f3 + f5)*fvu1u1u1 + 
    (f2 - f3 + f5)*fvu1u1u4 + (f2 - f3 + f5)*fvu1u1u6 + 
    (f2 - f3 + f5)*fvu1u1u7 + (f2 - f3 + f5)*fvu1u1u8 - 
    3*(f2 - f3 + f5)*tci12;
L ieum2ueu4 = 
   -5*(f2 - f3 + f5);
L ieu2uou4 = ((2*f107 + f232)*fvu4u25)/6 - 
    (7*(2*f107 + f232)*fvu4u28)/6 + ((-2*f107 - f232)*fvu4u39)/6 + 
    ((2*f107 + f232)*fvu4u41)/6 + ((-2*f107 - f232)*fvu4u49)/18 + 
    ((-2*f107 - f232)*fvu4u51)/6 + ((-2*f107 - f232)*fvu4u80)/6 + 
    (11*(2*f107 + f232)*fvu4u83)/6 - (3*(2*f107 + f232)*fvu4u91)/
     2 + ((-2*f107 - f232)*fvu4u93)/6 + ((2*f107 + f232)*fvu4u100)/
     18 + (7*(2*f107 + f232)*fvu4u102)/18 + 
    ((2*f107 + f232)*fvu4u139)/3 + ((-2*f107 - f232)*fvu4u148)/9 - 
    (5*(2*f107 + f232)*fvu4u171)/24 + (3*(2*f107 + f232)*fvu4u174)/
     8 + (11*(2*f107 + f232)*fvu4u182)/8 - 
    (5*(2*f107 + f232)*fvu4u184)/24 + ((-2*f107 - f232)*fvu4u190)/
     6 + (17*(2*f107 + f232)*fvu4u192)/36 + 
    ((-2*f107 - f232)*fvu4u199)/8 + ((-2*f107 - f232)*fvu4u201)/4 + 
    ((-2*f107 - f232)*fvu4u202)/2 + ((-2*f107 - f232)*fvu4u213)/3 + 
    ((2*f107 + f232)*fvu4u215)/3 + ((-2*f107 - f232)*fvu4u221)/3 - 
    (3*(2*f107 + f232)*fvu4u244)/2 - (4*(2*f107 + f232)*fvu4u246)/
     3 + (2*f107 + f232)*fvu4u250 + (13*(2*f107 + f232)*fvu4u252)/
     12 + ((2*f107 + f232)*fvu4u255)/4 + 
    (5*(2*f107 + f232)*fvu4u271)/24 + (43*(2*f107 + f232)*fvu4u273)/
     24 - (19*(2*f107 + f232)*fvu4u277)/24 + 
    (5*(2*f107 + f232)*fvu4u279)/24 + ((2*f107 + f232)*fvu4u282)/
     6 + ((2*f107 + f232)*fvu4u289)/8 + ((2*f107 + f232)*fvu4u290)/
     4 + ((2*f107 + f232)*fvu4u291)/2 + ((2*f107 + f232)*fvu4u293)/
     3 + ((2*f107 + f232)*fvu4u295)/3 + (2*f107 + f232)*fvu4u309 + 
    (-2*f107 - f232)*fvu4u313 + 
    fvu3u43*(((-2*f107 - f232)*fvu1u1u6)/12 + 
      ((2*f107 + f232)*fvu1u1u7)/12 + ((2*f107 + f232)*fvu1u1u9)/
       12) + fvu3u23*(((-2*f107 - f232)*fvu1u1u2)/12 + 
      ((-2*f107 - f232)*fvu1u1u6)/12 + ((2*f107 + f232)*fvu1u1u7)/
       12 + ((2*f107 + f232)*fvu1u1u9)/12) + 
    fvu3u25*((7*(2*f107 + f232)*fvu1u1u1)/12 + 
      ((2*f107 + f232)*fvu1u1u2)/6 + ((-2*f107 - f232)*fvu1u1u3)/
       12 + ((-2*f107 - f232)*fvu1u1u4)/2 + 
      ((2*f107 + f232)*fvu1u1u5)/3 + ((2*f107 + f232)*fvu1u1u6)/
       4 - (7*(2*f107 + f232)*fvu1u1u7)/12 + 
      ((-2*f107 - f232)*fvu1u1u10)/6) + 
    fvu3u81*(((-2*f107 - f232)*fvu1u1u3)/12 + 
      ((-2*f107 - f232)*fvu1u1u4)/6 + ((-2*f107 - f232)*fvu1u1u6)/
       12 + ((2*f107 + f232)*fvu1u1u7)/12 + 
      ((2*f107 + f232)*fvu1u1u9)/6 + ((-2*f107 - f232)*fvu1u1u10)/
       6) + fvu3u70*(((-2*f107 - f232)*fvu1u1u2)/12 + 
      ((2*f107 + f232)*fvu1u1u3)/12 + ((-2*f107 - f232)*fvu1u1u6)/
       6 + ((2*f107 + f232)*fvu1u1u8)/6 + 
      ((2*f107 + f232)*fvu1u1u9)/12 + ((2*f107 + f232)*fvu1u1u10)/
       6) + fvu3u63*(((-2*f107 - f232)*fvu1u1u6)/6 + 
      ((-2*f107 - f232)*fvu1u1u7)/6 + ((-2*f107 - f232)*fvu1u1u9)/
       6 + ((2*f107 + f232)*fvu1u1u10)/3) + 
    (13*(2*f107 + f232)*tci11^3*tci12)/30 + 
    fvu3u78*(((-2*f107 - f232)*fvu1u1u2)/6 + 
      ((2*f107 + f232)*fvu1u1u3)/6 + ((2*f107 + f232)*fvu1u1u4)/
       6 + ((-2*f107 - f232)*fvu1u1u6)/6 + 
      ((2*f107 + f232)*fvu1u1u8)/6 + ((-2*f107 - f232)*tci12)/
       3) + fvu3u45*(((-2*f107 - f232)*fvu1u1u2)/4 + 
      ((-2*f107 - f232)*fvu1u1u3)/12 + ((2*f107 + f232)*fvu1u1u4)/
       2 + ((-2*f107 - f232)*fvu1u1u5)/3 + 
      (7*(2*f107 + f232)*fvu1u1u6)/6 - 
      (5*(2*f107 + f232)*fvu1u1u7)/6 + (2*f107 + f232)*fvu1u1u8 + 
      ((2*f107 + f232)*fvu1u1u9)/12 + ((2*f107 + f232)*fvu1u1u10)/
       6 + (-2*f107 - f232)*tci12) + 
    fvu3u80*(((2*f107 + f232)*fvu1u1u2)/3 + 
      ((2*f107 + f232)*fvu1u1u3)/6 + ((-2*f107 - f232)*fvu1u1u4)/
       3 + ((-2*f107 - f232)*fvu1u1u5)/3 + 
      (11*(2*f107 + f232)*fvu1u1u6)/12 - 
      (7*(2*f107 + f232)*fvu1u1u7)/12 + 
      (3*(2*f107 + f232)*fvu1u1u8)/4 - 
      (13*(2*f107 + f232)*fvu1u1u9)/12 - 
      (3*(2*f107 + f232)*tci12)/4) + 
    fvu3u82*(((-2*f107 - f232)*fvu1u1u3)/6 + 
      ((-2*f107 - f232)*fvu1u1u4)/6 + ((2*f107 + f232)*fvu1u1u7)/
       6 + ((-2*f107 - f232)*fvu1u1u8)/6 + 
      ((2*f107 + f232)*fvu1u1u9)/6 + ((2*f107 + f232)*tci12)/
       3) + fvu3u71*(((2*f107 + f232)*fvu1u1u2)/6 + 
      ((-2*f107 - f232)*fvu1u1u4)/2 + ((2*f107 + f232)*fvu1u1u5)/
       3 + ((2*f107 + f232)*fvu1u1u7)/2 + 
      ((-2*f107 - f232)*fvu1u1u8)/6 + (2*f107 + f232)*tci12) - 
    (5*(2*f107 + f232)*tci11^2*tci21)/27 + 
    fvu1u1u10*((2*(2*f107 + f232)*tci11^3)/81 - 
      (2*(2*f107 + f232)*tci12*tci21)/3) + 
    tci12*((8*(2*f107 + f232)*tci31)/5 + 
      8*(2*f107 + f232)*tci32) - (1036*(2*f107 + f232)*tci41)/
     135 - (32*(2*f107 + f232)*tci42)/5 - 
    (8*(2*f107 + f232)*tci43)/3 + 
    ((217*(2*f107 + f232)*tci11^3)/810 + 
      (2*(2*f107 + f232)*tci12*tci21)/3 + 
      8*(2*f107 + f232)*tci32)*tcr11 + 
    (13*(2*f107 + f232)*tci11*tcr11^3)/90 + 
    fvu1u1u7*((-229*(2*f107 + f232)*tci11^3)/3240 + 
      (37*(2*f107 + f232)*tci12*tci21)/18 + 
      (2*(2*f107 + f232)*tci31)/5 + 
      ((2*f107 + f232)*tci21*tcr11)/6 + 
      ((-2*f107 - f232)*tci11*tcr11^2)/120) + 
    fvu1u1u5*(((-2*f107 - f232)*tci11^3)/45 + 
      (16*(2*f107 + f232)*tci12*tci21)/9 + 
      (16*(2*f107 + f232)*tci31)/5 + 
      (4*(2*f107 + f232)*tci21*tcr11)/3 + 
      ((-2*f107 - f232)*tci11*tcr11^2)/15) + 
    fvu1u1u1*((-5*(2*f107 + f232)*fvu3u45)/12 + 
      ((-2*f107 - f232)*fvu3u70)/4 + ((-2*f107 - f232)*fvu3u71)/3 + 
      ((-2*f107 - f232)*fvu3u78)/6 + ((2*f107 + f232)*fvu3u80)/6 + 
      ((2*f107 + f232)*fvu3u81)/4 + ((2*f107 + f232)*fvu3u82)/6 + 
      (53*(2*f107 + f232)*tci11^3)/810 + 
      (24*(2*f107 + f232)*tci31)/5 + 2*(2*f107 + f232)*tci21*
       tcr11 + ((-2*f107 - f232)*tci11*tcr11^2)/10) + 
    fvu1u1u2*(((-2*f107 - f232)*fvu3u43)/12 + 
      ((2*f107 + f232)*fvu3u63)/6 + (137*(2*f107 + f232)*
        tci11^3)/3240 + ((-2*f107 - f232)*tci12*tci21)/9 + 
      (14*(2*f107 + f232)*tci31)/5 + 
      (7*(2*f107 + f232)*tci21*tcr11)/6 - 
      (7*(2*f107 + f232)*tci11*tcr11^2)/120) + 
    fvu1u1u9*((-91*(2*f107 + f232)*tci11^3)/1080 + 
      (11*(2*f107 + f232)*tci12*tci21)/6 - 
      (6*(2*f107 + f232)*tci31)/5 + 
      ((-2*f107 - f232)*tci21*tcr11)/2 + 
      ((2*f107 + f232)*tci11*tcr11^2)/40) + 
    fvu1u1u8*((2*(2*f107 + f232)*tci11^3)/135 - 
      (13*(2*f107 + f232)*tci12*tci21)/6 - 
      (24*(2*f107 + f232)*tci31)/5 - 2*(2*f107 + f232)*tci21*
       tcr11 + ((2*f107 + f232)*tci11*tcr11^2)/10) + 
    fvu1u1u4*((-11*(2*f107 + f232)*tci11^3)/270 - 
      (2*(2*f107 + f232)*tci12*tci21)/3 - 
      (24*(2*f107 + f232)*tci31)/5 - 2*(2*f107 + f232)*tci21*
       tcr11 + ((2*f107 + f232)*tci11*tcr11^2)/10) + 
    fvu1u1u6*((97*(2*f107 + f232)*tci11^3)/3240 - 
      (49*(2*f107 + f232)*tci12*tci21)/18 - 
      (26*(2*f107 + f232)*tci31)/5 - 
      (13*(2*f107 + f232)*tci21*tcr11)/6 + 
      (13*(2*f107 + f232)*tci11*tcr11^2)/120) + 
    ((-109*(2*f107 + f232)*tci11^3)/243 - 
      (20*(2*f107 + f232)*tci12*tci21)/3)*tcr12 + 
    (-2*(2*f107 + f232)*tci11*tci12 - 
      (20*(2*f107 + f232)*tci21)/3)*tcr12^2 + 
    tcr11^2*(((-2*f107 - f232)*tci11*tci12)/30 + 
      (-2*f107 - f232)*tci21 + (-2*f107 - f232)*tci11*
       tcr12) + (65*(2*f107 + f232)*tci11*tcr33)/27;
L ieu1uou4 = (2*f107 + f232)*fvu3u45 - 
    (11*(2*f107 + f232)*tci11^3)/270 - 
    (2*(2*f107 + f232)*tci12*tci21)/3 - 
    (24*(2*f107 + f232)*tci31)/5 - 2*(2*f107 + f232)*tci21*
     tcr11 + ((2*f107 + f232)*tci11*tcr11^2)/10;
L ieu0uou4 = 0;
L ieum1uou4 = 0;
L ieum2uou4 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou4+w^2*ieu1uou4+w^3*ieu0uou4+w^4*ieum1uou4+w^5*ieum2uou4;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou4a = K[w^1];
L ieu1uou4a = K[w^2];
L ieu0uou4a = K[w^3];
L ieum1uou4a = K[w^4];
L ieum2uou4a = K[w^5];
.sort
#write <e4.tmp> "`optimmaxvar_'"
#write <e4_odd.c> "%O"
#write <e4_odd.c> "return Eps5o2<T>("
#write <e4_odd.c> "%E", ieu2uou4a
#write <e4_odd.c> ", "
#write <e4_odd.c> "%E", ieu1uou4a
#write <e4_odd.c> ", "
#write <e4_odd.c> "%E", ieu0uou4a
#write <e4_odd.c> ", "
#write <e4_odd.c> "%E", ieum1uou4a
#write <e4_odd.c> ", "
#write <e4_odd.c> "%E", ieum2uou4a
#write <e4_odd.c> ");\n}"
L H=+u^1*ieu2ueu4+u^2*ieu1ueu4+u^3*ieu0ueu4+u^4*ieum1ueu4+u^5*ieum2ueu4;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu4a = H[u^1];
L ieu1ueu4a = H[u^2];
L ieu0ueu4a = H[u^3];
L ieum1ueu4a = H[u^4];
L ieum2ueu4a = H[u^5];
.sort
#write <e4.tmp> "`optimmaxvar_'"
#write <e4_even.c> "%O"
#write <e4_even.c> "return Eps5o2<T>("
#write <e4_even.c> "%E", ieu2ueu4a
#write <e4_even.c> ", "
#write <e4_even.c> "%E", ieu1ueu4a
#write <e4_even.c> ", "
#write <e4_even.c> "%E", ieu0ueu4a
#write <e4_even.c> ", "
#write <e4_even.c> "%E", ieum1ueu4a
#write <e4_even.c> ", "
#write <e4_even.c> "%E", ieum2ueu4a
#write <e4_even.c> ");\n}"
.end
