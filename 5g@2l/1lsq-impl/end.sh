#!/usr/bin/env bash

l=1
dir=new_exprs
for f in $dir/P${l}_*.m; do
    g=${f#$dir/P${l}_epsexp_}
    h=${g%_lr*.m}
    d=hel_${h}

    cp $d/coeffs.cpp new_output/0q5g-1lsq-analytic-$h-coeffs.cpp

    cp $d/eps.cpp new_output/0q5g-1lsq-analytic-$h.cpp

done
