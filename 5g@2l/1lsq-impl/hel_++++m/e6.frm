#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci43;
S tci42;
S tci41;
S tci21;
S tcr31;
S tcr33;
S tcr32;
S f137;
S f135;
S f133;
S f131;
S f138;
S f124;
S f126;
S f121;
S f122;
S f123;
S f128;
S f129;
S f79;
S f159;
S f148;
S f146;
S f147;
S f82;
S f145;
S f142;
S f143;
S f140;
S f177;
S f44;
S f42;
S f58;
S f59;
S f57;
S fvu1u2u9;
S fvu3u78;
S fvu4u25;
S fvu4u221;
S fvu4u28;
S fvu3u71;
S fvu1u2u3;
S fvu3u70;
S fvu1u2u2;
S fvu4u225;
S fvu1u2u6;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u289;
S fvu4u234;
S fvu4u146;
S fvu4u235;
S fvu1u1u1;
S fvu1u1u6;
S fvu1u1u7;
S fvu4u233;
S fvu4u141;
S fvu3u18;
S fvu1u1u4;
S fvu1u1u5;
S fvu4u282;
S fvu3u61;
S fvu3u62;
S fvu1u1u8;
S fvu3u17;
S fvu3u63;
S fvu1u1u9;
S fvu4u148;
S fvu3u11;
S fvu3u13;
S fvu3u12;
S fvu4u202;
S fvu2u1u11;
S fvu4u201;
S fvu2u1u12;
S fvu4u132;
S fvu2u1u13;
S fvu2u1u14;
S fvu2u1u15;
S fvu4u39;
S fvu4u290;
S fvu4u139;
S fvu4u291;
S fvu4u293;
S fvu4u295;
S fvu3u82;
S fvu2u1u1;
S fvu3u80;
S fvu2u1u3;
S fvu3u35;
S fvu3u81;
S fvu4u213;
S fvu2u1u2;
S fvu4u215;
S fvu2u1u4;
S fvu3u8;
S fvu2u1u6;
S fvu2u1u9;
S fvu3u6;
S fvu4u219;
S fvu3u7;
S fvu3u4;
S fvu3u5;
S fvu3u3;
S fvu4u83;
S fvu3u24;
S fvu3u25;
S fvu4u273;
S fvu4u271;
S fvu4u80;
S fvu4u277;
S fvu3u23;
S fvu4u129;
S fvu4u279;
S fvu4u313;
S fvu3u51;
S fvu4u91;
S fvu3u53;
S fvu4u309;
S fvu4u93;
S fvu3u52;
S fvu3u55;
S fvu3u57;
S fvu3u56;
S fvu4u113;
S fvu3u59;
S fvu1u1u10;
S fvu4u111;
S fvu4u114;
S fvu4u192;
S fvu4u190;
S fvu4u41;
S fvu4u100;
S fvu4u246;
S fvu4u174;
S fvu4u199;
S fvu4u102;
S fvu4u244;
S fvu4u171;
S fvu4u49;
S fvu4u182;
S fvu4u184;
S fvu4u51;
S fvu4u255;
S fvu3u45;
S fvu3u43;
S fvu4u252;
ExtraSymbols,array,Z;

L ieu2ueu6 = (-99*f57 - 11*f58 + 11*f59 - 11*f122 + 
      63*f126 + 11*f159 - 11*f177)/27 - (f121*fvu3u3)/9 + 
    ((f121 - 2*f140)*fvu3u4)/12 - (11*f121*fvu3u5)/36 + 
    (f121*fvu3u6)/9 - (f121*fvu3u7)/6 - (8*f121*fvu3u8)/9 - 
    (f121*fvu3u11)/9 + (f121*fvu3u12)/36 + (f121*fvu3u13)/36 - 
    (f121*fvu3u17)/12 + (f121*fvu3u18)/12 + (f121*fvu3u24)/9 + 
    (f145*fvu3u35)/6 + (f121*fvu3u51)/9 + 
    ((f121 - 3*f142)*fvu3u52)/18 + (f146*fvu3u53)/6 - 
    (2*f121*fvu3u55)/9 - (f121*fvu3u56)/9 - (f121*fvu3u57)/6 + 
    (f121*fvu3u59)/6 + (f121*fvu3u61)/3 + (f121*fvu1u1u1^3)/18 - 
    (11*f121*fvu1u1u2^3)/54 - (f121*fvu1u1u6^3)/9 + 
    (f121*fvu1u1u8^3)/18 - (8*f121*fvu1u1u9^3)/27 + 
    fvu1u1u4^2*((-6*f42 + f44 + 3*f123 + 6*f145 + f147)/18 - 
      (f121*fvu1u1u5)/36 + (f121*fvu1u1u8)/36 - 
      (7*f121*fvu1u1u9)/36) + fvu1u1u3^2*
     ((3*f135 + f143 - f148)/18 + (f121*fvu1u1u4)/12 + 
      (f121*fvu1u1u6)/18 + (f121*fvu1u1u8)/12 + 
      (f121*fvu1u1u9)/9 - (f121*fvu1u1u10)/6) + 
    ((-6*f42 + f44 + 6*f145 + f147)*fvu2u1u6)/9 + 
    ((f143 - f148)*fvu2u1u13)/9 + ((f143 - f147)*fvu2u1u14)/9 + 
    (f121*fvu1u2u6*tci11^2)/3 - (f121*fvu1u2u9*tci11^2)/3 + 
    ((f124 + f133 + f137)*tci12)/9 + 
    ((-f143 + f148)*fvu1u2u2*tci12)/9 + 
    ((6*f42 - f44 - 6*f145 - f147)*fvu1u2u3*tci12)/9 + 
    (f121*fvu2u1u3*tci12)/3 - (2*f121*fvu2u1u9*tci12)/9 + 
    (2*f121*fvu2u1u11*tci12)/9 + (f121*fvu2u1u12*tci12)/3 + 
    fvu2u1u1*((6*f79 - f82 - 6*f140 + f148)/9 - 
      (2*f121*tci12)/3) + fvu1u1u8^2*(-(f121*fvu1u1u9)/18 + 
      (f121*fvu1u1u10)/6 - (f121*tci12)/2) + 
    fvu2u1u2*((6*f42 - f44 + 6*f79 - f82 - 6*f140 - 6*f145)/9 - 
      (2*f121*tci12)/9) + fvu1u1u1^2*
     ((6*f42 - f44 + 12*f79 - 2*f82 + 3*f131 - 12*f140 - 
        6*f145 + f148)/18 + (f121*fvu1u1u2)/18 - 
      (f121*fvu1u1u4)/18 - (f121*fvu1u1u8)/18 - 
      (f121*fvu1u1u9)/18 - (f121*fvu1u1u10)/6 - 
      (f121*tci12)/18) + fvu1u1u6^2*
     ((-3*f123 + 3*f126 - 3*f128 - 3*f131 - 3*f135 + f143 - 
        f147)/18 + (f121*fvu1u1u9)/6 - (f121*fvu1u1u10)/9 - 
      (f121*tci12)/18) + fvu1u1u5^2*(-(f121*fvu1u1u9)/12 + 
      (f121*tci12)/36) + fvu1u1u9^2*(-(f121*fvu1u1u10)/18 + 
      (f121*tci12)/12) + fvu1u1u10^2*
     (f128/6 + (f121*tci12)/6) + fvu1u1u2^2*
     (-(f121*fvu1u1u4)/18 + (f121*fvu1u1u5)/18 - 
      (f121*fvu1u1u8)/18 - (f121*fvu1u1u9)/9 + 
      (2*f121*tci12)/9) + tci11^2*
     ((12*f79 - 2*f82 + 18*f123 + 3*f126 + 18*f131 + 18*f135 - 
        12*f140 + 2*f143 + 2*f147)/108 + 
      ((5*f121 - 2*f140 + f142 - 3*f145 + f146)*tci12)/36) + 
    fvu1u1u10*(-f129/9 - (2*f121*fvu2u1u14)/9 - 
      (4*f121*fvu2u1u15)/9 - (f121*tci11^2)/9 + 
      (f147*tci12)/9) + fvu1u1u5*(-(f121*fvu1u1u9^2)/12 - 
      (f121*fvu2u1u4)/9 - (f121*tci11^2)/27 - 
      (f121*fvu1u1u9*tci12)/6) + 
    fvu1u1u8*((f121*fvu1u1u9^2)/9 + (f121*fvu2u1u9)/9 - 
      (2*f121*fvu2u1u11)/9 - (f121*fvu2u1u12)/3 + 
      (73*f121*tci11^2)/108 - (2*f121*fvu1u1u9*tci12)/9 - 
      (f121*fvu1u1u10*tci12)/3 + (f121*fvu1u2u9*tci12)/3) + 
    fvu1u1u9*((f121*fvu1u1u10^2)/6 - (7*f121*fvu2u1u11)/9 - 
      (f121*fvu2u1u12)/3 - (4*f121*fvu2u1u15)/9 - 
      (7*f121*tci11^2)/54 - (f121*fvu1u1u10*tci12)/9 + 
      (f121*fvu1u2u9*tci12)/3) + 
    fvu1u1u6*((9*f57 + f58 - f59 + f122 + f124 - 9*f126 + 
        f129 + f133 + f137 - f159 + f177)/9 + 
      (f121*fvu1u1u9^2)/6 - (2*f121*fvu2u1u14)/9 + 
      (2*f121*tci11^2)/27 + (f143*tci12)/9 + 
      (f121*fvu1u1u9*tci12)/3 + fvu1u1u10*
       (f147/9 - (2*f121*tci12)/9)) + 
    fvu1u1u2*((2*f121*fvu1u1u4^2)/9 + (f121*fvu1u1u5^2)/9 + 
      (f121*fvu1u1u8^2)/18 + (5*f121*fvu1u1u9^2)/18 - 
      (f121*fvu2u1u3)/3 - (f121*fvu2u1u4)/9 - 
      (5*f121*fvu2u1u11)/9 + (f121*tci11^2)/27 + 
      (f121*fvu1u1u5*tci12)/9 - (2*f121*fvu1u1u9*tci12)/9 - 
      (f121*fvu1u2u6*tci12)/3 + fvu1u1u8*((f121*fvu1u1u9)/9 - 
        (f121*tci12)/9) + fvu1u1u4*(-(f121*fvu1u1u5)/9 + 
        (f121*fvu1u1u8)/9 + (2*f121*fvu1u1u9)/9 - 
        (f121*tci12)/9)) + fvu1u1u4*(-f124/9 - 
      (f121*fvu1u1u5^2)/36 + ((6*f42 - f44 - 6*f145)*fvu1u1u6)/
       9 + (f121*fvu1u1u8^2)/36 - (7*f121*fvu1u1u9^2)/36 - 
      (f147*fvu1u1u10)/9 + (f121*fvu2u1u2)/9 - 
      (f121*fvu2u1u3)/3 - (2*f121*fvu2u1u11)/9 + 
      (f121*tci11^2)/27 - (f123*tci12)/3 + 
      (7*f121*fvu1u1u9*tci12)/18 - (f121*fvu1u2u6*tci12)/3 + 
      fvu1u1u8*(-(f121*fvu1u1u9)/9 - (f121*tci12)/18) + 
      fvu1u1u5*((f121*fvu1u1u9)/6 + (f121*tci12)/18)) + 
    fvu1u1u1*(-f133/9 + (2*f121*fvu1u1u2^2)/9 - 
      (f121*fvu1u1u3^2)/6 - (f121*fvu1u1u4^2)/9 + 
      ((-6*f42 + f44 + 6*f145)*fvu1u1u6)/9 - 
      (f121*fvu1u1u8^2)/9 - (f121*fvu1u1u9^2)/9 + 
      (f121*fvu2u1u1)/3 + (f121*fvu2u1u2)/9 + (f121*fvu2u1u9)/9 + 
      (2*f121*fvu2u1u11)/9 - (23*f121*tci11^2)/54 + 
      ((-6*f42 + f44 - 3*f131 + 6*f145 - f148)*tci12)/9 - 
      (f121*fvu1u1u9*tci12)/9 + fvu1u1u2*(-(f121*fvu1u1u4)/9 - 
        (f121*fvu1u1u8)/9 - (f121*fvu1u1u9)/9 + 
        (f121*tci12)/9) + fvu1u1u8*((f121*fvu1u1u9)/9 + 
        (2*f121*tci12)/9) + fvu1u1u4*((-6*f79 + f82 + 6*f140)/
         9 + (f121*fvu1u1u8)/9 + (f121*fvu1u1u9)/9 + 
        (2*f121*tci12)/9) + fvu1u1u3*((-6*f79 + f82 + 6*f140)/
         9 + (f121*tci12)/3) + fvu1u1u10*(-f148/9 + 
        (f121*tci12)/3)) + fvu1u1u3*(-f137/9 + 
      (f121*fvu1u1u4^2)/12 + (f121*fvu1u1u6^2)/18 + 
      (f121*fvu1u1u8^2)/12 + (f121*fvu1u1u9^2)/9 - 
      (f121*fvu1u1u10^2)/6 + (f121*fvu2u1u1)/3 + 
      (f121*tci11^2)/36 - (f135*tci12)/3 - 
      (f121*fvu1u1u8*tci12)/6 + fvu1u1u9*((f121*fvu1u1u10)/9 - 
        (2*f121*tci12)/9) + fvu1u1u4*
       ((6*f79 - f82 - 6*f140)/9 - (f121*fvu1u1u8)/6 - 
        (f121*tci12)/6) + fvu1u1u6*(-f143/9 - 
        (f121*fvu1u1u9)/3 + (2*f121*fvu1u1u10)/9 - 
        (f121*tci12)/9) + fvu1u1u10*(f148/9 + 
        (f121*tci12)/3)) + ((10*f121 - 6*f140 - 9*f145)*tci12*
      tcr11^2)/18 + ((-2*f121 + 4*f140 + f145)*tcr11^3)/18 + 
    ((7*f121 + 2*f140 - 6*f142 - 7*f145 - 6*f146)*tci11^2*
      tcr12)/36 + ((-f121 - 2*f140 + f145)*tcr12^3)/9 + 
    (2*(8*f121 - 3*f140 - 3*f145)*tci12*tcr21)/9 + 
    tcr11*(((25*f121 + 21*f145)*tci11^2)/108 + 
      ((-f121 + f145)*tci12*tcr12)/3 + 
      ((-5*f121 + 6*f140)*tcr21)/9) + 
    ((f121 + 2*f140 - f145)*tcr31)/3 + 
    ((f121 + 2*f140 - f145)*tcr32)/12 + 
    ((-11*f121 - 74*f140 - 18*f142 + 11*f145 - 18*f146)*tcr33)/
     72;
L ieu1ueu6 = (-9*f57 - f58 + f59 - f122 + 9*f126 + 
      f159 - f177)/9 + ((f121 - 2*f140 - f145)*fvu1u1u1^2)/6 + 
    ((-f121 + f142)*fvu1u1u3^2)/6 + ((f145 + f146)*fvu1u1u4^2)/
     6 + ((f142 - f146)*fvu1u1u6^2)/6 + 
    fvu1u1u3*(-f135/3 - (f140*fvu1u1u4)/3 - (f142*fvu1u1u6)/3 + 
      (f121*fvu1u1u10)/3) + fvu1u1u4*(-f123/3 - 
      (f145*fvu1u1u6)/3 - (f146*fvu1u1u10)/3) + 
    ((f121 - f140)*fvu2u1u1)/3 + ((-f140 - f145)*fvu2u1u2)/3 + 
    ((f145 + f146)*fvu2u1u6)/3 + ((-f121 + f142)*fvu2u1u13)/3 + 
    ((f142 - f146)*fvu2u1u14)/3 + 
    ((-f140 + f142 + f146)*tci11^2)/18 + 
    ((f123 + f131 + f135)*tci12)/3 + 
    ((f121 - f142)*fvu1u2u2*tci12)/3 + 
    ((-f145 - f146)*fvu1u2u3*tci12)/3 + 
    fvu1u1u6*((f123 - f126 + f128 + f131 + f135)/3 + 
      (f146*fvu1u1u10)/3 + (f142*tci12)/3) + 
    fvu1u1u1*(-f131/3 + (f140*fvu1u1u3)/3 + (f140*fvu1u1u4)/3 + 
      (f145*fvu1u1u6)/3 - (f121*fvu1u1u10)/3 + 
      ((-f121 + f145)*tci12)/3) + 
    fvu1u1u10*(-f128/3 + (f146*tci12)/3);
L ieu0ueu6 = f126/3;
L ieum1ueu6 = 0;
L ieum2ueu6 = 0;
L ieu2uou6 = -(f138*fvu4u25)/3 - (f138*fvu4u28)/3 - 
    (7*f138*fvu4u39)/3 - (f138*fvu4u41)/3 + (f138*fvu4u49)/3 - 
    (5*f138*fvu4u51)/9 + (5*f138*fvu4u80)/12 - 
    (37*f138*fvu4u83)/12 - (7*f138*fvu4u91)/4 + 
    (5*f138*fvu4u93)/12 + (f138*fvu4u100)/3 - 
    (23*f138*fvu4u102)/18 + (f138*fvu4u111)/4 + (f138*fvu4u113)/2 + 
    f138*fvu4u114 - (8*f138*fvu4u129)/3 + (f138*fvu4u132)/3 - 
    (f138*fvu4u139)/6 - (8*f138*fvu4u141)/3 + (4*f138*fvu4u146)/9 - 
    (11*f138*fvu4u148)/18 + (5*f138*fvu4u171)/12 + 
    (23*f138*fvu4u174)/12 - (f138*fvu4u182)/12 + 
    (5*f138*fvu4u184)/12 + (f138*fvu4u190)/9 - (f138*fvu4u192)/18 + 
    (f138*fvu4u199)/4 + (f138*fvu4u201)/2 + f138*fvu4u202 + 
    (7*f138*fvu4u213)/12 - (f138*fvu4u215)/4 + (9*f138*fvu4u219)/4 + 
    (7*f138*fvu4u221)/12 - (4*f138*fvu4u225)/9 - (f138*fvu4u233)/4 - 
    (f138*fvu4u234)/2 - f138*fvu4u235 + (17*f138*fvu4u244)/3 + 
    (4*f138*fvu4u246)/3 - (7*f138*fvu4u252)/2 - 
    (17*f138*fvu4u255)/18 - (5*f138*fvu4u271)/12 - 
    (43*f138*fvu4u273)/12 + (67*f138*fvu4u277)/12 - 
    (5*f138*fvu4u279)/12 - (f138*fvu4u282)/3 - (f138*fvu4u289)/4 - 
    (f138*fvu4u290)/2 - f138*fvu4u291 - (2*f138*fvu4u293)/3 - 
    (2*f138*fvu4u295)/3 - 2*f138*fvu4u309 + 2*f138*fvu4u313 + 
    fvu3u78*((2*f138*fvu1u1u2)/3 - (2*f138*fvu1u1u3)/3 - 
      (2*f138*fvu1u1u9)/3) + fvu3u70*((f138*fvu1u1u2)/6 - 
      (f138*fvu1u1u3)/6 - (f138*fvu1u1u9)/6) + 
    fvu3u45*(-(f138*fvu1u1u2)/2 - (7*f138*fvu1u1u3)/6 - 
      (2*f138*fvu1u1u6)/3 + (2*f138*fvu1u1u7)/3 + 
      (f138*fvu1u1u9)/2) + fvu3u23*((f138*fvu1u1u1)/3 + 
      (f138*fvu1u1u2)/6 + (f138*fvu1u1u6)/2 - (f138*fvu1u1u7)/6 - 
      (f138*fvu1u1u8)/3 - (f138*fvu1u1u9)/6 - 
      (f138*fvu1u1u10)/3) + fvu3u43*(-(f138*fvu1u1u2)/6 - 
      (f138*fvu1u1u4)/3 - (f138*fvu1u1u6)/6 + (f138*fvu1u1u7)/6 + 
      (f138*fvu1u1u9)/2 - (f138*fvu1u1u10)/3) + 
    fvu3u25*(-(f138*fvu1u1u2)/3 - (7*f138*fvu1u1u3)/6 + 
      f138*fvu1u1u4 - (2*f138*fvu1u1u5)/3 + (5*f138*fvu1u1u6)/6 - 
      (f138*fvu1u1u7)/6 + (f138*fvu1u1u10)/3) + 
    fvu3u81*((f138*fvu1u1u3)/6 + (f138*fvu1u1u4)/3 + 
      (f138*fvu1u1u6)/6 - (f138*fvu1u1u7)/6 - (f138*fvu1u1u9)/3 + 
      (f138*fvu1u1u10)/3) + fvu3u62*((-4*f138*fvu1u1u2)/3 + 
      (2*f138*fvu1u1u4)/3 + (2*f138*fvu1u1u5)/3 - 
      (4*f138*fvu1u1u6)/3 + (2*f138*fvu1u1u7)/3 + 
      (4*f138*fvu1u1u10)/3) + (13*f138*tci11^3*tci12)/15 + 
    fvu3u71*(-(f138*fvu1u1u2)/3 + (4*f138*fvu1u1u3)/3 + 
      f138*fvu1u1u4 - (2*f138*fvu1u1u5)/3 - (4*f138*fvu1u1u6)/3 + 
      (f138*fvu1u1u7)/3 + (f138*fvu1u1u8)/3 - 2*f138*tci12) + 
    fvu3u82*((f138*fvu1u1u3)/3 + (f138*fvu1u1u4)/3 - 
      (f138*fvu1u1u7)/3 + (f138*fvu1u1u8)/3 - (f138*fvu1u1u9)/3 - 
      (2*f138*tci12)/3) + fvu3u63*((2*f138*fvu1u1u2)/3 + 
      (f138*fvu1u1u3)/2 - f138*fvu1u1u4 + (2*f138*fvu1u1u5)/3 - 
      (f138*fvu1u1u6)/2 - (f138*fvu1u1u7)/3 - (f138*fvu1u1u8)/3 - 
      (f138*fvu1u1u9)/3 - (f138*fvu1u1u10)/6 + 2*f138*tci12) + 
    fvu3u80*((2*f138*fvu1u1u2)/3 + f138*fvu1u1u3 - 
      (19*f138*fvu1u1u6)/6 + (19*f138*fvu1u1u7)/6 - 
      (17*f138*fvu1u1u8)/6 + (13*f138*fvu1u1u9)/6 + 
      (17*f138*tci12)/6) - (10*f138*tci11^2*tci21)/27 + 
    fvu1u1u8*((-17*f138*tci11^3)/81 + (17*f138*tci12*tci21)/
       3) + tci12*((16*f138*tci31)/5 + 16*f138*tci32) - 
    (2072*f138*tci41)/135 - (64*f138*tci42)/5 - 
    (16*f138*tci43)/3 + ((1259*f138*tci11^3)/1620 + 
      f138*tci12*tci21 + (84*f138*tci31)/5 + 
      16*f138*tci32)*tcr11 - (11*f138*tci11*tcr11^3)/
     180 + fvu1u1u2*((181*f138*tci11^3)/1620 + 
      (2*f138*tci12*tci21)/9 + (44*f138*tci31)/5 + 
      (11*f138*tci21*tcr11)/3 - (11*f138*tci11*tcr11^2)/
       60) + fvu1u1u6*((-13*f138*tci11^3)/162 + 
      (46*f138*tci12*tci21)/9 + 8*f138*tci31 + 
      (10*f138*tci21*tcr11)/3 - (f138*tci11*tcr11^2)/6) + 
    fvu1u1u3*((73*f138*tci11^3)/1620 - (f138*tci12*tci21)/
       3 + (12*f138*tci31)/5 + f138*tci21*tcr11 - 
      (f138*tci11*tcr11^2)/20) + 
    fvu1u1u9*((103*f138*tci11^3)/540 - 
      (49*f138*tci12*tci21)/9 - (4*f138*tci31)/5 - 
      (f138*tci21*tcr11)/3 + (f138*tci11*tcr11^2)/60) + 
    fvu1u1u5*((f138*tci11^3)/45 - (16*f138*tci12*tci21)/9 - 
      (16*f138*tci31)/5 - (4*f138*tci21*tcr11)/3 + 
      (f138*tci11*tcr11^2)/15) + 
    fvu1u1u4*((-31*f138*tci11^3)/270 + (4*f138*tci12*tci21)/
       3 - (24*f138*tci31)/5 - 2*f138*tci21*tcr11 + 
      (f138*tci11*tcr11^2)/10) + 
    fvu1u1u10*((-13*f138*tci11^3)/180 - (f138*tci12*tci21)/
       9 - (28*f138*tci31)/5 - (7*f138*tci21*tcr11)/3 + 
      (7*f138*tci11*tcr11^2)/60) + 
    fvu1u1u7*((221*f138*tci11^3)/1620 - 
      (19*f138*tci12*tci21)/3 - (36*f138*tci31)/5 - 
      3*f138*tci21*tcr11 + (3*f138*tci11*tcr11^2)/20) + 
    fvu1u1u1*((f138*fvu3u25)/6 + (f138*fvu3u43)/3 + 
      (7*f138*fvu3u45)/6 - (2*f138*fvu3u62)/3 - (7*f138*fvu3u63)/6 + 
      (f138*fvu3u70)/6 - (2*f138*fvu3u71)/3 + (2*f138*fvu3u78)/3 - 
      f138*fvu3u80 - (f138*fvu3u81)/2 - (f138*fvu3u82)/3 - 
      (179*f138*tci11^3)/1620 + (f138*tci12*tci21)/3 - 
      (36*f138*tci31)/5 - 3*f138*tci21*tcr11 + 
      (3*f138*tci11*tcr11^2)/20) + 
    ((-218*f138*tci11^3)/243 - (40*f138*tci12*tci21)/3)*
     tcr12 + (-4*f138*tci11*tci12 - (40*f138*tci21)/3)*
     tcr12^2 + tcr11^2*(-(f138*tci11*tci12)/15 + 
      5*f138*tci21 - 2*f138*tci11*tcr12) + 
    (130*f138*tci11*tcr33)/27;
L ieu1uou6 = 
   -2*f138*fvu3u63 - (11*f138*tci11^3)/135 - 
    (4*f138*tci12*tci21)/3 - (48*f138*tci31)/5 - 
    4*f138*tci21*tcr11 + (f138*tci11*tcr11^2)/5;
L ieu0uou6 = 0;
L ieum1uou6 = 0;
L ieum2uou6 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou6+w^2*ieu1uou6+w^3*ieu0uou6+w^4*ieum1uou6+w^5*ieum2uou6;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou6a = K[w^1];
L ieu1uou6a = K[w^2];
L ieu0uou6a = K[w^3];
L ieum1uou6a = K[w^4];
L ieum2uou6a = K[w^5];
.sort
#write <e6.tmp> "`optimmaxvar_'"
#write <e6_odd.c> "%O"
#write <e6_odd.c> "return Eps5o2<T>("
#write <e6_odd.c> "%E", ieu2uou6a
#write <e6_odd.c> ", "
#write <e6_odd.c> "%E", ieu1uou6a
#write <e6_odd.c> ", "
#write <e6_odd.c> "%E", ieu0uou6a
#write <e6_odd.c> ", "
#write <e6_odd.c> "%E", ieum1uou6a
#write <e6_odd.c> ", "
#write <e6_odd.c> "%E", ieum2uou6a
#write <e6_odd.c> ");\n}"
L H=+u^1*ieu2ueu6+u^2*ieu1ueu6+u^3*ieu0ueu6+u^4*ieum1ueu6+u^5*ieum2ueu6;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu6a = H[u^1];
L ieu1ueu6a = H[u^2];
L ieu0ueu6a = H[u^3];
L ieum1ueu6a = H[u^4];
L ieum2ueu6a = H[u^5];
.sort
#write <e6.tmp> "`optimmaxvar_'"
#write <e6_even.c> "%O"
#write <e6_even.c> "return Eps5o2<T>("
#write <e6_even.c> "%E", ieu2ueu6a
#write <e6_even.c> ", "
#write <e6_even.c> "%E", ieu1ueu6a
#write <e6_even.c> ", "
#write <e6_even.c> "%E", ieu0ueu6a
#write <e6_even.c> ", "
#write <e6_even.c> "%E", ieum1ueu6a
#write <e6_even.c> ", "
#write <e6_even.c> "%E", ieum2ueu6a
#write <e6_even.c> ");\n}"
.end
