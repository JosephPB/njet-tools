#-
#: WorkSpace 40M
#: Threads 60
S u,w,x1,x2,x3,x4,x5;
S tci32;
S tci31;
S tcr21;
S tcr12;
S tcr11;
S tci11;
S tci12;
S tci41;
S tci21;
S tcr33;
S f66;
S f64;
S f132;
S f131;
S f68;
S f74;
S f75;
S f76;
S f77;
S f70;
S f71;
S f73;
S f79;
S f83;
S f82;
S f84;
S f28;
S f45;
S f31;
S f47;
S fvu1u2u9;
S fvu1u2u8;
S fvu3u78;
S fvu4u221;
S fvu4u28;
S fvu3u71;
S fvu4u225;
S fvu1u2u7;
S fvu1u2u6;
S fvu1u1u2;
S fvu1u1u3;
S fvu4u289;
S fvu1u1u1;
S fvu1u1u6;
S fvu3u19;
S fvu1u1u7;
S fvu3u18;
S fvu1u1u4;
S fvu1u1u5;
S fvu3u15;
S fvu4u282;
S fvu3u14;
S fvu1u1u8;
S fvu3u17;
S fvu3u63;
S fvu1u1u9;
S fvu4u148;
S fvu3u11;
S fvu3u10;
S fvu3u13;
S fvu3u12;
S fvu2u1u10;
S fvu4u202;
S fvu2u1u11;
S fvu4u201;
S fvu2u1u12;
S fvu4u39;
S fvu4u290;
S fvu4u139;
S fvu4u291;
S fvu4u295;
S fvu3u82;
S fvu2u1u1;
S fvu3u37;
S fvu3u36;
S fvu3u80;
S fvu2u1u3;
S fvu3u81;
S fvu4u213;
S fvu2u1u2;
S fvu2u1u5;
S fvu4u215;
S fvu2u1u4;
S fvu3u32;
S fvu2u1u7;
S fvu3u31;
S fvu3u8;
S fvu2u1u9;
S fvu3u6;
S fvu2u1u8;
S fvu3u7;
S fvu3u4;
S fvu3u5;
S fvu3u2;
S fvu3u3;
S fvu3u38;
S fvu3u1;
S fvu4u83;
S fvu3u24;
S fvu3u25;
S fvu4u273;
S fvu4u271;
S fvu4u80;
S fvu4u277;
S fvu4u279;
S fvu4u91;
S fvu4u93;
S fvu1u1u10;
S fvu4u192;
S fvu4u330;
S fvu4u100;
S fvu4u174;
S fvu4u199;
S fvu4u102;
S fvu4u244;
S fvu4u171;
S fvu4u325;
S fvu4u182;
S fvu4u184;
S fvu4u51;
S fvu4u255;
S fvu3u45;
S fvu3u42;
S fvu3u43;
S fvu4u252;
S fvu3u40;
ExtraSymbols,array,Z;

L ieu2ueu11 = (52*f68)/27 + ((2*f76 + f83)*fvu3u1)/9 + 
    (f76*fvu3u2)/18 - (2*f76*fvu3u3)/9 - (2*f83*fvu3u4)/9 + 
    ((-3*f76 + 3*f82 + f83)*fvu3u5)/18 + ((-f76 + f83)*fvu3u6)/
     6 + (f76*fvu3u7)/6 + ((4*f76 - 9*f83)*fvu3u8)/18 + 
    ((-f76 + f83)*fvu3u10)/9 + ((-2*f76 - f83)*fvu3u11)/18 + 
    ((-8*f76 + 23*f83)*fvu3u12)/18 + ((-2*f76 + f83)*fvu3u13)/6 + 
    ((2*f76 - f83)*fvu3u14)/3 - (2*f83*fvu3u15)/9 + 
    ((2*f76 + f83)*fvu3u17)/3 - (2*(f76 - f83)*fvu3u18)/9 + 
    ((2*f76 + f83)*fvu3u19)/9 + (4*(f76 - f83)*fvu3u24)/9 + 
    ((2*f76 + f83)*fvu3u31)/6 - (2*f83*fvu3u32)/9 + 
    (2*f83*fvu3u36)/9 + ((-4*f76 + 7*f83)*fvu3u37)/18 + 
    ((4*f76 - 7*f83)*fvu3u38)/9 - (f83*fvu3u40)/3 + 
    (2*f76*fvu3u42)/3 + (2*(f76 - f83)*fvu1u1u1^3)/9 + 
    (f71*fvu1u1u2^3)/3 + (2*(f76 - f83)*fvu1u1u3^3)/9 + 
    ((f76 - f83)*fvu1u1u5^3)/9 - (f83*fvu1u1u6^3)/9 + 
    ((-f66 - f68 + f70 - f71 + f73)*fvu1u1u7^3)/3 + 
    ((2*f64 - f76 - f79)*fvu1u1u9^3)/6 + 
    fvu1u1u8^2*(f70/6 + ((8*f76 - 29*f83)*fvu1u1u9)/18) + 
    fvu1u1u4^2*(((-3*f76 + f83)*fvu1u1u5)/9 + 
      ((f76 - f83)*fvu1u1u8)/6 + ((f76 - f83)*fvu1u1u9)/3) - 
    (f76*fvu2u1u4)/6 - (4*f83*fvu2u1u7)/9 + (2*f83*fvu2u1u12)/9 + 
    ((-f79 - f83)*fvu1u2u6*tci11^2)/3 + 
    ((6*f45 - f47 - 6*f79 - 6*f83 + 6*f131 - f132)*fvu1u2u7*
      tci11^2)/9 + ((54*f76 - 13*f83)*tci12)/108 + 
    (f82*fvu1u2u9*tci12)/3 + (f83*fvu2u1u1*tci12)/3 + 
    ((-6*f28 + f31 + 6*f82)*fvu2u1u2*tci12)/9 + 
    ((-f82 + f83)*fvu2u1u3*tci12)/3 + 
    ((6*f28 - f31 - 6*f82 + 6*f83 - 6*f131 + f132)*fvu2u1u5*
      tci12)/9 + ((-f82 + f83)*fvu2u1u9*tci12)/3 + 
    fvu1u1u7^2*((-10*f76 - 3*f79 + 3*f82 - 7*f83)/108 + 
      ((2*f76 + f83)*fvu1u1u8)/3 - (2*(f76 - f83)*fvu1u1u9)/9 - 
      (4*f76*tci12)/3) + fvu1u2u8*(-(f82*tci11^2)/3 + 
      (f82*tci12)/3) + tci11^2*
     ((12*f45 - 2*f47 + 18*f66 + 21*f68 - 18*f70 + 18*f71 - 
        18*f73 + 12*f75 + 12*f76 - 2*f77 - 12*f79 - 48*f83 + 
        2*f84 + 48*f131 - 8*f132)/108 + ((f76 + f82)*tci12)/
       6) + fvu1u1u2^2*((-f79 - f83)/6 + (f83*fvu1u1u3)/6 + 
      ((-2*f76 - f83)*fvu1u1u4)/6 + ((2*f76 + f83)*fvu1u1u5)/3 - 
      (f83*fvu1u1u7)/3 - (f76*fvu1u1u8)/6 + 
      ((f76 + f83)*fvu1u1u9)/3 + ((f76 - f83)*tci12)/6) + 
    fvu1u1u3^2*((-2*f83*fvu1u1u4)/9 + (2*(f76 - 3*f83)*fvu1u1u7)/
       9 + (f76*fvu1u1u8)/6 + ((3*f76 - f83)*tci12)/18) + 
    fvu2u1u11*((f76 - f83)/6 + (f83*tci12)/6) + 
    fvu1u1u6^2*((f83*fvu1u1u7)/6 + (f83*fvu1u1u8)/6 + 
      (f83*tci12)/3) + fvu1u1u5^2*
     ((6*f28 - f31 - 3*f66 - 6*f82 + 6*f83 - 6*f131 + f132)/
       18 - (2*f83*fvu1u1u6)/9 + (2*f76*fvu1u1u7)/27 + 
      ((-2*f76 - f83)*fvu1u1u8)/18 + ((f76 + 2*f83)*fvu1u1u9)/
       18 + ((-4*f76 + f83)*tci12)/18) + 
    fvu1u1u9^2*((6*f45 - f47 + 3*f73 - 6*f75 - 6*f76 + f77 - 
        6*f79 + 2*f84)/18 + ((f79 + f83)*tci12)/3) + 
    fvu1u1u1^2*((2*(f76 - 4*f83)*fvu1u1u2)/9 - 
      (2*(4*f76 + 5*f83)*fvu1u1u3)/27 - (f76*fvu1u1u4)/6 + 
      ((-f76 - f83)*fvu1u1u7)/9 - (f76*fvu1u1u8)/6 + 
      ((-f76 - 2*f83)*fvu1u1u9)/9 + ((f76 + 8*f83)*tci12)/18) + 
    fvu1u1u9*((-8*f73)/9 + ((-6*f45 + f47 + 6*f79 + 6*f83 - 
         6*f131 + f132)*fvu2u1u11)/9 - 
      (2*(3*f76 - f83)*tci11^2)/27 + 
      ((6*f45 - f47 - 6*f79 + f84)*tci12)/9) + 
    fvu2u1u10*((-16*f83)/9 + ((6*f28 - f31 - 6*f82 + 6*f83 - 
         6*f131 + f132)*tci12)/9) + 
    fvu1u1u5*((8*f66)/9 - (2*f83*fvu1u1u6^2)/9 + 
      ((2*f76 + f83)*fvu1u1u7^2)/9 - (2*f76*fvu1u1u8^2)/9 + 
      (2*(f76 - f83)*fvu1u1u9^2)/9 + ((-f76 + 10*f83)*fvu2u1u4)/
       18 + (2*(f76 - 7*f83)*fvu2u1u5)/9 - 
      (2*(f76 - 3*f83)*fvu2u1u7)/9 + ((-2*f76 + f83)*tci11^2)/
       9 + (2*f83*tci12)/3 + (2*(f76 - f83)*fvu1u2u7*tci12)/
       9 + fvu1u1u9*((f76 - 4*f83)/18 + (2*(f76 - 4*f83)*tci12)/
         9) + fvu1u1u8*((-6*f83 + 6*f131 - f132)/9 + 
        (2*(f76 - f83)*tci12)/9) + fvu1u1u7*
       (f83/9 + (2*(f76 - f83)*fvu1u1u8)/9 - 
        (2*(3*f76 - f83)*tci12)/9) + 
      fvu1u1u6*((-4*f83*fvu1u1u8)/9 + (f83*tci12)/27)) + 
    fvu1u1u3*((2*f83*fvu1u1u4^2)/9 + ((-3*f76 + f83)*fvu1u1u5^2)/
       9 + (f83*fvu1u1u7^2)/9 + (f76*fvu1u1u8^2)/3 + 
      (2*f83*fvu2u1u1)/9 + ((-6*f75 - 6*f76 + f77)*fvu2u1u5)/9 + 
      ((3*f76 - f83)*tci11^2)/18 + (f76*fvu1u1u5*tci12)/6 - 
      (f76*fvu1u1u7*tci12)/3 + ((2*f76 - f83)*fvu1u1u8*
        tci12)/6 - (f76*fvu1u2u7*tci12)/3 + 
      fvu1u1u4*(((f76 - f83)*fvu1u1u8)/6 + (2*f83*tci12)/9)) + 
    fvu1u1u2*((-f66 - f68 + f70 - f71 + f73)/3 - 
      (2*f83*fvu1u1u3^2)/3 + ((-f76 + f83)*fvu1u1u4^2)/3 + 
      ((3*f76 - f83)*fvu1u1u5^2)/18 - (5*f83*fvu1u1u7^2)/9 + 
      ((f76 - 7*f83)*fvu1u1u8^2)/18 + ((-3*f76 + f83)*fvu1u1u9^2)/
       9 + ((f76 - 7*f83)*fvu2u1u3)/18 + 
      ((-f76 + 7*f83)*fvu2u1u4)/9 + (2*(f76 - 3*f83)*fvu2u1u11)/
       9 + ((6*f45 - f47 + 3*f66 + 3*f68 - 3*f70 + 3*f71 - 
         3*f73 - 6*f79 - 6*f83 + 6*f131 - f132)*tci11^2)/18 + 
      (f84*tci12)/9 + ((-f76 + 4*f83)*fvu1u2u6*tci12)/9 + 
      fvu1u1u5*(f76/6 + ((f76 - 4*f83)*tci12)/18) + 
      fvu1u1u9*((2*f76 + f83)/9 + ((f76 - 4*f83)*tci12)/18) + 
      fvu1u1u4*(((-2*f76 - f83)*fvu1u1u5)/3 + (2*f76*fvu1u1u8)/
         3 + ((2*f76 + f83)*fvu1u1u9)/3 + 
        (2*(f76 - 3*f83)*tci12)/9) + 
      fvu1u1u3*((2*f83*fvu1u1u7)/9 - (f83*tci12)/18) + 
      fvu1u1u7*(-f83/9 + (4*f83*tci12)/9) + 
      fvu1u1u8*(-f84/9 + ((3*f76 - f83)*fvu1u1u9)/18 + 
        ((-f76 + f83)*tci12)/3)) + 
    fvu1u1u4*(((-2*f76 - f83)*fvu1u1u5^2)/3 - 
      (2*(3*f76 - f83)*fvu1u1u8^2)/9 + (f76*fvu1u1u9^2)/18 - 
      (2*f76*fvu2u1u2)/9 + (f76*fvu2u1u3)/18 - 
      (f76*fvu2u1u11)/9 - (2*f76*tci11^2)/9 - 
      (f76*fvu1u1u9*tci12)/18 + (2*f76*fvu1u2u6*tci12)/9 + 
      fvu1u1u8*(-(f76*fvu1u1u9)/9 - (f76*tci12)/9) + 
      fvu1u1u5*((2*(3*f76 - f83)*fvu1u1u9)/9 + 
        ((2*f76 + f83)*tci12)/3)) + 
    fvu1u1u1*(-(f76*fvu1u1u2^2)/6 + (2*(f76 - f83)*fvu1u1u3^2)/
       9 - (f76*fvu1u1u4^2)/3 - (2*(f76 + 2*f83)*fvu1u1u5^2)/9 + 
      (2*(f76 - 3*f83)*fvu1u1u7^2)/9 + 
      (2*(f76 - 5*f83)*fvu1u1u8^2)/9 + ((-f76 - f83)*fvu1u1u9^2)/
       6 + (2*f83*fvu2u1u1)/9 + ((-2*f76 + 3*f83)*fvu2u1u2)/6 + 
      (f76*fvu2u1u9)/6 + ((-f76 + f83)*fvu2u1u11)/3 - 
      (f76*tci11^2)/3 + ((f76 - f83)*fvu1u1u9*tci12)/6 + 
      fvu1u1u3*(((-3*f76 + 2*f83)*fvu1u1u4)/9 - 
        (f76*tci12)/6) + fvu1u1u8*(((6*f76 - f83)*fvu1u1u9)/
         18 + (f76*tci12)/6) + fvu1u1u7*
       ((2*(f76 - 3*f83)*fvu1u1u9)/9 + (2*(3*f76 - f83)*tci12)/
         9) + fvu1u1u2*((f76*fvu1u1u4)/3 + (f76*fvu1u1u8)/3 - 
        (2*(f76 - 2*f83)*fvu1u1u9)/9 + (2*(f76 + 2*f83)*tci12)/
         9) + fvu1u1u4*((f76*fvu1u1u8)/3 + 
        ((6*f75 + 6*f76 - f77)*fvu1u1u9)/9 + 
        ((-2*f76 + 5*f83)*tci12)/9)) + 
    fvu1u1u8*((-f76 + 2*f83)/9 + (2*(10*f76 - 13*f83)*fvu1u1u9^2)/
       9 + ((-6*f45 + f47 + 6*f79)*fvu2u1u9)/9 - 
      (f79*fvu2u1u10)/3 + ((6*f45 - f47 - 6*f79)*fvu2u1u11)/9 + 
      (2*(f76 - 6*f83)*tci11^2)/27 - (f70*tci12)/3 + 
      (f79*fvu1u2u8*tci12)/3 + fvu1u1u9*((-f79 - 2*f83)/6 + 
        ((-4*f76 + 9*f83)*tci12)/18)) + 
    fvu1u1u7*((15*f76 - 11*f83)/27 - (4*(f76 - f83)*fvu1u1u8^2)/
       9 + ((f64 - f76)*fvu1u1u9^2)/3 - 
      (4*(f76 - 4*f83)*fvu2u1u7)/9 + (2*f83*fvu2u1u8)/9 - 
      (2*(f76 - f83)*fvu2u1u10)/9 + 
      ((f66 + f68 - f70 + f71 - f73)*tci11^2)/3 + 
      ((-4*f76 + 3*f79 - 3*f82 - 16*f83)*tci12)/18 + 
      ((-6*f28 + f31 + 6*f75 + 6*f76 - f77 + 6*f82)*fvu1u2u8*
        tci12)/9 + fvu1u1u8*(-f84/9 - (2*(f76 - 3*f83)*tci12)/
         9) + fvu1u1u9*((f76 + f82)/3 + 
        ((-6*f75 - 6*f76 + f77 + f84)*tci12)/9)) + 
    fvu1u1u6*(-(f83*fvu1u1u8^2)/3 - (f83*fvu2u1u8)/9 - 
      (f83*tci11^2)/9 + ((-6*f83 + 6*f131 - f132)*fvu1u1u8*
        tci12)/9 + fvu1u1u7*(-(f83*fvu1u1u8)/3 + 
        ((-6*f83 + 6*f131 - f132)*tci12)/9)) + 
    (f83*tci12*tcr11^2)/9 + (f83*tcr11^3)/18 - 
    (f83*tci11^2*tcr12)/3 + (f83*tci12*tcr21)/3 + 
    tcr11*((f83*tci11^2)/9 + (f83*tci12*tcr12)/6 + 
      (4*f83*tcr21)/9) + ((-12*f64 - 12*f76 + 9*f79 - 9*f82 + 
       26*f83)*tcr33)/36;
L ieu1ueu11 = 
   (8*f68)/9 + (8*f71*fvu1u1u2^2)/9 + ((-f82 + f83)*fvu1u1u5^2)/
     6 - (8*(f66 + f68 - f70 + f71 - f73)*fvu1u1u7^2)/9 - 
    (4*f83*fvu1u1u8^2)/27 + fvu1u1u8*((-8*f70)/9 - 
      (10*(f76 - f83)*fvu1u1u9)/9) + ((-3*f76 + f83)*fvu2u1u4)/
     18 + ((6*f83 - 6*f131 + f132)*fvu2u1u7)/9 - 
    (2*f83*fvu2u1u10)/9 + ((f64 + f76 - f83)*fvu2u1u11)/6 - 
    (f83*fvu2u1u12)/3 + ((f64 + f76 - f79 - 4*f83)*tci11^2)/
     18 + ((-6*f28 + f31 - 3*f71 + 6*f75 + 6*f76 - f77 + 6*f82)*
      tci12)/18 + ((6*f28 - f31 - 6*f82)*fvu1u2u8*tci12)/9 + 
    ((-6*f28 + f31 + 6*f82)*fvu1u2u9*tci12)/9 + 
    fvu1u1u2*(-f83/18 + ((-2*f76 - f83)*fvu1u1u5)/6 + 
      (f83*fvu1u1u7)/9 - (f64*fvu1u1u8)/3 + 
      ((f76 - 11*f83)*fvu1u1u9)/18 + (f64*tci12)/3) + 
    fvu1u1u9*(-f73/3 + ((f64 - f79)*tci12)/3) + 
    fvu1u1u5*(f66/3 - (f83*fvu1u1u7)/18 - (f83*fvu1u1u8)/3 - 
      (2*(f76 + f83)*fvu1u1u9)/9 + (4*f83*tci12)/9) + 
    fvu1u1u7*((8*(f66 + f68 - f70 + f71 - f73))/9 - 
      (f64*fvu1u1u8)/3 + ((-2*f76 - f83)*fvu1u1u9)/3 + 
      (4*(2*f76 + f83)*tci12)/27);
L ieu0ueu11 = f68/3;
L ieum1ueu11 = 0;
L ieum2ueu11 = 0;
L ieu2uou11 = 2*f74*fvu4u28 + 2*f74*fvu4u39 + 
    (2*f74*fvu4u51)/3 + 2*f74*fvu4u80 + 2*f74*fvu4u83 + 
    2*f74*fvu4u91 + 2*f74*fvu4u93 - (2*f74*fvu4u100)/3 + 
    (2*f74*fvu4u102)/3 - 2*f74*fvu4u139 + (2*f74*fvu4u148)/3 + 
    (3*f74*fvu4u171)/4 - (11*f74*fvu4u174)/4 - (11*f74*fvu4u182)/4 + 
    (3*f74*fvu4u184)/4 - (7*f74*fvu4u192)/6 + (f74*fvu4u199)/4 + 
    (f74*fvu4u201)/2 + f74*fvu4u202 + 4*f74*fvu4u213 - 
    2*f74*fvu4u215 + 2*f74*fvu4u221 - (2*f74*fvu4u225)/3 + 
    f74*fvu4u244 - (3*f74*fvu4u252)/2 + (f74*fvu4u255)/6 - 
    (11*f74*fvu4u271)/4 - (9*f74*fvu4u273)/4 + (f74*fvu4u277)/4 - 
    (3*f74*fvu4u279)/4 + (2*f74*fvu4u282)/3 - (f74*fvu4u289)/4 - 
    (f74*fvu4u290)/2 - f74*fvu4u291 - 2*f74*fvu4u295 - 
    2*f74*fvu4u325 + 2*f74*fvu4u330 + 
    fvu3u71*(-(f74*fvu1u1u3) + f74*fvu1u1u6 - f74*fvu1u1u7) + 
    fvu3u25*(-(f74*fvu1u1u1) + f74*fvu1u1u3 - f74*fvu1u1u6 + 
      f74*fvu1u1u7) + fvu3u81*(-(f74*fvu1u1u2) - f74*fvu1u1u5 - 
      f74*fvu1u1u8) + fvu3u45*(f74*fvu1u1u2 + f74*fvu1u1u3 - 
      f74*fvu1u1u9) + fvu3u43*(f74*fvu1u1u2 + f74*fvu1u1u6 - 
      f74*fvu1u1u7 - f74*fvu1u1u9) + 
    fvu3u82*(f74*fvu1u1u5 + f74*fvu1u1u6 - f74*fvu1u1u7 + 
      f74*fvu1u1u8 - f74*fvu1u1u9) + 
    fvu3u63*(f74*fvu1u1u6 + f74*fvu1u1u7 + f74*fvu1u1u9 - 
      2*f74*fvu1u1u10) + (151*f74*tci11^3*tci12)/135 + 
    fvu3u80*(-(f74*fvu1u1u3) + f74*fvu1u1u5 - (f74*fvu1u1u6)/2 + 
      (f74*fvu1u1u7)/2 - (f74*fvu1u1u8)/2 + (3*f74*fvu1u1u9)/2 + 
      (3*f74*tci12)/2) + fvu3u78*(f74*fvu1u1u2 - 
      2*f74*fvu1u1u3 + f74*fvu1u1u5 - f74*fvu1u1u8 + 
      2*f74*tci12) + (2*f74*tci11^2*tci21)/9 + 
    fvu1u1u8*((5*f74*tci11^3)/27 - 5*f74*tci12*tci21) + 
    fvu1u1u3*((4*f74*tci11^3)/27 - 4*f74*tci12*tci21) + 
    fvu1u1u5*((4*f74*tci11^3)/27 - 4*f74*tci12*tci21) + 
    fvu1u1u1*(-(f74*fvu3u45) + f74*fvu3u71 + f74*fvu3u78 + 
      f74*fvu3u81 - f74*fvu3u82 - (8*f74*tci11^3)/27 + 
      8*f74*tci12*tci21) + tci12*((48*f74*tci31)/5 + 
      16*f74*tci32) + (4*f74*tci41)/3 + 
    ((4*f74*tci11^3)/9 - 8*f74*tci12*tci21)*tcr11 - 
    (f74*tci11*tci12*tcr11^2)/5 + 
    fvu1u1u7*((7*f74*tci11^3)/90 - (f74*tci12*tci21)/3 + 
      (24*f74*tci31)/5 + 2*f74*tci21*tcr11 - 
      (f74*tci11*tcr11^2)/10) + 
    fvu1u1u9*((7*f74*tci11^3)/90 - (f74*tci12*tci21)/3 + 
      (24*f74*tci31)/5 + 2*f74*tci21*tcr11 - 
      (f74*tci11*tcr11^2)/10) + 
    fvu1u1u6*((f74*tci11^3)/270 + (5*f74*tci12*tci21)/3 + 
      (24*f74*tci31)/5 + 2*f74*tci21*tcr11 - 
      (f74*tci11*tcr11^2)/10) + 
    fvu1u1u2*(-(f74*fvu3u63) - f74*fvu3u80 - 
      (31*f74*tci11^3)/270 + (4*f74*tci12*tci21)/3 - 
      (24*f74*tci31)/5 - 2*f74*tci21*tcr11 + 
      (f74*tci11*tcr11^2)/10) + 
    fvu1u1u10*((-11*f74*tci11^3)/135 - (4*f74*tci12*tci21)/
       3 - (48*f74*tci31)/5 - 4*f74*tci21*tcr11 + 
      (f74*tci11*tcr11^2)/5) - 
    (40*f74*tci12*tci21*tcr12)/3 - 4*f74*tci11*tci12*
     tcr12^2;
L ieu1uou11 = -2*f74*fvu3u81 + 
    (4*f74*tci11^3)/27 - 4*f74*tci12*tci21;
L ieu0uou11 = 0;
L ieum1uou11 = 0;
L ieum2uou11 = 0;

.sort
Format O4;
Format C;
L K=+w^1*ieu2uou11+w^2*ieu1uou11+w^3*ieu0uou11+w^4*ieum1uou11+w^5*ieum2uou11;
B w;
.sort
#optimize K
B w;
.sort
L ieu2uou11a = K[w^1];
L ieu1uou11a = K[w^2];
L ieu0uou11a = K[w^3];
L ieum1uou11a = K[w^4];
L ieum2uou11a = K[w^5];
.sort
#write <e11.tmp> "`optimmaxvar_'"
#write <e11_odd.c> "%O"
#write <e11_odd.c> "return Eps5o2<T>("
#write <e11_odd.c> "%E", ieu2uou11a
#write <e11_odd.c> ", "
#write <e11_odd.c> "%E", ieu1uou11a
#write <e11_odd.c> ", "
#write <e11_odd.c> "%E", ieu0uou11a
#write <e11_odd.c> ", "
#write <e11_odd.c> "%E", ieum1uou11a
#write <e11_odd.c> ", "
#write <e11_odd.c> "%E", ieum2uou11a
#write <e11_odd.c> ");\n}"
L H=+u^1*ieu2ueu11+u^2*ieu1ueu11+u^3*ieu0ueu11+u^4*ieum1ueu11+u^5*ieum2ueu11;
B u;
.sort
#optimize H
B u;
.sort
L ieu2ueu11a = H[u^1];
L ieu1ueu11a = H[u^2];
L ieu0ueu11a = H[u^3];
L ieum1ueu11a = H[u^4];
L ieum2ueu11a = H[u^5];
.sort
#write <e11.tmp> "`optimmaxvar_'"
#write <e11_even.c> "%O"
#write <e11_even.c> "return Eps5o2<T>("
#write <e11_even.c> "%E", ieu2ueu11a
#write <e11_even.c> ", "
#write <e11_even.c> "%E", ieu1ueu11a
#write <e11_even.c> ", "
#write <e11_even.c> "%E", ieu0ueu11a
#write <e11_even.c> ", "
#write <e11_even.c> "%E", ieum1ueu11a
#write <e11_even.c> ", "
#write <e11_even.c> "%E", ieum2ueu11a
#write <e11_even.c> ");\n}"
.end
