#!/usr/bin/env bash

IN=Fs.m
OUT=Fs.c

perl \
    -0777p \
    -e  "
    s|\{?F\[(\d+), (\d+)\][,\s}]*|PentagonFunctions::FunctionObjectType<T> F_\1_\2 { PentagonFunctions::FunctionID(\1, \2).get_evaluator<T>() };\n|g;
    s|\{?F\[(\d+), (\d+), (\d+)\][,\s}]*|PentagonFunctions::FunctionObjectType<T> F_\1_\2_\3 { PentagonFunctions::FunctionID(\1, \2, \3).get_evaluator<T>() };\n|g;
        " \
    ${IN} > ${OUT}

echo >> ${OUT}

perl \
    -0777p \
    -e  "
    s|\{?F\[(\d+), (\d+)\][,\s}]*|TreeValue fv_\1_\2;\n|g;
    s|\{?F\[(\d+), (\d+), (\d+)\][,\s}]*|TreeValue fv_\1_\2_\3;\n|g;
        " \
    ${IN} >> ${OUT}

echo >> ${OUT}

perl \
    -0777p \
    -e  "
    s|\{?F\[(\d+), (\d+)\][,\s}]*|fv_\1_\2 = F_\1_\2(k);\n|g;
    s|\{?F\[(\d+), (\d+), (\d+)\][,\s}]*|fv_\1_\2_\3 = F_\1_\2_\3(k);\n|g;
        " \
    ${IN} >> ${OUT}

echo >> ${OUT}
