#!/usr/bin/env bash

OUT=new_output
mkdir -p ${OUT}

SRC2L=part_2l.cpp
SRC1Lsq=part_1lsq.cpp

root=`git root`/5g@2l/1lsq-impl
root_2l="`git root`/5g@2l/2l-impl-final"

l=2
dir="${root_2l}/exprs"
for f in $dir/P${l}_*.m; do
    # echo $f
    g=${f#$dir/P${l}_}
    h=${g%_lr*.m}
    d=${h}
    df=2l_hel_${d}
    mkdir -p ${df}

    h_symb=""
    h_char=""
    j=0
    for i in {0..4}; do
        if [ "${h:$i:1}" = "+" ]; then
            j=$(($j + 2 ** $i))
            h_symb+="+"
            h_char+="p"
        else
            h_symb+="-"
            h_char+="m"
        fi
    done

    if (( $j < 10 )); then
        j="0$j"
    fi

    echo $h $h_symb $h_char $j

    cp ${df}/coeffs.cpp ${OUT}/0q5g-2l-${h_char}-coeffs-analytic.cpp

    for i in {1..12}; do
        cp ${df}/eps_p${i}.cpp ${OUT}/0q5g-2l-${h_char}-p${i}-analytic.cpp
    done

    SRC=${df}/${SRC2L}
    F=`cat ${root_2l}/${d}/fs.fcount`
    echo $((F++))
    Z=`cat ${root_2l}/${d}/*.tmp | sort -n | tail -1`
    echo $((Z++))
    perl -p -e "s|CCCCC|$h_char|g;" template_start.cpp >${SRC}
    perl -p -e "s|CCCCC|$h_char|g; s|FFF|$F|g; s|ZZZ|$Z|g;" template_source.cpp >>${SRC}
    perl -p -e "s|CCCCC|$h_char|g;" template_end.cpp >>${SRC}
done

INIT=init.cpp
echo "" >${INIT}

l=1
dir=${root}/new_exprs
for f in $dir/P${l}_*.m; do
    g=${f#$dir/P${l}_epsexp_}
    h=${g%_lr*.m}
    d=hel_${h}
    df=1lsq_${d}
    mkdir -p ${df}

    h_symb=""
    h_char=""
    j=0
    for i in {0..4}; do
        if [ "${h:$i:1}" = "+" ]; then
            j=$(($j + 2 ** $i))
            h_symb+="+"
            h_char+="p"
        else
            h_symb+="-"
            h_char+="m"
        fi
    done

    if (( $j < 10 )); then
        j="0$j"
    fi

    echo $h $h_symb $h_char $j

    cp ${df}/coeffs.cpp ${OUT}/0q5g-1lsq-${h_char}-coeffs-analytic.cpp

    cp ${df}/eps.cpp ${OUT}/0q5g-1lsq-${h_char}-analytic.cpp

    SRC=${df}/${SRC1Lsq}
    F=`cat ${root}/${d}/fs.fcount`
    echo $((F++))
    Z=`cat ${root}/${d}/*.tmp | sort -n | tail -1`
    echo $((Z++))
    perl -p -e "s|CCCCC|$h_char|g;" template_start.cpp >${SRC}
    perl -p -e "s|CCCCC|$h_char|g; s|FFF|$F|g; s|ZZZ|$Z|g;" template_source.cpp >>${SRC}
    perl -p -e "s|CCCCC|$h_char|g;" template_end.cpp >>${SRC}

    echo "#include \"0q5g-2v-${h_char}-analytic.h\"" >>${INIT}
done

echo "" >>${INIT}

for f in 1lsq_hel_*/part_1lsq.cpp; do
    g=${f#1lsq_hel_}
    h=${g%/part_1lsq.cpp}
    h_symb=""
    h_char=""
    j=0
    for i in {0..4}; do
        if [ "${h:$i:1}" = "+" ]; then
            j=$(($j + 2 ** $i))
            h_symb+="+"
            h_char+="p"
        else
            h_symb+="-"
            h_char+="m"
        fi
    done

    if (( $j < 10 )); then
        j="0$j"
    fi

    if [ -f 2l_hel_${h}/part_2l.cpp ]; then
        FILE=2l_hel_${h}/part_2l.cpp
    else
        FILE=$f
    fi

    cp ${FILE} ${OUT}/0q5g-2v-${h_char}-analytic.cpp

    echo "Amp0q5g_a2v_${h_char}<T> hamp2v${j};" >>${INIT}
done

echo "" >>${INIT}

for f in 1lsq_hel_*/header.h; do
    g=${f#1lsq_hel_}
    h=${g%/header.h}
    h_symb=""
    h_char=""
    j=0
    for i in {0..4}; do
        if [ "${h:$i:1}" = "+" ]; then
            j=$(($j + 2 ** $i))
            h_symb+="+"
            h_char+="p"
        else
            h_symb+="-"
            h_char+="m"
        fi
    done

    if (( $j < 10 )); then
        j="0$j"
    fi

    cp ${f} ${OUT}/0q5g-2v-${h_char}-analytic.h

    echo "hamp2v${j} = Amp0q5g_a2v_${h_char}<T>(this);" >>${INIT}
done

FLAGS=flags.txt
rm -f ${FLAGS}
touch ${FLAGS}
PART=Makefile.part
rm -f ${PART}
touch ${PART}
# for f in ${OUT}/*.cpp; do
#     echo "${f#*/} \\">>${PART}
# done
# echo "" >>${PART}
# for f in ${OUT}/*.h; do
#     echo "${f#*/} \\">>${PART}
# done
PART2=Makefile.2.part
rm -f ${PART2}
touch ${PART2}

for f in 1lsq_hel_*/header.h; do
    g=${f#1lsq_hel_}
    h=${g%/header.h}
    h_symb=""
    h_char=""
    j=0
    for i in {0..4}; do
        if [ "${h:$i:1}" = "+" ]; then
            j=$(($j + 2 ** $i))
            h_symb+="+"
            h_char+="p"
        else
            h_symb+="-"
            h_char+="m"
        fi
    done

    if (( $j < 10 )); then
        j="0$j"
    fi

    perl -p -e "s|CCCCC|$h_char|g;" template_Makefile.am >>${PART}
    for f in ${OUT}/*${h_char}*.cpp; do
        echo "${f#*/} \\">>${PART}
    done
    perl -p -e "s|CCCCC|$h_char|g;" template_Makefile2.am >>${PART}

    prec=( "SD" "VC" "DD" "QD" )   
    for p in ${prec[@]}; do
        echo -n "libnjet2an0q5g2v${h_char}${p}.la " >>${PART2}
    done

    echo -n " -lnjet2an0q5g2v${h_char}" >>${FLAGS}
done

echo >>${FLAGS}
perl -pi -e "s|(0q5g\-2v\-[pm]{5}\-analytic\.cpp) \\\|\1|g;" ${PART}
