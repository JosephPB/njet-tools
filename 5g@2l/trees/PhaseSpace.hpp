#include <algorithm>
#include <array>
#include <numeric>

#include "ngluon2/Mom.h"

template <typename T>
const T zero { 1e-10 };

template <typename T, std::size_t mul>
void test_massless(const std::array<MOM<T>, mul>& momenta)
{
    std::for_each(momenta.cbegin(), momenta.cend(), [](const MOM<T>& m) { assert(abs(m.mass()) < zero<T>); });
}

template <typename T, std::size_t mul>
void test_mom_cons(const std::array<MOM<T>, mul>& momenta)
{
    MOM<T> sum { std::accumulate(momenta.cbegin(), momenta.cend(), MOM<T>()) };
    assert(sum.x0 < zero<T>);
    assert(sum.x1 < zero<T>);
    assert(sum.x2 < zero<T>);
    assert(sum.x3 < zero<T>);
}

template <typename T>
std::array<MOM<T>, 5> phase_space_point(const T y1, const T y2, const T theta, const T alpha, const T sqrtS = T(1.), int r = 0)
{
    assert(T(0.) <= y1);
    assert(T(0.) <= y2);
    assert(y1 + y2 <= T(1.));
    assert(r >= 0);

    r %= 5;

    const T x1 { T(1.) - y1 };
    const T x2 { T(1.) - y2 };

    // cos(beta)
    const T cb { T(1.) + T(2.) * (T(1.) - x1 - x2) / x1 / x2 };
    assert((T(-1.) <= cb) && (cb <= T(1.)));
    // sin(beta)
    const T sb { sqrt(T(1.) - pow(cb, 2)) };

    const T ct { cos(theta) };
    const T st { sin(theta) };

    const T ca { cos(alpha) };
    const T sa { sin(alpha) };

    std::array<MOM<T>, 5> momenta { {
        sqrtS / T(2.) * MOM<T>(T(-1.), T(0.), T(0.), T(-1.)),
        sqrtS / T(2.) * MOM<T>(T(-1.), T(0.), T(0.), T(1.)),
        x1 * sqrtS / T(2.) * MOM<T>(T(1.), st, T(0.), ct),
        x2 * sqrtS / T(2.) * MOM<T>(T(1.), ca * ct * sb + cb * st, sa * sb, cb * ct - ca * sb * st),
        MOM<T>(),
    } };

    momenta[4] = -std::accumulate(momenta.cbegin(), momenta.cend() - 1, MOM<T>());

    std::rotate(momenta.begin(), momenta.begin() + r, momenta.end());

    test_massless(momenta);
    test_mom_cons(momenta);

    return momenta;
}
