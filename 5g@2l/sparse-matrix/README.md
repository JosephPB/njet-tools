# sparse-matrix
Fork of [new-format](../new-format)
## NJet compile (IPPP workstation)
Using `eigen` for sparse matrix.
```shell
CPPFLAGS=`pkg-config PentagonFunctions eigen3 --cflags` LDFLAGS=`pkg-config PentagonFunctions --libs` CXXFLAGS='-std=c++17 -Wall -Wextra -O0' $SCRATCH/njet-develop/configure --prefix=$NJET_LOCAL --enable-oneloop1 --enable-an0q5g2l
```
Option: ` --with-qd=no`
## Usage
```shell
math -script mmppp/proc-1l-new-mmppp.wl
./loops.sh
./init.sh
```
