(* ::Package:: *)

SetDirectory["/home/ryan/git/njet-tools/5g@2l/2l-impl/mmppp"];


data=Get["../P2_mm+++_lr_apart.m"];


epsQuin[o_]:=Module[{one},
one=(((Plus@@(data[[1,o,#]]/eps^(5-#)&/@Range[5])))//Series[#,{eps,0,0}]&)//Normal;
Coefficient[one,eps,#]&/@Range[0,-4,-1]
]


res=epsQuin/@Range[12];


all=Join[data[[2;;3]]//Reverse,{res}];


Export["all.m",all//.{Power[a_,b_]->pow[a,b]}];


correction=((1+eps^2*Pi^2/12)*(MuR2)^eps)^2;
genEps5=Plus@@(a[#]/eps^#&/@Range[0,4]);
genEps5*correction//Series[#,{eps,0,0}]&//Simplify
(%//Normal)//.{Log[MuR2]->LR,Power[Log[MuR2],n_]->Power[LR,n],Pi^2->PI2,a[i_]->ep5[i]};
Coefficient[%,eps,#]&/@Range[0,-4,-1]//.{Power[a_,b_]->pow[a,b]};
Export["correction.m",%];


Flatten[data[[1]]//.{Plus->List,Times->List}];
Select[%,MatchQ[#,_F]&]//DeleteDuplicates;
%//Sort;
Export["pentas.m",%];


epsQuin2[o_]:=Module[{one},
one=(((Plus@@(data[[1,o,#]]/eps^(5-#)&/@Range[5])))//Series[#,{eps,0,0}]&)//Normal;
inveps[Abs[#],o]->Coefficient[one,eps,#]&/@Range[0,-4,-1]
]
Export["eps.m",epsQuin2/@Range[12]];


Export["fs.m",Simplify/@data[[2]]];


Export["ys.m",Simplify/@data[[3]]];
