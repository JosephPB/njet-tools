#!/usr/bin/env bash

for f in exprs/P2_*_lr_bcfw_*_*.m; do
    g=${f#exprs/P2_}
    h=${g%_lr_bcfw_*_*.m}
    cd $h
    tform ys.frm >form.ys.log 2>&1 &
    #tform fs.frm >form.fs.log 2>&1 &
    #for i in {1..12}; do
    #    tform e${i}.frm >form.${i}.log 2>&1 &
    #done
    cd ..
done
