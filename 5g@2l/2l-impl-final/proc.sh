#!/usr/bin/env bash

l=(13 22 26 11 19 14 21 25 28 07)
k=0
for f in exprs/P2_*_lr_bcfw_*_*.m; do
    g=${f#exprs/P2_}
    h=${g%_lr_bcfw_*_*.m}

    cd $h

    j=${l[$k]}
    echo $j
    echo $((k++))
    INT_R=int_coeffs.cpp
    COEFFS=coeffs.cpp
    perl -p -e "s|HH|$j|g" ../template_coeffs_start.cpp >${COEFFS}
    cat ys.cpp >>${COEFFS}
    cat fs.cpp >>${COEFFS}
    cat ../template_coeffs_end.cpp >>${COEFFS}
    cp -f ${COEFFS} ${INT_R}

    for i in {1..12}; do
        FILE=eps_p${i}.cpp
        perl -p -e "s|HH|$j|g; s|PP|$i|g;" ../template_perm_start.cpp >${FILE}
        cat e${i}_odd.cpp >>${FILE}
        perl -p -e "s|HH|$j|g; s|PP|$i|g;" ../template_perm_middle.cpp >>${FILE}
        cat e${i}_even.cpp >>${FILE}
        cat ../template_perm_end.cpp >>${FILE}
        # clang-format -i ${FILE}
    done

    cd ..
done
