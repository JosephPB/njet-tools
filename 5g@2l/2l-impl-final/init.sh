#!/usr/bin/env bash

mkdir output

for f in exprs/P2_*_lr_bcfw_*_*.m; do
    g=${f#exprs/P2_}
    h=${g%_lr_bcfw_*_*.m}
    mkdir $h
    cp $f $h/P2_$h.m
    m=proc-2l-$h.wl
    perl -p -e "s|HHHHH|$h|g;" proc-2l-HHHHH.wl >$h/$m
    cd $h
    math -script $m
    cd ..
done
