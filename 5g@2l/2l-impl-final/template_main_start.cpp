#include "0q5g-analytic.h"

// includes (MuR)^(2*eps) correction, but not (cGamma)^2
template <typename T>
typename Amp0q5g_a<T>::LoopValue<Eps5<T>>
Amp0q5g_a<T>::correction2L(const LoopValue<Eps5<T>> &ep5) {
  const T LR{log(MuR2())};
  return LoopValue<Eps5<T>>(
      ep5.get0() + T(2.) * ep5.get1() * LR + T(2.) * ep5.get2() * njet_pow(LR, 2) + (T(4.) * ep5.get3() * njet_pow(LR, 3)) / T(3.) + (T(2.) * ep5.get4() * njet_pow(LR, 4)) / T(3.), 
      ep5.get1() + T(2.) * ep5.get2() * LR + T(2.) * ep5.get3() * njet_pow(LR, 2) + (T(4.) * ep5.get4() * njet_pow(LR, 3)) / T(3.), 
      ep5.get2() + T(2.) * ep5.get3() * LR + T(2.) * ep5.get4() * njet_pow(LR, 2),
      ep5.get3() + T(2.) * ep5.get4() * LR, 
      ep5.get4());
}

