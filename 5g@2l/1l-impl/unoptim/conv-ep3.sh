#!/usr/bin/env bash

OUT=$2

I="
template <typename T>
LoopResult<T> Amp0q5g_a<T>::hAgPh28oXX()
{
"
F="
const LoopResult<T> res { amp, conj(amp) };
return res;
}
"

perl \
    -0777p \
    -e  "
        s|\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)||g;
        s|ex\[(\d)\]|x\1|g;
        s|f\[(\d+), \"?([\ddsmp]+)\"?\]|g_\1_\2|g;
        s|\{\{|${I}const LoopValue amp(|g;
        s|\},[\s\n]*\{|);${F}${I}const LoopValue amp(|g;
        s|\}\}|);${F}|g;
        s|F\[(\d+), (\d+)\]|fv_\1_\2|g;
        s|F\[(\d+), (\d+), (\d+)\]|fv_\1_\2_\3|g;
        s|tc(\w)\[(\d), (\d)\]|tc\1\2\3|g;
        s|([^ *+-/()]+)\^(\d+)|njet_pow(\1, \2)|g;
        s|(\(.*?)\)\^(\d+)|njet_pow\1, \2)|g;
        s|Log\[MuR2\]|log(MuR2())|g;
        s|([ (-])(\d+)\*|\1T(\2.)*|g;
        s|([/])(\d+)|\1T(\2.)|g;
        s|([ (-])(\d+) ([+-])|\1T(\2.) \3|g;
        s|Pi|pi()|g;
        " \
    $1 > ${OUT}

for i in {00..11}; do perl -0777pi -e "s|XX|$i|;" ${OUT}; done
