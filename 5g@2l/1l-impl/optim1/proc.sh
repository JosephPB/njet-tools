#!/usr/bin/env bash

../conv-form-y.sh ys.m && tform ys.frm && ../pp-form.sh ys.c
../conv-form-f.sh fs.m && tform fs.frm && ../pp-form.sh fs.c
../conv-form-eps.sh eps.m && tform eps.frm && ../pp-form.sh eps.c

OUT=end.cpp

cat ../template_start.cpp > ${OUT}

i=`cat *.tmp | sort | tail -1`
echo "std::array<TreeValue, ${i}> Z;" >>${OUT}

cat ys.cpp >> ${OUT}
cat fs.cpp >> ${OUT}
cat eps.cpp >> ${OUT}

cat ../template_end.cpp >> ${OUT}

for n in {1..1000}; do
    m=$((n - 1))
    perl -pi -e "s|\[$n\]|[$m]|g" ${OUT}
done

clang-format -i ${OUT}
