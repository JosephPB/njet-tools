(* ::Package:: *)

SetDirectory["/home/ryan/git/njet-tools/5g@2l/1l-impl/optim1"];


data=Get["new/P1_m++m+_lr_apart.m"];


Dimensions[data]
Dimensions[data[[1]]]
Dimensions[data[[2]]]
Dimensions[data[[3]]]


epsTrip[o_]:=Module[{one},
one=(((Plus@@(data[[1,o,#]]/eps^(3-#)&/@Range[3])))//Series[#,{eps,0,0}]&)//Normal;
inveps[Abs[#],o]->Coefficient[one,eps,#]&/@Range[0,-2,-1]
]


res=epsTrip/@Range[12];


Export["eps.m",res];
Export["fs.m",data[[2]]];
Export["ys.m",data[[3]]];


res[[1,3]]


data[[2,1]]


data[[3,69]]
