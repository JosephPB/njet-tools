#!/usr/bin/env bash

OUT=$2

I="
#include \"0q5g-analytic.h\"

template <typename T>
std::array<typename Amp0q5g_a<T>::LoopValue, 12> Amp0q5g_a<T>::hAgP28()
{
    const std::array<int, 5> p {0,1,2,3,4};
    setxi_2l(p.data());
    setpenta(p.data());
"
F="
for (LoopValue amp : amps) {
    amp = correction(amp);
}
return amps;
}

#ifdef USE_SD
template class Amp0q5g_a<double>;
#endif
#ifdef USE_DD
template class Amp0q5g_a<dd_real>;
#endif
#ifdef USE_QD
template class Amp0q5g_a<qd_real>;
#endif
#ifdef USE_VC
template class Amp0q5g_a<Vc::double_v>;
#endif
"

rm -f ${OUT}

perl \
    -0777p \
    -e  "
        s|\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)||g;
        s|,[\s]+([yf])\[(\d+?)\] ->|};\nconst TreeValue \1\2 {|g;
        s|\{\n?([yf])\[(\d+?)\] ->|{\n// {\nconst TreeValue \1\2 {|g;
        s|\},[\s\n]*\{[\s\n]*(\/\/ \{)|};\n// }\n\1|g;
        s|\},[\s\n]*\{\{|};\n// }\nstd::array<LoopValue, 12> amps {\nLoopValue(|g;
        s|\},[\s\n]*\{|),\nLoopValue(|g;
        s|ex\[(\d)\]|x\1|g;
        s|([yf])\[(\d+?)\]|\1\2|g;
        s|F\[(\d+), (\d+)\]|fv_\1_\2|g;
        s|F\[(\d+), (\d+), (\d+)\]|fv_\1_\2_\3|g;
        s|tc(\w)\[(\d), (\d)\]|tc\1\2\3|g;
        s|pow\[Pi, 2\]|PI2|g;
        s|pow\[(.*?),[\s\n]+([\-\d]+)\]|njet_pow(\1, \2)|gs;
        s|pow\[(.*),[\s\n]+([\-\d]+)\]|njet_pow(\1, \2)|g;
        s|([ (\-={\/] ?)(-?\d+)\.?( ?[*\/;+\-\n},)])|\1T(\2.)\3|g;
        s|([ (\-={\/] ?)(-?\d+)\.?( ?[*\/;+\-\n},])|\1T(\2.)\3|g;
        s|\}\}\}|)\n};${F}|g;
        s|\{\{|${I}|g;
        " \
    $1 > ${OUT}
