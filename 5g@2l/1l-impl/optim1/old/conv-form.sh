#!/usr/bin/env bash

# Use on `*.m` file

IN=$1
BASE=${IN%.m}
FRM=${BASE}.frm
C=${BASE}.c
Z=${BASE}.tmp
TMP=tmp

echo -e "#-\nS u,v,w,x1,x2,x3,x4,x5;" >$FRM

perl \
    -0777p \
    -e "
        s|\(\* Created with the Wolfram Language for Students - Personal Use Only : www\.wolfram\.com \*\)||g;
        s|([yf])\[(\d+?)\]|\1\2|g;
        s|F\[(\d+), (\d+)\]|fvu\1u\2|g;
        s|F\[(\d+), (\d+), (\d+)\]|fvu\1u\2u\3|g;
        s|tc(\w)\[(\d), (\d)\]|tc\1\2\3|g;
        s|\{{0,2}inveps\[(\d), (\d+)\]|ieu\1u\2|g;
        s|ex\[(\d)\]|x\1|g;
        s|\{{0,2}([yf]\d+ )->( .*?)\}?,[\s\n]*|L \1=\2;\n|gs;
        s|\{{0,2}(ieu\du\d+ )->( .*?)\}?,[\s\n]*|L \1=\2;\n|gs;
        s|\{{0,2}(ieu\du\d+ )->( .*?)\}{3}[\s\n]*|L \1=\2;\n|gs;
        s|,[ \n]*(L gs)|;\n\1|g;
        " \
    ${IN} >${TMP}

constsr=$(rg -o "tc[ir]\d+" $TMP)
declare -A consts
for c in $constsr; do
    consts[$c]=1
done
for c in ${!consts[@]}; do
    echo -e "S $c;" >>$FRM
done

pentasr=$(rg -o "fv[u\d]*" $TMP)
declare -A pentas
for c in $pentasr; do
    pentas[$c]=1
done
for c in ${!pentas[@]}; do
    echo -e "S $c;" >>$FRM
done

echo "ExtraSymbols,array,Z;" >>$FRM
cat ${TMP} >>${FRM}
rm -f ${TMP}

echo -e "\n.sort\nFormat O4;\nFormat C;" >>$FRM

# ys
h="L H="
i=1
ysr=$(rg -o "y\d+" $FRM)
declare -A ys
for y in $ysr; do
    ys[$y]=1
done

for y in ${!ys[@]}; do
    h+="+u^$((i++))*$y"
done

echo "$h;" >>$FRM

echo -e "B u;\n.sort\n#optimize H\nB u;\n.sort" >>$FRM

i=1
for y in ${!ys[@]}; do
    echo "L ${y}a = H[u^$((i++))];" >>$FRM
done

echo -e ".sort" >>$FRM

echo "#write <$Z> \"std::array<TreeValue, \`optimmaxvar_'> Z;\"" >>$FRM
echo "#write <$C> \"%O\"" >>$FRM

for y in ${!ys[@]}; do
    echo "#write <$C> \"const TreeValue $y{%E};\n\", ${y}a" >>$FRM
done

# fs
J="L J="
i=1
fsr=$(rg -o "f\d+" $FRM)
declare -A fs
for f in $fsr; do
    fs[$f]=1
done
for f in ${!fs[@]}; do
    J+="+v^$((i++))*$f"
done

echo "$J;" >>$FRM

echo -e "B v;\n.sort\n#optimize J\nB v;\n.sort" >>$FRM

i=1
for f in ${!fs[@]}; do
    echo "L ${f}a = J[v^$((i++))];" >>$FRM
done

echo -e ".sort" >>$FRM

echo "#write <$Z> \"std::array<TreeValue, \`optimmaxvar_'> Z;\"" >>$FRM
echo "#write <$C> \"%O\"" >>$FRM

for f in ${!fs[@]}; do
    echo "#write <$C> \"const TreeValue $f{%E};\n\", ${f}a" >>$FRM
done

# es
K="L K="
i=1
esr=$(rg -o "ie[^ ]+" $FRM)
declare -A es
for e in $esr; do
    es[$e]=1
done
for e in ${!es[@]}; do
    K+="+w^$((i++))*$e"
done

echo "$K;" >>$FRM

echo -e "B w;\n.sort\n#optimize K\nB w;\n.sort" >>$FRM

i=1
for e in ${!es[@]}; do
    echo "L ${e}a = K[w^$((i++))];" >>$FRM
done

echo -e ".sort" >>$FRM

echo "#write <$Z> \"std::array<TreeValue, \`optimmaxvar_'> Z;\"" >>$FRM
echo "#write <$C> \"%O\"" >>$FRM

for e in ${!es[@]}; do
    echo "#write <$C> \"const TreeValue $e{%E};\n\", ${e}a" >>$FRM
done

echo ".end" >>$FRM
