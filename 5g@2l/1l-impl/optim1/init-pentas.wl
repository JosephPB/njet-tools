(* ::Package:: *)

SetDirectory["/home/ryan/git/njet-tools/5g@2l/1l-impl/optim1"];


d14=Get["P1_m+++m_lr_apart.m"];
d22=Get["P1_m++m+_lr_apart.m"];
d26=Get["P1_m+m++_lr_apart.m"];
d28=Get["P1_mm+++_lr_apart.m"];
d07=Get["P1_+++mm_lr_apart.m"];
d11=Get["P1_++m+m_lr_apart.m"];
d19=Get["P1_++mm+_lr_apart.m"];
d13=Get["P1_+m++m_lr_apart.m"];
d21=Get["P1_+m+m+_lr_apart.m"];
d25=Get["P1_+mm++_lr_apart.m"];


data=(#[[1]]&/@{d28,d25,d19,d07,d26,d21,d11,d22,d13,d14})//.{Plus->List,Times->List,Power->List}//Flatten;


Fs=Select[Variables[data],MatchQ[#,_F]&]


Export["pentas.m",Fs];
