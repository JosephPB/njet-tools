#-
S x1,x2,x3,x4,x5;
ExtraSymbols,array,Z;

L a28 = x1^3*x2^2*x3;
L a25 = (x2^2*x3)/x1;
L a19 = x3/(x1*x2^2);
L a07 = 1/(x1*x2^2*x3^3);
L a26 = x1^3*x2^2*x3;
L a21 = ((1 + x2)^4*x3)/(x1*x2^2);
L a11 = (1 + x3)^4/(x1*x2^2*x3^3);
L a22 = x1^3*x2^2*x3;
L a13 = (1 + x3 + x2*x3)^4/(x1*x2^2*x3^3);
L a14 = x1^3*x2^2*x3;

.sort
Format O4;
Format C;

#optimize a28
#write <trees.c> "\ntemplate <typename T>\nTreeValue Amp0q5g_a<T>::hA028X()\n{"
#write <trees.c> "const std::array<TreeValue, `optimmaxvar_'> Z;"
#write <trees.c> "%O"
#write <trees.c> "return a28{%E};", a28
#write <trees.c> "}"

#optimize a25
#write <trees.c> "\ntemplate <typename T>\nTreeValue Amp0q5g_a<T>::hA025X()\n{"
#write <trees.c> "const std::array<TreeValue, `optimmaxvar_'> Z;"
#write <trees.c> "%O"
#write <trees.c> "return a25{%E};", a25
#write <trees.c> "}"

#optimize a19
#write <trees.c> "\ntemplate <typename T>\nTreeValue Amp0q5g_a<T>::hA019X()\n{"
#write <trees.c> "const std::array<TreeValue, `optimmaxvar_'> Z;"
#write <trees.c> "%O"
#write <trees.c> "return a19{%E};", a19
#write <trees.c> "}"

#optimize a07
#write <trees.c> "\ntemplate <typename T>\nTreeValue Amp0q5g_a<T>::hA007X()\n{"
#write <trees.c> "const std::array<TreeValue, `optimmaxvar_'> Z;"
#write <trees.c> "%O"
#write <trees.c> "return a07{%E};", a07
#write <trees.c> "}"

#optimize a26
#write <trees.c> "\ntemplate <typename T>\nTreeValue Amp0q5g_a<T>::hA026X()\n{"
#write <trees.c> "const std::array<TreeValue, `optimmaxvar_'> Z;"
#write <trees.c> "%O"
#write <trees.c> "return a26{%E};", a26
#write <trees.c> "}"

#optimize a21
#write <trees.c> "\ntemplate <typename T>\nTreeValue Amp0q5g_a<T>::hA021X()\n{"
#write <trees.c> "const std::array<TreeValue, `optimmaxvar_'> Z;"
#write <trees.c> "%O"
#write <trees.c> "return a21{%E};", a21
#write <trees.c> "}"

#optimize a11
#write <trees.c> "\ntemplate <typename T>\nTreeValue Amp0q5g_a<T>::hA011X()\n{"
#write <trees.c> "const std::array<TreeValue, `optimmaxvar_'> Z;"
#write <trees.c> "%O"
#write <trees.c> "return a11{%E};", a11
#write <trees.c> "}"

#optimize a22
#write <trees.c> "\ntemplate <typename T>\nTreeValue Amp0q5g_a<T>::hA022X()\n{"
#write <trees.c> "const std::array<TreeValue, `optimmaxvar_'> Z;"
#write <trees.c> "%O"
#write <trees.c> "return a22{%E};", a22
#write <trees.c> "}"

#optimize a13
#write <trees.c> "\ntemplate <typename T>\nTreeValue Amp0q5g_a<T>::hA013X()\n{"
#write <trees.c> "const std::array<TreeValue, `optimmaxvar_'> Z;"
#write <trees.c> "%O"
#write <trees.c> "return a13{%E};", a13
#write <trees.c> "}"

#optimize a14
#write <trees.c> "\ntemplate <typename T>\nTreeValue Amp0q5g_a<T>::hA014X()\n{"
#write <trees.c> "const std::array<TreeValue, `optimmaxvar_'> Z;"
#write <trees.c> "%O"
#write <trees.c> "return a14{%E};", a14
#write <trees.c> "}"

.end
