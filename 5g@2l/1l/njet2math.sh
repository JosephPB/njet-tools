#!/usr/bin/env bash

OUT=$1.out

perl \
    -0777p \
    -e  "
        s|\(([\d.e+\-]+),([\d.e+\-]+)\)|(\1)|g;
        s|e([+-]\d+)|*10^(\1)|g;
        s|\/e\^|/eps^|g;
        s|\((.+,.+)\)|{\1}|g;
        " \
    $1 > ${OUT}1

        # s|\(([\d.e+\-]+),([\d.e+\-]+)\)|(\1+I*(\2))|g;
        # s|$|,|gm;
        # s|\(|{|g;
        # s|\)|}|g;

tail -n +30 ${OUT}1 > ${OUT}2
head -n -15 ${OUT}2 > $OUT
echo } >> $OUT
echo } >> $OUT
