#include <array>
#include <cmath>
#include <iostream>
#include <numeric>

template <int mul>
struct Helicity : std::array<int, mul> {
    int signed_order() const
    {
        return std::accumulate(this->cbegin(), this->cend(), 0);
    }

    int order() const
    {
        return std::abs(this->signed_order());
    }

    bool more_plus() const
    {
        return this->signed_order() > 0;
    }
};

template <int mul>
std::ostream& operator<<(std::ostream& out, const Helicity<mul>& hel)
{
    for (int h : hel) {
        out << (h == 1 ? '+' : '-');
    }
    return out;
}
