#include "0q5g-analytic.h"

template <typename T> 
std::array<DoubleLoopResult<T>, 12> Amp0q5g_a<T>::hA2gHH()
  {
    const std::array<int, 5> o{0, 1, 2, 3, 4};
    const TreeValue phase{-i_ * hA0HH(o.data()) / hA0HHX()};

    const DoubleLoopValue p1e{hA2gHHp1e()};
    const DoubleLoopValue p2e{hA2gHHp2e()};
    const DoubleLoopValue p3e{hA2gHHp3e()};
    const DoubleLoopValue p4e{hA2gHHp4e()};
    const DoubleLoopValue p5e{hA2gHHp5e()};
    const DoubleLoopValue p6e{hA2gHHp6e()};
    const DoubleLoopValue p7e{hA2gHHp7e()};
    const DoubleLoopValue p8e{hA2gHHp8e()};
    const DoubleLoopValue p9e{hA2gHHp9e()};
    const DoubleLoopValue p10e{hA2gHHp10e()};
    const DoubleLoopValue p11e{hA2gHHp11e()};
    const DoubleLoopValue p12e{hA2gHHp12e()};

    const DoubleLoopValue p1o{hA2gHHp1o()};
    const DoubleLoopValue p2o{hA2gHHp2o()};
    const DoubleLoopValue p3o{hA2gHHp3o()};
    const DoubleLoopValue p4o{hA2gHHp4o()};
    const DoubleLoopValue p5o{hA2gHHp5o()};
    const DoubleLoopValue p6o{hA2gHHp6o()};
    const DoubleLoopValue p7o{hA2gHHp7o()};
    const DoubleLoopValue p8o{hA2gHHp8o()};
    const DoubleLoopValue p9o{hA2gHHp9o()};
    const DoubleLoopValue p10o{hA2gHHp10o()};
    const DoubleLoopValue p11o{hA2gHHp11o()};
    const DoubleLoopValue p12o{hA2gHHp12o()};

    std::array<DoubleLoopValue, 12> amps{
        p1e + p1o, p2e + p2o,   p3e + p3o,   p4e + p4o,
        p5e + p5o, p6e + p6o,   p7e + p7o,   p8e + p8o,
        p9e + p9o, p10e + p10o, p11e + p11o, p12e + p12o,
    };

    for (DoubleLoopValue &amp : amps) {
      amp = phase * correction2L(amp);
    }

    for (int i{0}; i < 678; ++i) {
      f[i] = std::conj(f[i]);
    }

    std::array<DoubleLoopValue, 12> camps{
        p1e - p1o, p2e - p2o,   p3e - p3o,   p4e - p4o,
        p5e - p5o, p6e - p6o,   p7e - p7o,   p8e - p8o,
        p9e - p9o, p10e - p10o, p11e - p11o, p12e - p12o,
    };

    const TreeValue cphase{std::conj(phase)};

    for (DoubleLoopValue &camp : camps) {
      camp = cphase * correction2L(camp);
    }

    std::array<DoubleLoopResult<T>, 12> resvec;

    for (int i{0}; i < 12; ++i) {
      resvec[i] = {amps[i], camps[i]};
    }

    return resvec;
  }

#ifdef USE_SD
  template class Amp0q5g_a<double>;
#endif
#ifdef USE_DD
  template class Amp0q5g_a<dd_real>;
#endif
#ifdef USE_QD
  template class Amp0q5g_a<qd_real>;
#endif
#ifdef USE_VC
  template class Amp0q5g_a<Vc::double_v>;
#endif
