#!/usr/bin/env bash

perl \
    -0777p \
    -i.bak \
    -e  "
        s|f\[(\d+), (\d+)\]|g_\1_\2|g;
        s|f\[(\d+), (\d+), (\d+)\]|g_\1_\2_\3|g;
        s|pow\[eps, -1\]|DoubleLoopValue(T(0.),T(1.),T(0.),T(0.),T(0.))|g;
        s|pow\[eps, -2\]|DoubleLoopValue(T(0.),T(0.),T(1.),T(0.),T(0.))|g;
        s|pow\[eps, -3\]|DoubleLoopValue(T(0.),T(0.),T(0.),T(1.),T(0.))|g;
        s|pow\[eps, -4\]|DoubleLoopValue(T(0.),T(0.),T(0.),T(0.),T(1.))|g;
        s|([ (-])(\d+)\*|\1T(\2.)*|g;
        s|([/])(\d+)|\1T(\2.)|g;
        s|([ (-])(\d+) ([+-])|\1T(\2.) \3|g;
        s| 0,| T(0.),|g;
        " \
    $1

# clang-format -i $1
