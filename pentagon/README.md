# pentagon

## Description

Testing PentagonFunctions-cpp
* https://gitlab.com/pentagon-functions/PentagonFunctions-cpp
* https://arxiv.org/abs/2009.07803

## Testing installation with Docker
```shell
podman run -it --rm docker.io/library/debian:bullseye bash
```
```shell
apt update
apt install -y g++ wget make meson git cmake pkg-config
wget https://www.davidhbailey.com/dhbsoftware/qd-2.3.22.tar.gz
tar xf qd-2.3.22.tar.gz
cd qd-2.3.22
./configure
make
make install
cd ..
git clone https://gitlab.com/pentagon-functions/PentagonFunctions-cpp.git
cd PentagonFunctions-cpp
meson setup build -D high-precision=true
```
PROBLEM with qd: it doesn't install a pkgconfig qd.pc!

## Install PentagonFunctions-cpp
First install `meson` and `ninja`, eg.
```shell
python3 -m pip install --user meson ninja
```
Then install [`qd >= 2.3.22`](https://www.davidhbailey.com/dhbsoftware/)
* laptop
    ```shell
    echo "export QD_LOCAL=$HOME/local/qd" >> ~/.zshrc && source ~/.zshrc
    cd ~/scratch
    wget https://www.davidhbailey.com/dhbsoftware/qd-2.3.22.tar.gz
    tar xf qd-2.3.22.tar.gz
    cd qd-2.3.22
    ./configure --prefix=$QD_LOCAL
    make
    make install
    ```
* IPPP
    ```shell
    echo "export QD_LOCAL=$HOME/local/qd" >> ~/.profile && source ~/.bashrc
    cd ~/build
    wget https://www.davidhbailey.com/dhbsoftware/qd-2.3.22.tar.gz
    tar -xf qd-2.3.22.tar.gz
    cd qd-2.3.22
    mkdir build
    cd build
    ../configure --prefix=$QD_LOCAL --with-pic=yes
    make -j
    make install
    ```
and copy over `$QD_LOCAL/pkgconfig/qd.pc` [pkgconfig file](https://people.freedesktop.org/~dbn/pkg-config-guide.html)
```vim
mkdir $QD_LOCAL/pkgconfig
cp qd.pc $QD_LOCAL/pkgconfig
```
and add `~/.zshrc` entries (then `source`)
```vim
export LD_LIBRARY_PATH=$QD_LOCAL/lib:$LD_LIBRARY_PATH
export PATH=$QD_LOCAL/bin:$PATH
export PKG_CONFIG_PATH=$QD_LOCAL/pkgconfig:$PKG_CONFIG_PATH
```
then
```shell
echo "export PENTA_LOCAL=$HOME/local/pentagon" >> ~/.zshrc && source ~/.zshrc
git clone https://gitlab.com/pentagon-functions/PentagonFunctions-cpp.git
cd PentagonFunctions-cpp
meson setup $HOME/scratch/pentagonfunctions -D prefix=$PENTA_LOCAL -D high-precision=true -D examples=true -D pkg_config_path=$QD_LOCAL/pkgconfig # w/o examples on IPPP system
cd $HOME/scratch/pentagonfunctions
ninja install # takes a while
echo "export LD_LIBRARY_PATH=$PENTA_LOCAL/lib64:$LD_LIBRARY_PATH" >> ~/.zshrc && source ~/.zshrc
mkdir $PENTA_LOCAL/pkgconfig
cp meson-private/PentagonFunctions.pc $PENTA_LOCAL/pkgconfig
cp meson-private/Li2++.pc $PENTA_LOCAL/pkgconfig
```
and add `~/.zshrc` entries (and `source`)
```vim
export LD_LIBRARY_PATH=$PENTA_LOCAL/lib64:$LD_LIBRARY_PATH
export PKG_CONFIG_PATH=$PENTA_LOCAL/pkgconfig:$PKG_CONFIG_PATH
```

Access paths with `pkg-config PentagonFunctions` + `--cflags` or `--libs`.

## PentagonFunctions in NJet
### Production
Configure with
```shell
CPPFLAGS=`pkg-config PentagonFunctions --cflags` LDFLAGS=`pkg-config PentagonFunctions --libs` FFLAGS='-std=legacy' CXXFLAGS='-std=c++17 -O2 -Wall' ~/git/njet-develop/configure --prefix=$NJET_LOCAL --enable-oneloop1 --enable-2loop --enable-quaddouble
```
then add to `Makefile` of script using NJet:
```shell
CPPFLAGS+=-I${PENTA_LOCAL}/include/PentagonFunctions
LDFLAGS+=-L${PENTA_LOCAL}/lib64
LIBS+= -lPentagonFunctions -lLi2++
```
or use `pkf-config` (RECOMMENDED)
```shell
CPPFLAGS+=`pkg-config PentagonFunctions --cflags`
LDFLAGS+==`pkg-config PentagonFunctions --libs`
```
### Development
For quick dev builds on laptop with no 2-loops:
```shell
CPPFLAGS=`pkg-config PentagonFunctions --cflags` LDFLAGS=`pkg-config PentagonFunctions --libs` FFLAGS='-std=legacy' CXXFLAGS='-std=c++17 -Wall -Wextra -g -O0' ~/git/njet-develop/configure --prefix=$NJET_LOCAL --enable-oneloop1 --with-qd=no --disable-analytic4jet
```
Could also use `-Og`.
### IPPP
On scratch, with 2-loops,
```shell
CPPFLAGS=`pkg-config PentagonFunctions --cflags` LDFLAGS=`pkg-config PentagonFunctions --libs` FFLAGS='-std=legacy' CXXFLAGS='-std=c++17 -Wall -Wextra -g -O0' $SCRATCH/njet-develop/configure --prefix=$NJET_LOCAL --enable-oneloop1 --enable-2loop --with-qd=no
```
Or, to compile with DD (not on scratch)
* Works fine with 1L, but
* Trying with 2L gives the error:
```
libtool: link: g++  -fPIC -DPIC -shared -nostdlib /lib/../lib64/crti.o /opt/rh/devtoolset-8/root/usr/lib/gcc/x86_64-redhat-linux/8/crtbeginS.o  blha/.libs/libnjet2_la-njet_olp.o  -Wl,--whole-archive ngluon2/.libs/libngluon2.a chsums/.libs/libchsums.a analytic/.libs/libanalytic.a ir/.libs/libir.a lib-oneloop1/.libs/liboneloop1.a -Wl,--no-whole-archive  -Wl,-rpath -Wl,/mt/home/rmoodie/build/njet/lib-qcdloop1/.libs -Wl,-rpath -Wl,/mt/home/rmoodie/build/njet/lib-qd/src/.libs -Wl,-rpath -Wl,/mt/home/rmoodie/build/njet/lib-qd/fortran/.libs -Wl,-rpath -Wl,/mt/home/rmoodie/local/lib -L/mt/home/rmoodie/local/pentagon/lib64 lib-qcdloop1/.libs/libqcdloop1.so -L/opt/rh/devtoolset-8/root/usr/lib/gcc/x86_64-redhat-linux/8 -L/opt/rh/devtoolset-8/root/usr/lib/gcc/x86_64-redhat-linux/8/../../../../lib64 -L/lib/../lib64 -L/usr/lib/../lib64 -L/opt/rh/devtoolset-8/root/usr/lib/gcc/x86_64-redhat-linux/8/../../.. lib-qd/src/.libs/libqd.so lib-qd/fortran/.libs/libqdmod.so -lPentagonFunctions -lgfortran -lquadmath -lstdc++ -lm -lc -lgcc_s /opt/rh/devtoolset-8/root/usr/lib/gcc/x86_64-redhat-linux/8/crtendS.o /lib/../lib64/crtn.o  -O0 -mcmodel=medium -Wl,--version-script=./libnjet.ver   -Wl,-soname -Wl,libnjet2.so.0 -o .libs/libnjet2.so.0.0.0
/lib/../lib64/crti.o: In function `_init':
(.init+0x7): relocation truncated to fit: R_X86_64_GOTPCREL against undefined symbol `__gmon_start__'
/opt/rh/devtoolset-8/root/usr/lib/gcc/x86_64-redhat-linux/8/crtbeginS.o: In function `deregister_tm_clones':
crtstuff.c:(.text+0x3): relocation truncated to fit: R_X86_64_PC32 against `.tm_clone_table'
crtstuff.c:(.text+0xa): relocation truncated to fit: R_X86_64_PC32 against symbol `__TMC_END__' defined in .data section in .libs/libnjet2.so.0.0.0
crtstuff.c:(.text+0x16): relocation truncated to fit: R_X86_64_GOTPCREL against undefined symbol `_ITM_deregisterTMCloneTable'
/opt/rh/devtoolset-8/root/usr/lib/gcc/x86_64-redhat-linux/8/crtbeginS.o: In function `register_tm_clones':
crtstuff.c:(.text+0x33): relocation truncated to fit: R_X86_64_PC32 against `.tm_clone_table'
crtstuff.c:(.text+0x3a): relocation truncated to fit: R_X86_64_PC32 against symbol `__TMC_END__' defined in .data section in .libs/libnjet2.so.0.0.0
crtstuff.c:(.text+0x57): relocation truncated to fit: R_X86_64_GOTPCREL against undefined symbol `_ITM_registerTMCloneTable'
/opt/rh/devtoolset-8/root/usr/lib/gcc/x86_64-redhat-linux/8/crtbeginS.o: In function `__do_global_dtors_aux':
crtstuff.c:(.text+0x72): relocation truncated to fit: R_X86_64_PC32 against `.bss'
crtstuff.c:(.text+0x7d): relocation truncated to fit: R_X86_64_GOTPCREL against symbol `__cxa_finalize@@GLIBC_2.2.5' defined in .text section in /lib64/libc.so.6
crtstuff.c:(.text+0x8a): relocation truncated to fit: R_X86_64_PC32 against symbol `__dso_handle' defined in .data.rel.ro.local section in /opt/rh/devtoolset-8/root/usr/lib/gcc/x86_64-redhat-linux/8/crtbeginS.o
crtstuff.c:(.text+0x9a): additional relocation overflows omitted from the output
/opt/rh/devtoolset-8/root/usr/libexec/gcc/x86_64-redhat-linux/8/ld: failed to convert GOTPCREL relocation; relink with --no-relax
collect2: error: ld returned 1 exit status
```
* Attempt 1: use `mcmodel` (FAIL)
```shell
CPPFLAGS=`pkg-config PentagonFunctions --cflags` LDFLAGS=`pkg-config PentagonFunctions --libs` FFLAGS='-std=legacy' CXXFLAGS='-std=c++17 -Wall -Wextra -g -O0 -mcmodel=medium' $GIT/njet-develop/configure --prefix=$NJET_LOCAL --enable-oneloop1 --enable-2loop
```
using [medium memory model](https://stackoverflow.com/questions/12916176/gfortran-for-dummies-what-does-mcmodel-medium-do-exactly).

## Converting Mathematica output for C++

* Unique F(...) to declarations
```vim
:%s/F(\(\d\),\(\d\+\))/PentagonFunctions::FunctionObjectType<T> F_\1_\2 { PentagonFunctions::FunctionID(\1, \2).get_evaluator<T>() };/g
:%s/F(\(\d\),\(\d\+\),\(\d\+\))/PentagonFunctions::FunctionObjectType<T> F_\1_\2_\3 { PentagonFunctions::FunctionID(\1, \2, \3).get_evaluator<T>() };/g
```
* then F(...) value defs
```vim
:%s/^.*F\(_[^ ]\+\) .*$/TreeValue fv\1;/g
```
* then F value assignments
```vim
:%s/TreeValue fv\(_.*\);/fv\1 = F\1(k);/g
```

* funcmonomials
```vim
:%s/tci(\(\d\),\(\d\))/tci\1\2/g
:%s/tcr(\(\d\),\(\d\))/tcr\1\2/g
:%s/Power/pow/g
:%s/F(\(\d\),\(\d\+\))/fv_\1_\2/g
:%s/F(\(\d\),\(\d\+\),\(\d\+\))/fv_\1_\2_\3/g
:%s/Complex/TreeValue/g
:%s/\(-\d\+\)\ /T(\1.) /g
:%s/(\(\d\+\)\ /(T(\1.) /g
```

* Easiest to convert to pow in Mathematica `Power[a_,b_]->pow[a,b]` and not use `CForm` to preserve fractions
    * Use `InputForm` to get `*`s

# Matching Mathematica and C++
* `test_pf.wl` agrees with `standalone/eg2.cpp`
