(* ::Package:: *)

(* ::Section:: *)
(*Set up*)


SetAttributes[fundamentalDelta,Orderless];
SetAttributes[adjointDelta,Orderless];
Format[fundamentalDynkinIndex]:=Subscript["T","F"];
Format[A[a__]]:=Subscript["A",Length[List[a]]][a];
Format[fundamentalTrace[a__]]:=Subscript["tr","F"][a];
Format[dagger[a_]]:=Power[a,"\[ConjugateTranspose]"];
applyRules[expr_,rules_]:=FixedPoint[ExpandAll[#]/.rules&,expr];
rules={
	dagger[-1*a_]->-dagger[a],
	dagger[a___f*b___fundamentalTrace]->dagger[a]*dagger[b],
	dagger[a_+ b__]:>dagger[a]+dagger[b],
	dagger[a_fundamentalTrace]:>Reverse[a],
	dagger[a_f]:>a,
	a_fundamentalTrace:>Module[
		{p=FirstPosition[a,Min[List@@a]][[1]]},
		Join[a[[p;;]],a[[;;p-1]]]
		]/;a[[1]]!=Min[List@@a],
	a_f:>(-I/fundamentalDynkinIndex)*(fundamentalTrace@@a-Reverse[fundamentalTrace@@a]),
	t_fundamentalTrace:>Times@@(Module[
		{i},
		T[a[t[[#]]],i[#],i[Mod[#+1,Length[t],1]]]&/@Range[Length[t]]
		]),
	T[a_,i_,j_]*T[b_,j_,i_]:>fundamentalDynkinIndex*adjointDelta[a,b],
	T[a_,i1_,i2_]*T[a_,i3_,i4_]:>
		fundamentalDynkinIndex(fundamentalDelta[i1,i4]*fundamentalDelta[i2,i3]
								-(1/Nc)*fundamentalDelta[i1,i2]*fundamentalDelta[i3,i4]),
	adjointDelta[i_,i_]:>Nc^2-1,
	adjointDelta[i1_,i2_]^2:>Nc^2-1,
	adjointDelta[i1_,i2_]*adjointDelta[i2_,i3_]:>adjointDelta[i1,i3],
	T[a1_,i1_,i2_]*adjointDelta[a1_,a2_]:>T[a2,i1,i2],
	fundamentalDelta[i_,i_]:>Nc,
	fundamentalDelta[i1_,i2_]^2:>Nc,
	fundamentalDelta[i1_,i2_]*fundamentalDelta[i2_,i3_]:>fundamentalDelta[i1,i3],
	T[a1_,i1_,i2_]*fundamentalDelta[i1_,i3_]:>T[a1,i3,i2],
	T[a1_,i1_,i2_]*fundamentalDelta[i2_,i3_]:>T[a1,i1,i3],
	T[a_,i_,i_]->0
	};
nonCyclicalPermutationsReflected[l_]:=Prepend[#,l[[1]]]&/@Permute[l[[2;;]],CyclicGroup[Length@l-1]];
qcd=fundamentalDynkinIndex->1/2;
export[expr_]:=CForm[Simplify[expr]/.{(Nc^2-1)->V,Nc^6->Nc6,Nc^5->Nc5,Nc^4->Nc4,Nc^3->Nc3,Nc^2->Nc2,Nc^(-2)->1/Nc2}];


(*Everything is in the colour basis of traces over fundamental generators (using the reflection symmetry to half the basis size)*)
amplitude[l_]:=Plus@@((fundamentalTrace@@#+(-1)^(-Length@l)*fundamentalTrace@@Reverse[#])*A@@#&/@(nonCyclicalPermutationsReflected[l]));

(*Manually specify unique Einstein summed index with argument `extra` - nb n+1 already used (& n+2 for double soft)!*)
(*These are for soft gluon emissions from a gluon leg of the reduced amplitude*)
ccAmp[n_,i_,extra_]:=Module[
	{legs},
	legs=Join[Range[1,i-1],{extra},Range[i+1,n]];
	Return[amplitude[legs]*f[extra,i, n+1]];
	];
	
cc2Amp[n_,i_,j_,extra1_,extra2_]:=Module[
	{legs,ii,jj},
	If[i==j,
	legs=Join[Range[1,i-1],{extra1},Range[i+1,n]];
	Return[amplitude[legs]*f[extra1,extra2,n+1]*f[extra2,i,n+2]];
	,
	If[j<i,jj=i;ii=j;,ii=i;jj=j;];
	legs=Join[Range[1,ii-1],{extra1},Range[ii+1,jj-1],{extra2},Range[jj+1,n]];
	Return[amplitude[legs]*f[extra1,ii, n+1]*f[extra2,jj,n+2]];
	]
	];

processAmplitude[amplitude_]:=Module[
	{partialAmplitudes},
	partialAmplitudes=DeleteDuplicates[Cases[amplitude,_A,Infinity]];
	Return[{Coefficient[amplitude,#]&/@partialAmplitudes,partialAmplitudes}];
	]; 

normalisation[n_]:=fundamentalDynkinIndex^n*Nc^(n-6)*(Nc^2-1);

generateNJetColMat[n_,norm_,v_:False]:=Module[
	{amp1,amp2,coefficients1,partialAmplitudesVector1,coefficients2,partialAmplitudesVector2,matrix,sol,mat,vec,size,facs,facRep,matRepd,lenC},
	amp1=amplitude[Range[n]];
	amp2=amplitude[Range[n]];
	If[v,Print["Amplitude"]];
	If[v,Print[amp1]];
	If[v,Print["Conjugate amplitude"]];
	If[v,Print[dagger[amp2]]];
	If[v,Print["Squared amplitude"]];
	If[v,Print[amp1*dagger[amp2]]];
	{coefficients1,partialAmplitudesVector1}=processAmplitude[amp1];
	{coefficients2,partialAmplitudesVector2}=processAmplitude[amp2];
	If[v,Print["Coefficients"]];
	If[v,Print[coefficients1]];
	If[v,Print[coefficients2]];
	lenC=Length[coefficients1];
	matrix=applyRules[
		Outer[#1*dagger[#2]&,coefficients1,coefficients2],
		rules
		];
	If[v,Print["Normalisation (unformatted)"]];
	If[v,Print[norm/.qcd]];
	mat=matrix/norm;
	vec=partialAmplitudesVector1;
	If[v,Print["Result"]];
	If[v,Print[MatrixForm/@{Expand[mat],vec}/.qcd]];
	Print["Partial amps"];
	Print[(If[#>n,i-1,#-1]&/@vec[[#]])&/@Range[lenC]];
	size=Length[mat[[1]]];
	facs=DeleteDuplicates[Flatten[mat]];
	facRep=(facs[[#]]->#-1)&/@Range[Length[facs]];
	If[v,Print["Replacement rules"]];
	If[v,Print[facRep/.qcd]];
	matRepd=(#/.facRep)&/@mat;
	Print["Normalisation"];
	Print[export[norm/.qcd]];
	Print["Colour matrix"];
	Print[MatrixForm[matRepd]];
	Print["Lower triangle (scan rows)/upper triangle (scan columns)"];
	Print[DeleteCases[Flatten[Table[Table[If[ii<=jj,matRepd[[ii,jj]],Null],{ii,size}],{jj,size}]],Null]];
	Print["Element values"];
	Print[export/@facs/.qcd];
	Return[matrix];
	];

generateNJetColMatCC[n_,norm_,i_,j_,v_:False]:=Module[
	{amp1,amp2,coefficients1,partialAmplitudesVector1,coefficients2,partialAmplitudesVector2,matrix,sol,mat,vec,size,facs,facRep,matRepd,lenC},
	If[i<1||i>n,Print["i must be in [1," + n + "]"]];
	If[j<1||j>n,Print["j must be in [1," + n + "]"]];
	amp1=ccAmp[n,i,n+2];
	amp2=ccAmp[n,j,n+3];
	If[v,Print["Amplitude"]];
	If[v,Print[amp1]];
	If[v,Print["Conjugate amplitude"]];
	If[v,Print[dagger[amp2]]];
	If[v,Print["Squared amplitude"]];
	If[v,Print[amp1*dagger[amp2]]];
	{coefficients1,partialAmplitudesVector1}=processAmplitude[amp1];
	{coefficients2,partialAmplitudesVector2}=processAmplitude[amp2];
	If[v,Print["Coefficients"]];
	If[v,Print[coefficients1]];
	If[v,Print[coefficients2]];
	lenC=Length[coefficients1];
	matrix=applyRules[
		Outer[#1*dagger[#2]&,coefficients1,coefficients2],
		rules
		];
	If[v,Print["Normalisation (unformatted)"]];
	If[v,Print[norm/.qcd]];
	mat=matrix/norm;
	vec=partialAmplitudesVector1;
	If[v,Print["Result"]];
	If[v,Print[MatrixForm/@{Expand[mat],vec}/.qcd]];
	Print["Partial amps"];
	Print[(If[#>n,i-1,#-1]&/@vec[[#]])&/@Range[lenC]];
	size=Length[mat[[1]]];
	facs=DeleteDuplicates[Flatten[mat]];
	facRep=(facs[[#]]->#-1)&/@Range[Length[facs]];
	If[v,Print["Replacement rules"]];
	If[v,Print[facRep/.qcd]];
	matRepd=(#/.facRep)&/@mat;
	Print["Normalisation"];
	Print[export[norm/.qcd]];
	Print["Colour matrix"];
	Print[MatrixForm[matRepd]];
	Print["Lower triangle (scan rows)/upper triangle (scan columns)"];
	Print[DeleteCases[Flatten[Table[Table[If[ii<=jj,matRepd[[ii,jj]],Null],{ii,size}],{jj,size}]],Null]];
	Print["Element values"];
	Print[export/@facs/.qcd];
	Return[matrix];
	];
	
generateNJetColMatCC2[n_,norm_,i_,j_,k_,l_,v_:False]:=Module[
	{tmp1,tmp2,amp1,amp2,coefficients1,partialAmplitudesVector1,coefficients2,partialAmplitudesVector2,matrix,sol,mat,vec,size,facs,facRep,matRepd,lenC},
	If[i<1||i>n,Print["i must be in [1," + n + "]"]];
	If[j<1||j>n,Print["j must be in [1," + n + "]"]];
	If[k<1||k>n,Print["k must be in [1," + n + "]"]];
	If[l<1||l>n,Print["l must be in [1," + n + "]"]];
	tmp1=n+3;
	tmp2=n+4;
	amp1=cc2Amp[n,i,j,tmp1,tmp2];
	amp2=cc2Amp[n,k,l,n+5,n+6];
	(*If[i\[Equal]k,amp2/.{fundamentalTrace[a___,i,b___]\[Rule]fundamentalTrace[a,-i,b],f[a___,i,b___]\[Rule]f[a,-i,b]}]*);
	If[v,Print["Amplitude"]];
	If[v,Print[amp1]];
	If[v,Print["Conjugate amplitude"]];
	If[v,Print[dagger[amp2]]];
	If[v,Print["Squared amplitude"]];
	If[v,Print[amp1*dagger[amp2]]];
	{coefficients1,partialAmplitudesVector1}=processAmplitude[amp1];
	{coefficients2,partialAmplitudesVector2}=processAmplitude[amp2];
	If[v,Print["Coefficients"]];
	If[v,Print[coefficients1]];
	If[v,Print[coefficients2]];
	lenC=Length[coefficients1];
	matrix=applyRules[
		Outer[#1*dagger[#2]&,coefficients1,coefficients2],
		rules
		];
(*	If[v,Print["Normalisation (unformatted)"]];
	If[v,Print[norm/.qcd]];
	mat=matrix/norm;
	vec=partialAmplitudesVector1;
	If[v,Print["Result"]];
	If[v,Print[MatrixForm/@{Expand[mat],vec}/.qcd]];
	Print["Partial amps"];
	Print[(If[#\[Equal]tmp2,j-1,If[#\[Equal]tmp1,i-1,#-1]]&/@vec[[#]])&/@Range[lenC]];
	size=Length[mat[[1]]];
	facs=DeleteDuplicates[Flatten[mat]];
	facRep=(facs[[#]]->#-1)&/@Range[Length[facs]];
	(*If[v,Print["Replacement rules"]];
	If[v,Print[facRep/.qcd]];*)
	matRepd=(#/.facRep)&/@mat;
	Print["Normalisation"];
	Print[export[norm/.qcd]];
	Print["Colour matrix"];
	Print[MatrixForm[matRepd]];
	Print["Lower triangle (scan rows)/upper triangle (scan columns)"];
	Print[DeleteCases[Flatten[Table[Table[If[ii<=jj,matRepd[[ii,jj]],Null],{ii,size}],{jj,size}]],Null]];
	Print["Element values"];
	Print[export/@facs/.qcd];*)
	Return[matrix];
	];


(* ::Section:: *)
(*Run: colour correlation matrices*)


(* ::Subsection::Closed:: *)
(*4 g*)


CC=generateNJetColMat[4,normalisation[4]*2];


(* ::Subsection::Closed:: *)
(*4g + single correlation*)


CC11=generateNJetColMatCC[4,normalisation[4]*Nc/2,1,1,True];


CC11/.{fundamentalDynkinIndex->1}//Simplify//MatrixForm


CC11/CC/.qcd//Factor//MatrixForm


CC12=generateNJetColMatCC[4,normalisation[4]*Nc/2,1,2,True];


CC12/(Nc^2-1)*Nc/2/.{fundamentalDynkinIndex->1}//Simplify//MatrixForm


CC13=generateNJetColMatCC[4,normalisation[4]*Nc/2,1,3];


CC14=generateNJetColMatCC[4,normalisation[4]*Nc/2,1,4];


(* ::Subsection:: *)
(*4 g + double correlation*)


CC1234=generateNJetColMatCC2[4,normalisation[4]*Nc/2,1,2,3,4]


CC1234/.qcd//Simplify//MatrixForm


CC1211=generateNJetColMatCC2[4,normalisation[4]*Nc/2,1,2,1,1,True];


CC1211[[1]][[1]]/.qcd//Factor


(* EOF *)
