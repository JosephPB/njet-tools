(* ::Package:: *)

(* ::Section::Closed:: *)
(*Set up*)


(* global variables *)
(* flavours in reduced matrix element *)
Flavours = {g,g,g,g};
nn = Length[Flavours];
cbl = 3; (* colour basis length *)


<< "/home/ryan/git/myfiniteflowexamples/InitDiagramsFF.m"
<< "/home/ryan/git/myfiniteflowexamples/InitFORMpaths.m";
cfaclabels = {};
SetDirectory["/home/ryan/git/njet-tools/colour-matrices"];


export[expr_]:=CForm[Simplify[expr]/.{(Nc^2-1)->V,Nc^6->Nc6,Nc^5->Nc5,Nc^4->Nc4,Nc^3->Nc3,Nc^2->Nc2,Nc^(-2)->1/Nc2}];


TT[i_,colourfactor_] := Module[{res},
If[Flavours[[i]]==g,
 res = F[aa[-i],aa[i],aa[-nn-1]]*(colourfactor /. aa[-i]->aa[i]);
];
Return[res];
];

TT[i_,j_,colourfactor_] := Module[{res,k},
If[Flavours[[i]]==g,
 res = F[aa[-i],aa[i],aa[-nn-1]]*(colourfactor /. aa[-i]->aa[i]);
];
If[i==j,k=i+1,k=j];
If[Flavours[[j]]==g,
 res = F[aa[-j],aa[k],aa[-nn-2]]*(res /. aa[-j]->aa[k]);
];
Return[res];
];


ColourBasis4g = aa[-#]&/@TTT@@Prepend[#,1]&/@Permute[Range[2,nn],CyclicGroup[nn-1]] /. TTT[a__]:>trT[a]+(-1)^Length[{a}]*trT@@Reverse[{a}];


ColourBasis4gCC = ColourBasis4g /. t_trT:>Reverse[t];


(*Memoize*)
ColourBasis4gCorrelated[i_]:= ColourBasis4gCorrelated[i] = FORMColourContract[TT[i,#]]&/@ColourBasis4g;
ColourBasis4gCCCorrelated[i_]:= ColourBasis4gCCCorrelated[i] = ColourBasis4gCorrelated[i] /. t_trT:>Reverse[t];


(*Memoize*)
SetAttributes[CC1,Orderless];
CC1[i_,j_]:=CC1[i,j]=Outer[FORMColourContract[Expand[#1*#2]]&,ColourBasis4gCCCorrelated[i],ColourBasis4gCorrelated[j]]/2;


(*Memoize*)
ColourBasis4gCorrelated[i_,j_] := ColourBasis4gCorrelated[i,j] = FORMColourContract[TT[i,j,#]]&/@ColourBasis4g;
ColourBasis4gCCCorrelated[i_,j_] := ColourBasis4gCCCorrelated[i,j] = ColourBasis4gCorrelated[i,j] /. t_trT:>Reverse[t];


(*Memoize*)
CC2[i_,j_,k_,l_]:=CC2[i,j,k,l]=(
  Outer[FORMColourContract[Expand[#1*#2]]&,ColourBasis4gCCCorrelated[i,j],ColourBasis4gCorrelated[k,l]]
+ Outer[FORMColourContract[Expand[#1*#2]]&,ColourBasis4gCCCorrelated[k,l],ColourBasis4gCorrelated[i,j]]
)/4


(* ::Section::Closed:: *)
(*Test*)


structure[mat_]:=Module[
	{facs,facRep,matRepd},
	facs=DeleteDuplicates[Flatten[mat]];
	facRep=(facs[[#]]->#-1)&/@Range[Length@facs];
	matRepd=(#/.facRep)&/@mat;
	Return[MatrixForm[matRepd]];
];


CC = Outer[FORMColourContract[Expand[#1*#2]]&,ColourBasis4gCC,ColourBasis4g];


CC1[1,1]/CC//Factor//MatrixForm


Sum[CC1[1,i],{i,4}]//MatrixForm


CC2[1,1,1,1]/CC//Factor//MatrixForm


CC2[1,1,1,1]/CC1[1,1]//Factor//MatrixForm


CC2[1,2,1,1]/CC1[1,2]//Factor // MatrixForm


CC2[1,2,1,3]/CC1[2,3]//Factor // MatrixForm


CC2[1,1,2,1]/CC1[1,2]//Factor//MatrixForm


CC2[2,1,1,1]/CC1[1,2]//Factor//MatrixForm


CC2[1,1,1,2]/CC1[1,2]//Factor//MatrixForm


CC2[1,1,1,3]/CC1[1,3]//Factor//MatrixForm


CC2[1,3,1,2]/CC1[2,3]//Factor//MatrixForm


CC2[2,2,2,2]/CC1[2,2]//Factor//MatrixForm


CC2[2,2,4,2]/CC1[2,4]//Factor//MatrixForm


CC2[1,2,1,2]/CC1[2,2]//Factor//MatrixForm


CC2[1,2,2,1]/CC//Factor//MatrixForm


structure[CC2[1,2,2,1]]


Table[structure[CC1[i,j]],{i,nn},{j,nn}]//TableForm


Table[structure[CC2[1,1,i,j]],{i,2},{j,2}]//TableForm


CC2[2,4,2,1]/CC1[1,4]//Factor//MatrixForm


CC2[2,1,3,1]/CC1[2,3]//Factor//MatrixForm


CC2[1,2,1,1]/CC1[1,2]//Factor//MatrixForm


CC2[1,1,1,2]/CC1[1,2]//Factor//MatrixForm


CC2[1,3,1,1]/CC1[1,3]//Factor//MatrixForm


CC2[4,2,3,4]/CC1[2,3]//Factor//MatrixForm


CC2[4,2,3,4]/CC//Factor//MatrixForm


CC2[4,3,3,1]/CC1[1,4]//Factor//MatrixForm


CC2[4,3,3,1]/CC//Factor//MatrixForm


CC2[1,2,3,4]/CC//Factor//MatrixForm


CC2[3,4,1,2]/CC//Factor//MatrixForm


CC2[1,2,3,4]/CC2[3,4,1,2]//MatrixForm


CC2[2,1,3,4]/CC//Factor//MatrixForm


CC2[1,2,3,4]/CC2[2,1,4,3]//Factor//MatrixForm


Sum[CC2[1,1,i,j],{i,1,4},{j,1,4}]


CC2[1,1,3,4]/CC1[3,4]+Table[2 Nc,{i,3},{j,3}]//Simplify//MatrixForm


CC1[1,1]//Simplify//MatrixForm


((Nc^3 (6+Nc^2))/(2 (-1+Nc^2)))/((2 Nc^3 (6+Nc^2))/(4+Nc^4))//Simplify


CC2[1,1,2,2]/CC1[1,2]^2//Factor//MatrixForm


(* ::Section::Closed:: *)
(*Out 1 - obsolete*)


res=Table[
	If[
		(10*i+j)<=(10*k+l),
		Which[
			i==j==k&&i!=l,Nc*CC1[i,l],
			i==k==l&&i!=j,Nc*CC1[i,j],
			j==l,2*Nc*CC1[i,k],
			i==k&&i!=j&&k!=l,2*Nc*CC1[j,l],
			True,CC2a[i,j,k,l]
			],
		Null
		],
	{i,nn},{k,nn},{j,nn},{l,nn}
	];
(*%//TableForm*)


ijkl[i_,j_,k_,l_]:=res[[i,k,j,l]]


structure[ijkl[1,3,1,3]]//MatrixForm


(* use output3 instead *)
output[mat_]:=Module[
	{symmat,flmat,facs,facRep,matRepd},
	symmat=Table[Infinity,{i,nn},{k,nn},{j,nn},{l,nn},{a,cbl},{b,cbl}];
	Do[
		If[BooleanQ[mat[[i,j,k,l]]==Null],Null,symmat[[i,j,k,l,a,b]]=If[b>=a,mat[[i,j,k,l,a,b]],Null]],
		{i,nn},{j,nn},{k,nn},{l,nn},{a,cbl},{b,cbl}
	];
	flmat=DeleteCases[#,Null]&//@Flatten[symmat,{{1},{2},{3},{4},{6,5}}];
	facs=DeleteDuplicates[Flatten[flmat]];
	facRep=(facs[[#]]->#-1)&/@Range[Length@facs];
	matRepd=(#/.facRep)&/@flmat;
	Print["Upper triangle (scan columns) of (ij)(kl)"];
	Print[DeleteCases[
		Flatten[Table[
			If[
				(4*(i-1)+(j-1))<=(4*(k-1)+(l-1)),
				matRepd[[i,j,k,l]],
				Null
			],
			{i,nn},{j,nn},{k,nn},{l,nn}
		]],
		Null
	]];
	Print["Element values: ",Length@facs];
	Print[export/@facs];
];


(* ::Section::Closed:: *)
(*Out 2 - stable*)


res=Table[
	(*Print[((nnn^3)*(i-1)+(nnn^2)*(j-1)+nnn*(k-1)+(l-1))/(nnn^4)*100//N,"%"];*)
	If[
		((4*(i-1)+(j-1))>=(4*(k-1)+(l-1))),
		CC2[i,j,k,l],
		Null
		],
	{i,nn},{j,nn},{k,nn},{l,nn}
	];


output2[mat_]:=Module[
	{symmat,facs,facRep,matRepd,out,norm},
	symmat=mat;
	Do[
		If[BooleanQ[symmat[[i,j,k,l]]==Null],Null,If[b>a,symmat[[i,j,k,l,a,b]]=Null]],
		{i,nn},{j,nn},{k,nn},{l,nn},{a,cbl},{b,cbl}
	];
	Do[
		symmat[[i,j,k,l]]=If[BooleanQ[symmat[[i,j,k,l]]==Null],Null,DeleteCases[Flatten[symmat[[i,j,k,l]]],Null]],
		{i,nn},{j,nn},{k,nn},{l,nn}
	];
	symmat=DeleteCases[Flatten[symmat],Null];
	facs=DeleteDuplicates[Flatten[symmat]];
	facRep=(facs[[#]]->#-1)&/@Range[Length@facs];
	matRepd=(#/.facRep)&/@symmat;
	out=Partition[matRepd,6];
	Print["Upper triangle, scan columns of (ij)(kl)/lower, rows: ", Length@out];
	Print[out];
	norm=PolynomialGCD@@facs;
	Print["Normalisation"];
	Print[export[norm]];
	Print["Element values: ",Length@facs];
	Print[export/@(facs/norm//Simplify)];
];


output2[res]


(* ::Section::Closed:: *)
(*Out 2 - unstable*)


(* nb replace i=j&k=l with {} in output *)
res=ParallelTable[
	(*Print[((nnn^3)*(i-1)+(nnn^2)*(j-1)+nnn*(k-1)+(l-1))/(nnn^4)*100//N,"%"];*)
	If[
		((4*(i-1)+(j-1))<=(4*(k-1)+(l-1))),
		If[(i==j||k==l),Infinity,CC2[i,j,k,l]],
		Null
		],
	{i,nn},{j,nn},{k,nn},{l,nn}
	];


output2[mat_]:=Module[
	{symmat,facs,facRep,matRepd,out,norm},
	symmat=mat;
	Do[
		If[BooleanQ[symmat[[i,j,k,l]]==Null||symmat[[i,j,k,l]]]||BooleanQ[symmat[[i,j,k,l]]==Infinity],Null,If[b<a,symmat[[i,j,k,l,a,b]]=Null]],
		{i,nn},{j,nn},{k,nn},{l,nn},{a,cbl},{b,cbl}
	];
	Do[
		If[BooleanQ[symmat[[i,j,k,l]]==Null]||BooleanQ[symmat[[i,j,k,l]]==Infinity],Null,symmat[[i,j,k,l]]=DeleteCases[Flatten[symmat[[i,j,k,l]]],Null]],
		{i,nn},{j,nn},{k,nn},{l,nn}
	];
	symmat=DeleteCases[Flatten[symmat/.Infinity->Table[Infinity,{i,6}]],Null];
	facs=DeleteCases[DeleteDuplicates[Flatten[symmat]],Infinity];
	facRep=(facs[[#]]->#-1)&/@Range[Length@facs];
	matRepd=(#/.facRep)&/@symmat;
	out=Partition[matRepd,6];
	Print["Upper triangle, scan columns of (ij)(kl): ", Length@out];
	Print[out/.Table[Infinity,{i,6}]->{}];
	norm=PolynomialGCD@@facs;
	Print["Normalisation"];
	Print[export[norm]];
	Print["Element values: ",Length@facs];
	Print[export/@(facs/norm//Simplify)];
];
output2[res]


(* ::Section::Closed:: *)
(*Out 3 - unstable*)


Module[
	{symmat,flmat,facs,facRep,matRepd,out,norm},
	symmat=Table[Null,{i,nn},{j,nn},{k,nn},{l,nn},{a,cbl},{b,cbl}];
	ParallelDo[
		If[
			((4*(i-1)+(j-1))<=(4*(k-1)+(l-1)))&&(a<=b),
			symmat[[i,j,k,l,a,b]]=CC2[i,j,k,l][[a,b]]
		],
		{i,nn},{j,nn},{k,nn},{l,nn},{a,cbl},{b,cbl}
	];
	flmat=Table[
		DeleteCases[Flatten[symmat[[i,j,k,l]],{2,1}],Null],
		{i,nn},{j,nn},{k,nn},{l,nn}
	];
	flmat=DeleteCases[Flatten[flmat,{4,3,2,1}],Null];
	facs=DeleteDuplicates[Flatten[flmat]];
	Print[facs];
	facRep=(facs[[#]]->#-1)&/@Range[Length@facs];
	matRepd=(#/.facRep)&/@flmat;
	out=Partition[matRepd,6];
	Print["Upper triangle, scan columns of (ij)(kl): ", Length@out];
	Print[out];
	norm=PolynomialGCD@@facs;
	Print["Normalisation"];
	Print[export[norm]];
	Print["Element values: ",Length@facs];
	Print[export/@(facs/norm//Simplify)];
];


CC2[2,2,2,2]


(* ::Section::Closed:: *)
(*Parallel WIP*)


	WaitAll@Table[
        ParallelSubmit[
			If[
				((4*(i-1)+(j-1))<=(4*(k-1)+(l-1))),
				CC2[i,j,k,l]
			]
        ],
        {i,nn},{j,nn},{k,nn},{l,nn}
    ];


Module[
	{symmat,flmat,facs,facRep,matRepd,out,norm},
	symmat=Table[Null,{ii,nn^2},{jj,nn^2},{a,cbl},{b,cbl}];
	
	Do[
		If[
			((4*(i-1)+(j-1))<=(4*(k-1)+(l-1)))&&(a<=b),
			symmat[[(nn^3)*(i-1)+(nn^2)*(j-1)+nn*(k-1)+(l-1),a,b]]=CC2[i,j,k,l][[a,b]]
		],
		{i,nn},{j,nn},{k,nn},{l,nn},{a,cbl},{b,cbl}
	];
	flmat=Table[
		DeleteCases[Flatten[symmat[[i,j,k,l]],{2,1}],Null],
		{i,nn},{j,nn},{k,nn},{l,nn}
	];
	flmat=DeleteCases[Flatten[flmat,{4,3,2,1}],Null];
	facs=DeleteDuplicates[Flatten[flmat]];
	facRep=(facs[[#]]->#-1)&/@Range[Length@facs];
	matRepd=(#/.facRep)&/@flmat;
	out=Partition[matRepd,6];
	Print["Upper triangle, scan columns of (ij)(kl): ", Length@out];
	Print[out];
	norm=PolynomialGCD@@facs;
	Print["Normalisation"];
	Print[export[norm]];
	Print["Element values: ",Length@facs];
	Print[export/@(facs/norm//Simplify)];
];


(* ::Section:: *)
(*Check*)


Do[
	Print[CC2[i,4,2,3]/.Nc->3//MatrixForm],
	{i,4}
]


Print[CC2[3,4,2,3]/.Nc->3//MatrixForm]


ColourBasis4g//TableForm


(* ::Section:: *)
(*Kinematics*)


(* S_ij(p_k,p_l) *)
sso[i_,j_,k_,l_]:=d[i,j]/d[k,l]*(1/(d[i,k]*d[j,l])+1/(d[j,k]*d[i,l]))-d[i,j]^2/(d[i,k]*d[j,l]*d[i,l]*d[j,k])


sso[pi,pj,q1,q2]/.{d[i_,j_]->dot[i,j]}//FullSimplify//CForm


(* S_ij(p_k,p_l) *)
s[i_,j_,k_,l_]:=sso[i,j,k,l]+(d[i,k]*d[j,l]+d[i,l]*d[j,k])/(d[i,k+l]*d[j,k+l])*((1-eps)/(d[k,l]^2)-sso[i,j,k,l]/2)-2*d[i,j]/(d[k,l]*d[i,k+l]*d[j,k+l])
s2[i_,j_,k_,l_]:=sso2[i,j,k,l]+(d[i,k]*d[j,l]+d[i,l]*d[j,k])/(d[i,k+l]*d[j,k+l])*(1/(d[k,l]^2)-sso2[i,j,k,l]/2)-2*d[i,j]/(d[k,l]*d[i,k+l]*d[j,k+l])


s2[pi,pj,q1,q2]//.{d[i_,j_+l__]->(d[i,j]+d[i,l])}/.{d[i_,j_]->s[i,j]/2}//FullSimplify


(s[pi,pj,q1,q2]//.{d[i_,j_+l__]->(d[i,j]+d[i,l])}//Series[#,{eps,0,0}]&//Normal//FullSimplify)/.{d[i_,j_]->dot[i,j]}//CForm
